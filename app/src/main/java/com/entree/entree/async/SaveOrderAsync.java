package com.entree.entree.async;

import android.content.Context;
import android.os.AsyncTask;

import com.entree.entree.utils.Utils;

/**
 * Created by jesudass on 1/23/2016.
 */
public class SaveOrderAsync extends AsyncTask<Void, Void,Void> {
    private Context mContext;
    private Utils utils;
    public SaveOrderAsync(Context mContext){
        this.mContext = mContext;
        utils = new Utils(mContext);
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        utils.showLoader();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

}
