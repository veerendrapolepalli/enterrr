package com.entree.entree.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.entree.entree.constructors.Restaurant;
import com.entree.entree.interfacelistener.RestaurantListner;
import com.entree.entree.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesudass on 05/02/16.
 */
public class GetRestaturantData extends AsyncTask<Void, Void, Void> {

    Context mContext;
    Boolean overrideLocation = false;
    LatLng newLatLng;
    Utils utils;
    private RestaurantListner restaurantListner = null;
    List<ParseObject> ob;
    private ArrayList<Restaurant> arrRestaurant = new ArrayList<Restaurant>();


    public GetRestaturantData(Context mContext, Boolean overrideLocation, LatLng newLatLng, RestaurantListner restaurantListner) {
        this.mContext = mContext;
        this.overrideLocation = overrideLocation;
        this.newLatLng = newLatLng;
        utils = new Utils(mContext);
        this.restaurantListner = restaurantListner;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        utils.showLoader();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (ob != null) {
            for (ParseObject restaurant : ob) {
                ParseFile ResBannerPhotoFile = restaurant.getParseFile("ResBannerPhoto");
                ParseFile ResPhotoFile = restaurant.getParseFile("ResPhoto");
                String Longitude = restaurant.getString("Longitude");
                String ResSlogan = restaurant.getString("ResSlogan");
                String ClosingTime = restaurant.getString("ClosingTime");
                String ContactEmail = restaurant.getString("ContactEmail");
                Boolean IsClosed = restaurant.getBoolean("IsClosed");
                String Website = restaurant.getString("Website");
                String FaxNo = restaurant.getString("FaxNo");
                String Pincode = restaurant.getString("Pincode");
                String ContactMobile = restaurant.getString("ContactMobile");
                String LandlineNo = restaurant.getString("LandlineNo");
                String Address2 = restaurant.getString("Address2");
                Boolean IsAvailableForDelivery = restaurant.getBoolean("IsAvailableForDelivery");
                String ResName = restaurant.getString("ResName");
                String ResTypeName = restaurant.getString("ResTypeName");
                String State = restaurant.getString("State");
                String Address1 = restaurant.getString("Address1");
                String Latitude = restaurant.getString("Latitude");
                String OpeningTime = restaurant.getString("OpeningTime");
                String ContactPerson = restaurant.getString("ContactPerson");
                String City = restaurant.getString("City");
                String ResBannerPhoto = "";

                if (ResBannerPhotoFile != null) {
                    ResBannerPhoto = ResBannerPhotoFile.getUrl();
                }
                String ResPhoto = "";
                if (ResPhotoFile != null) {
                    ResPhoto = ResPhotoFile.getUrl();
                }

                String ResId = restaurant.getObjectId();

                float[] results = new float[1];
                //Location.distanceBetween(newLatLng.latitude, newLatLng.longitude, Double.parseDouble(Latitude), Double.parseDouble(Longitude), results);
                float distanceInMeters = results[0];
                boolean isWithin10km = distanceInMeters < 10000;

                if (isWithin10km) {
                    Restaurant rest = new Restaurant(ResId, Longitude, ResSlogan, ClosingTime,
                            ContactEmail, IsClosed, Website,
                            FaxNo, Pincode, ContactMobile, LandlineNo,
                            Address2, ResPhoto, IsAvailableForDelivery, ResName,
                            ResTypeName, State, Address1, Latitude,
                            OpeningTime, ContactPerson, City, ResBannerPhoto);
                    arrRestaurant.add(rest);
                }
            }
        }
        utils.hideLoader();
        if (restaurantListner != null)
            restaurantListner.returnRestaurantList(arrRestaurant);
    }

    @Override
    protected Void doInBackground(Void... params) {
        arrRestaurant = new ArrayList<Restaurant>();
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                "Restaurant");
        try {
            ob = query.find();
        } catch (ParseException e) {

            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

}
