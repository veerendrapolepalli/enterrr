package com.entree.entree.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.entree.entree.constructors.SelectedLocation;
import com.entree.entree.interfacelistener.SearchedLocationListener;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.JSONParser;
import com.entree.entree.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by jesudass on 13-Feb-16.
 */
public class GetSearchedLocationAsync extends AsyncTask<Void,Void,Void> {
    Context mContext;
    SearchedLocationListener searchedLocationListener;
    String url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%s&radius=200&sensor=true&key=%s";
    String searchedText = "";
    Utils utils ;
    JSONObject json;
    JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    ArrayList<SelectedLocation> arrselectedLocation = new ArrayList<SelectedLocation>();
    ArrayList<String> names  = new ArrayList<String>();

    public GetSearchedLocationAsync(Context context,String searchedText,SearchedLocationListener searchedLocationListener){
        this.mContext = context;
        this.searchedText = searchedText;
        this.searchedLocationListener = searchedLocationListener;
        url = String.format(url, URLEncoder.encode(searchedText), AppConstants.GOOGLE_API_SEARCH_KEY);
        utils =  new Utils(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        utils.showLoader();
        utils.hideKeyBoard();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        utils.hideLoader();
        if(searchedLocationListener!=null) {
            searchedLocationListener.onSearchComplete(arrselectedLocation,names);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        JSONParser jParser = new JSONParser();
        json = jParser.getJSONFromUrl(url.toString());
        if (json != null) {
            try {
                contacts = json.getJSONArray(TAG_RESULT);
                arrselectedLocation = new ArrayList<SelectedLocation>();
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);
                    String description = c.getString("description");
                    String placeId = c.getString("place_id");
                    Log.d("description", description);
                    arrselectedLocation.add(new SelectedLocation(placeId, description));
                    names.add(description);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
