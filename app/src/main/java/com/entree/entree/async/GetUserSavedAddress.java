package com.entree.entree.async;

import android.content.Context;
import android.os.AsyncTask;

import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.interfacelistener.GetCustomerLocationListener;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesudass on 13-Feb-16.
 */
public class GetUserSavedAddress extends AsyncTask<Void,Void,Void> {
    List<ParseObject> ob= null;
    List<ParseObject> listCustomerAddress= null;
    ParseObject customersObj;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    GetCustomerLocationListener custAddressListener;
    Context mContext;
    Utils utils;

    public GetUserSavedAddress(Context mContext,GetCustomerLocationListener custAddressListener){
        this.custAddressListener = custAddressListener;
        this.mContext = mContext;
        utils = new Utils(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        utils.showLoader();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        utils.hideLoader();
        if(custAddressListener!=null)
            custAddressListener.onCustomerLocationFetched(arrCustAddr);
    }

    @Override
    protected Void doInBackground(Void... voids) {

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            currentUser.getObjectId();
        }
        ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                "Customers");
        queryCustomers.whereEqualTo("UserId", currentUser);
        try {
            ob = queryCustomers.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null && ob.size()>0) {
            customersObj = (ParseObject) ob.get(0);
            ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                    "CustomerAddresses");
            queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
            queryCustomerAddr.setLimit(3);

            try {
                listCustomerAddress = queryCustomerAddr.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if(listCustomerAddress!=null && listCustomerAddress.size()>0) {
                for(ParseObject custAddrObj : listCustomerAddress) {
                    String addressId = custAddrObj.getObjectId();
                    ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                    String firstName = custAddrObj.getString("FirstName");
                    String lastName = custAddrObj.getString("LastName");
                    String addressTitle = custAddrObj.getString("AddressTitle");
                    String address1 = custAddrObj.getString("Address1");
                    String address2 = custAddrObj.getString("Address2");
                    String landMark = custAddrObj.getString("Landmark");
                    String city = custAddrObj.getString("City");
                    String state = custAddrObj.getString("State");
                    String pincode = custAddrObj.getString("Pincode");
                    String latitude = custAddrObj.getString("Latitude");
                    String longitude = custAddrObj.getString("Longitude");

                    CustomerAddresses custAddress = new CustomerAddresses(addressId,customersObj,
                            firstName,lastName,addressTitle,address1,address2,landMark,
                            city,state,pincode,latitude,longitude);
                    arrCustAddr.add(custAddress);
                }

            }
        }
        return null;
    }
}
