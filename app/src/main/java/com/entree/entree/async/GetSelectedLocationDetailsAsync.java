package com.entree.entree.async;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.entree.entree.activity.MapLocation;
import com.entree.entree.interfacelistener.SelectedPlaceDetailListener;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.JSONParser;
import com.entree.entree.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nidhin on 13-Feb-16.
 */
public class GetSelectedLocationDetailsAsync extends AsyncTask<Void,Void,Void> {
    Context mContext;
    String placeId;
    Utils util;
    String detailUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=%s";
    JSONObject json;
    JSONObject placeDetail;
    String Lattitude = "";
    String Longitude = "";
    SelectedPlaceDetailListener selectedPlaceDetailListener;

    public GetSelectedLocationDetailsAsync(Context mContext,String placeId,SelectedPlaceDetailListener selectedPlaceDetailListener) {
        this.mContext = mContext;
        this.placeId = placeId;
        this.selectedPlaceDetailListener = selectedPlaceDetailListener;
        util = new Utils(mContext);
        detailUrl = String.format(detailUrl,placeId, AppConstants.GOOGLE_API_SEARCH_KEY);
    }
    @Override
    protected Void doInBackground(Void... voids) {
        // TODO Auto-generated method stub


        JSONParser jParser = new JSONParser();

        // getting JSON string from URL
        json = jParser.getJSONFromUrl(detailUrl.toString());
        if (json != null) {
            try {
                // Getting Array of Contacts
                placeDetail = json.getJSONObject("result");
                if (placeDetail != null) {
                    placeDetail = placeDetail.getJSONObject("geometry");
                    if (placeDetail != null) {
                        placeDetail = placeDetail.getJSONObject("location");
                        if (placeDetail != null) {
                            Lattitude = placeDetail.getString("lat");
                            Longitude = placeDetail.getString("lng");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        util.showLoader();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        util.hideLoader();
        if(selectedPlaceDetailListener!=null)
            selectedPlaceDetailListener.onFetchedDetailComplete(Lattitude,Longitude);
    }
}
