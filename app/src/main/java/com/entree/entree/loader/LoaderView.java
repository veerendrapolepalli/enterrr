package com.entree.entree.loader;

import android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class LoaderView extends View {

	public LoaderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, 3, 70 - 3 , 0xff64ad43, 0xffffffff,0xdd000000 );
	}

	public LoaderView(Context context) {
		super(context);
		init(context, 3, 70 - 3 , 0xff64ad43, 0xffffffff,0xdd000000 );
	}

	public LoaderView(Context context, int strokeWidth, int ovalDiameter) {
		super(context);
		init(context, strokeWidth, ovalDiameter,0xff64ad43, 0xffffffff,0xdd000000 );

	}
	public LoaderView(Context mContext, int strokeWidth, int ovalDiameter, int progressColor, int progressNormalColor, int backgroundColor ){
		super(mContext);
		init(mContext, strokeWidth, ovalDiameter,progressColor, progressNormalColor,backgroundColor );
	}
	private int STROKE_WIDTH;
	private int OVAL_DIAMETER;
	private RectF bgOval;
	private RectF whiteOval;
	private RectF greenOval;

	private Paint bgPaint;
	private Paint whitePaint;
	private Paint greenPaint;
	private Paint textPaint;
	private int currProgressType = 0;
	private Context mContext;
	private Rect textBounds = new Rect();
	

	private void init(Context mContext, int strokeWidth, int ovalDiameter, int progressColor, int progressNormalColor, int backgroundColor ) {
		this.mContext = mContext;
		this.STROKE_WIDTH = strokeWidth;
		this.OVAL_DIAMETER = ovalDiameter;
		bgOval = new RectF(0, 0, OVAL_DIAMETER + STROKE_WIDTH,
				OVAL_DIAMETER + STROKE_WIDTH);
		whiteOval = new RectF(STROKE_WIDTH, STROKE_WIDTH, OVAL_DIAMETER,
				OVAL_DIAMETER);
		greenOval = new RectF(STROKE_WIDTH, STROKE_WIDTH, OVAL_DIAMETER,
				OVAL_DIAMETER);

		bgPaint = initPaint(true, Paint.Style.FILL, backgroundColor);
		whitePaint = initPaint(true, Paint.Style.STROKE, progressNormalColor);
		whitePaint.setStrokeWidth(STROKE_WIDTH);
		greenPaint = initPaint(true, Paint.Style.STROKE, progressColor);
		greenPaint.setStrokeWidth(STROKE_WIDTH);

		textPaint = new Paint();
		textPaint.setColor(Color.parseColor("#a38aaf"));
		textPaint.setStyle(Paint.Style.FILL);
		textPaint.setAntiAlias(true);
		textPaint.setTextSize(getResources().getDimension(10));
		
	}

	private Paint initPaint(boolean setAntiAlias, Paint.Style paintStyle,
			int color) {
		Paint paint = new Paint();
		paint.setAntiAlias(setAntiAlias);
		paint.setStyle(paintStyle);
		paint.setColor(color);
		return paint;
	}

	private void drawArcs(Canvas canvas, RectF oval, Paint paint) {
		canvas.drawArc(oval, -90, (float) newProgress / 100 * 360, false, paint);
	}

	private String textToDraw;
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(Color.TRANSPARENT);

		canvas.drawArc(bgOval, 360, 360, false, bgPaint);
		canvas.drawArc(whiteOval, 360, 360, false, whitePaint);
		drawArcs(canvas, greenOval, greenPaint);
		
//		if (currProgressType == PapyrusConst.ACTION_FILE_DOWNLOAD || currProgressType == PapyrusConst.ACTION_FILE_UNZIPPING) {
//			textToDraw = newProgress + "%";
//		}
		textToDraw = newProgress + "%";
		
		/*if (currProgressType == PapyrusConst.ACTION_FILE_DOWNLOAD) {
			textToDraw = newProgress + "%";
			//canvas.drawText(newProgress + "%", 24, 40, textPaint);
			// canvas.drawText(newProgress + "%", 20, 35, textPaint);
		} else {
			 textToDraw = "Installing";
			// canvas.drawText("Installing", 10, 40, textPaint);
			//canvas.drawText("Installing", 8, 38, textPaint);
		}*/
		textPaint.setTextAlign(Align.CENTER);
		//textPaint.me
		textPaint.getTextBounds(textToDraw, 0,textToDraw.length()-1, textBounds);
		
		canvas.drawText(textToDraw, (OVAL_DIAMETER+STROKE_WIDTH)/2, (OVAL_DIAMETER+STROKE_WIDTH)/2+8, textPaint);

	}

	private int newProgress = 0;

	public void invalidate(int newProgress) {
		this.newProgress = newProgress;
		invalidate();
	}
	public void setProgress(int newProgress) {
		this.newProgress = newProgress;
		invalidate();
	}

	/**
	 * {@code PapyrusConst.ACTION_FILE_DOWNLOAD} or
	 * {@code PapyrusConst.ACTION_FILE_UNZIPPING}
	 * 
	 * @param progressType
	 */
	public void setCurrProgressType(int progressType) {
		this.currProgressType = progressType;
//		if (progressType == PapyrusConst.ACTION_FILE_DOWNLOAD || progressType == PapyrusConst.ACTION_FILE_UNZIPPING) {
//			textPaint.setTextSize(getResources().getDimension(R.dimen.loader_view_on_progress_text_size));
//		} else {
//			textPaint.setTextSize(getResources().getDimension(R.dimen.loader_view_text_size));
//		}
	}

}
