package com.entree.entree.chat;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.entree.entree.R;
import com.sendbird.android.MessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdFileUploadEventHandler;
import com.sendbird.android.model.FileInfo;
import com.sendbird.android.model.MessageModel;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by U45106 on 5/7/2016.
 */
public  class SendBirdChatFragment extends Fragment {
    private static final int REQUEST_PICK_IMAGE = 100;

    public ListView mListView;
    private SendBirdMessagingAdapter mAdapter;
    public EditText mEtxtMessage;
    private Button mBtnSend;
    private ImageButton mBtnUpload;
    private ProgressBar mProgressBtnUpload;

    public SendBirdChatFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sendbird_fragment_messaging, container, false);
        initUIComponents(rootView);
        return rootView;
    }


    private void initUIComponents(View rootView) {
        mListView = (ListView) rootView.findViewById(R.id.list);
        turnOffListViewDecoration(mListView);
        mListView.setAdapter(mAdapter);

        mBtnSend = (Button) rootView.findViewById(R.id.btn_send);
        mBtnUpload = (ImageButton) rootView.findViewById(R.id.btn_upload);
        mProgressBtnUpload = (ProgressBar) rootView.findViewById(R.id.progress_btn_upload);
        mEtxtMessage = (EditText) rootView.findViewById(R.id.etxt_message);

        mBtnSend.setEnabled(false);
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        mBtnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(Helper.requestReadWriteStoragePermissions(getActivity())) {
                    ((SendBirdMessagingActivity) getActivity()).isUploading = true;
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_PICK_IMAGE);
                }
            }
        });

        mEtxtMessage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        send();
                    }
                    return true; // Do not hide keyboard.
                }

                return false;
            }
        });
        mEtxtMessage.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        mEtxtMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mBtnSend.setEnabled(s.length() > 0);

                if (s.length() > 0) {
                    SendBird.typeStart();
                } else {
                    SendBird.typeEnd();
                }
            }
        });
        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Helper.hideKeyboard(getActivity());
                return false;
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (view.getFirstVisiblePosition() == 0 && view.getChildCount() > 0 && view.getChildAt(0).getTop() == 0) {
                        SendBird.queryMessageList(SendBird.getChannelUrl()).prev(mAdapter.getMinMessageTimestamp(), 30, new MessageListQuery.MessageListQueryResult() {
                            @Override
                            public void onResult(List<MessageModel> messageModels) {
                                if (messageModels.size() <= 0) {
                                    return;
                                }

                                for (MessageModel model : messageModels) {
                                    mAdapter.addMessageModel(model);
                                }
                                mAdapter.notifyDataSetChanged();
                                mListView.setSelection(messageModels.size());
                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });
                    } else if (view.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1 && view.getChildCount() > 0) {
                        SendBird.queryMessageList(SendBird.getChannelUrl()).next(mAdapter.getMaxMessageTimestamp(), 30, new MessageListQuery.MessageListQueryResult() {
                            @Override
                            public void onResult(List<MessageModel> messageModels) {
                                if (messageModels.size() <= 0) {
                                    return;
                                }

                                for (MessageModel model : messageModels) {
                                    mAdapter.addMessageModel(model);
                                }
                                mAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
    }

    private void showUploadProgress(boolean tf) {
        if (tf) {
            mBtnUpload.setEnabled(false);
            mBtnUpload.setVisibility(View.INVISIBLE);
            mProgressBtnUpload.setVisibility(View.VISIBLE);
        } else {
            mBtnUpload.setEnabled(true);
            mBtnUpload.setVisibility(View.VISIBLE);
            mProgressBtnUpload.setVisibility(View.GONE);
        }
    }

    private void turnOffListViewDecoration(ListView listView) {
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setHorizontalFadingEdgeEnabled(false);
        listView.setVerticalFadingEdgeEnabled(false);
        listView.setHorizontalScrollBarEnabled(false);
        listView.setVerticalScrollBarEnabled(true);
        listView.setSelector(new ColorDrawable(0x00ffffff));
        listView.setCacheColorHint(0x00000000); // For Gingerbread scrolling bug fix
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PICK_IMAGE && data != null && data.getData() != null) {
                upload(data.getData());
            }
        }
    }

    private void send() {
        SendBird.send(mEtxtMessage.getText().toString());
        mEtxtMessage.setText("");

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Helper.hideKeyboard(getActivity());
        }
    }

    private void upload(Uri uri) {
        try {
            Hashtable<String, Object> info = Helper.getFileInfo(getActivity(), uri);
            final String path = (String) info.get("path");
            final String mime = (String) info.get("mime");
            final int size = (Integer) info.get("size");

            if (path == null) {
                Toast.makeText(getActivity(), "Uploading file must be located in local storage.", Toast.LENGTH_LONG).show();
            } else {
                showUploadProgress(true);
                SendBird.uploadFile(new File(path), mime, size, "", new SendBirdFileUploadEventHandler() {
                    @Override
                    public void onUpload(FileInfo fileInfo, Exception e) {
                        showUploadProgress(false);
                        if (e != null) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Fail to upload the file.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        SendBird.sendFile(fileInfo);
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Fail to upload the file.", Toast.LENGTH_LONG).show();
        }
    }


    public void setSendBirdMessagingAdapter(SendBirdMessagingAdapter adapter) {
        mAdapter = adapter;
        if (mListView != null) {
            mListView.setAdapter(adapter);
        }
    }
}
