package com.entree.entree.chat;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.entree.entree.R;
import com.parse.Parse;
import com.parse.ParseUser;

import java.security.MessageDigest;

/**
 * SendBird Prebuilt UI
 */
public class MainActivity extends FragmentActivity {
    public static String VERSION = "2.1.1.0";

    /**
        To test push notifications with your own appId, you should replace google-services.json with yours.
        Also you need to set Server API Token and Sender ID in SendBird dashboard.
        Please carefully read "Push notifications" section in SendBird Android documentation
    */ 
    final String appId = "240520B1-F2D6-4BE2-9368-5BFB1B6D4ACD"; /* Sample SendBird Application */
 /*
 get userid and username from server
  */
    String userId = ParseUser.getCurrentUser().getUsername().toString(); /* Generate Device UUID */
    String userName = ParseUser.getCurrentUser().getUsername().toString(); /* Generate User Nickname */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Start GCM Service.
         */
       /* Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
*/

        ((EditText)findViewById(R.id.etxt_nickname)).setText(userName);

        ((EditText)findViewById(R.id.etxt_nickname)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                userName = s.toString();
            }
        });


        findViewById(R.id.btn_select_member).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startUserList();
                Intent intent = new Intent(MainActivity.this, SendBirdMessagingActivity.class);
               Bundle args = SendBirdMessagingActivity.makeMessagingStartArgs(appId, "veerendra", "veera", "veerend");
               // Bundle args = SendBirdMessagingActivity.makeMessagingStartArgs(appId, "897FEC90E385831F8C3F21BCEA73C433F0B57EFB", userName, "88DCA664DCF769F1303D65FE59D39686720B749A");
                intent.putExtras(args);
                startActivity(intent);
            }
        });

    }

  /*
    *//*
         send targetUserIds to join chat
     *//*

    private void startMessaging(String [] targetUserIds) {
        Intent intent = new Intent(MainActivity.this, SendBirdMessagingActivity.class);
        Bundle args = SendBirdMessagingActivity.makeMessagingStartArgs(appId, "897FEC90E385831F8C3F21BCEA73C433F0B57EFB", userName, "88DCA664DCF769F1303D65FE59D39686720B749A");
        intent.putExtras(args);
    }
*/




    private  String generateDeviceUUID() {
        String serial = android.os.Build.SERIAL;
        String androidID = Settings.Secure.ANDROID_ID;
        String deviceUUID = serial + androidID;

        /*
         * SHA-1
         */
        MessageDigest digest;
        byte[] result;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            result = digest.digest(deviceUUID.getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (byte b : result) {
            sb.append(String.format("%02X", b));
        }


        return sb.toString();
    }
}
