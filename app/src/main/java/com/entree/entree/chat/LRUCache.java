package com.entree.entree.chat;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by U45106 on 5/7/2016.
 */
public  class LRUCache {
    private final int maxSize;
    private int totalSize;
    private ConcurrentLinkedQueue<String> queue;
    private ConcurrentHashMap<String, String> map;

    public LRUCache(final int maxSize) {
        this.maxSize = maxSize;
        this.queue = new ConcurrentLinkedQueue<String>();
        this.map = new ConcurrentHashMap<String, String>();
    }

    public String get(final String key) {
        if (map.containsKey(key)) {
            queue.remove(key);
            queue.add(key);
        }

        return map.get(key);
    }

    public synchronized void put(final String key, final String value) {
        if (key == null || value == null) {
            throw new NullPointerException();
        }

        if (map.containsKey(key)) {
            queue.remove(key);
        }

        queue.add(key);
        map.put(key, value);
        totalSize = totalSize + getSize(value);

        while (totalSize >= maxSize) {
            String expiredKey = queue.poll();
            if (expiredKey != null) {
                totalSize = totalSize - getSize(map.remove(expiredKey));
            }
        }
    }

    private int getSize(String value) {
        return value.length();
    }
}