package com.entree.entree.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.activity.MyAccountActivity;
import com.entree.entree.activity.OrderHistoryActivity;
import com.parse.ParseUser;


/**
 * Created by vpol0001 on 5/1/2016.
 */
public class NavigationFragment extends Fragment implements View.OnClickListener {

    private TextView userName;
    private TextView emailAddress;
    private TextView account;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigation_screen_fragament_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userName = (TextView) view.findViewById(R.id.userName);
        emailAddress = (TextView) view.findViewById(R.id.userId);
        account = (TextView) view.findViewById(R.id.account);
        account.setOnClickListener(this);
        view.findViewById(R.id.order_history).setOnClickListener(this);
        userName.setText(ParseUser.getCurrentUser().getUsername().toString());
        emailAddress.setText(ParseUser.getCurrentUser().getEmail().toString());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.account:
                intent = new Intent(getActivity(), MyAccountActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.order_history:
                intent = new Intent(getActivity(), OrderHistoryActivity.class);
                getActivity().startActivity(intent);
                break;
        }
    }
}
