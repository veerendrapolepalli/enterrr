package com.entree.entree.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.entree.entree.R;
import com.entree.entree.utils.AppConstants;
import com.parse.ParseUser;
import com.sendbird.android.SendBird;
import com.sendbird.android.UserListQuery;

/**
 * Created by vamshi on 24-03-2016.
 */
public class Chat extends Fragment {


    View view;
    Context mContext;
    UserListQuery mUserListQuery;
    ListView mChatView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_chat,container,false);
        mChatView = (ListView) view.findViewById(R.id.chat_view);
        mContext = getActivity();
        SendBird.init(mContext,AppConstants.SENDBIRD_APPLICATION_ID);
//        SendBird.login( + "_" + );
        SendBird.login(SendBird.LoginOption.build(Track.orderNumber).setUserName(ParseUser.getCurrentUser().getUsername()));

        return view;
    }

    private void startMessaging(String userId) {
        SendBird.startMessaging(userId); // Start a 1:1 messaging with given userId.
    }

//    SendBird.setEventHandler(new SendBirdEventHandler() {
//        ...
//        @Override
//        public void onMessagingStarted(MessagingChannel channel) {
//            SendBird.join(chnannel.getUrl());
//            SendBird.connect();
//        }
//    });


}
