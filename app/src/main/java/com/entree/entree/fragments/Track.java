package com.entree.entree.fragments;


import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.entree.entree.R;
import com.entree.entree.constructors.Order;
import com.entree.entree.constructors.OrderLocation;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by vamshi on 24-03-2016.
 */
public class Track extends android.support.v4.app.Fragment {

    public static View view;
    public static GoogleMap googleMap;
    Utils utils = null;
    Context mContext;
    LogUtilities logUtils;
    ArrayList<OrderLocation> orderLocation = new ArrayList<OrderLocation>();
    List<ParseObject> ob = new ArrayList<>();
    Order orders;
    public static String orderID = " ";
    public static String orderNumber = " ";
    public static Date orderDate ;
    public static String objectID = " ";
    ArrayList<Order> Orders = new ArrayList<>();
    SupportMapFragment fm;
    ArrayList<ParseObject> arrDriverObj = new ArrayList<ParseObject>();
    ArrayList<ParseObject> arrOrderObj = new ArrayList<ParseObject>();
    Location dCurrentLocation,rCurrentLocation,oCurrentLocation;
    ArrayList<ParseObject> arrRestaurantObj = new ArrayList<ParseObject>();
    MapView mapView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }else {
            try {
                view = inflater.inflate(R.layout.fragment_track_order, container, false);
                fm = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
//                getParameters();
                googleMap = fm.getMap();
                mContext = getActivity();
                utils = new Utils(mContext);
                logUtils = new LogUtilities(mContext);

//                getOrderMapping();
//                new GetLocations().execute();

            } catch (InflateException e) {
        /* map is already there, just return view as it is */
            }
        }
        return view;

    }

    private void getParameters(){
        Intent i = getActivity().getIntent();
        if (i.hasExtra("Orders")) {
            orders = (Order) i.getSerializableExtra("Orders");
            if (orders != null) {
                orderID = orders.getOrderId();
                //Toast.makeText(mContext,resID+"",Toast.LENGTH_SHORT).show();
//                resName = resObj.getResName();
                getActivity().setTitle(orders.getOrderId());
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(googleMap != null){
            getOrderMapping();
        }
        if(googleMap == null) {
//            view = inflater.inflate(R.layout.fragment_track_order, container, false);
            fm = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map));
            googleMap = fm.getMap();
        }

    }

    private void getOrderMapping(){
        List<ParseObject> ob;

        logUtils.log(orderID);
        Summary.orderID = orderID;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderMapping");
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderMapping");
        query.include("DriverId");
//        query.include("Employees");
        query.include("ResId");
        query.include("OrderId");
//        query.include("CustomerId");
        query.whereEqualTo("OrderId",ParseObject.createWithoutData("Orders",orderID));



        try {
            ob = query.find();

            if(ob != null){

                String DriverLatitude="0.0",DriverLongitude="0.0",CustomerLatitude="0.0",CustomerLongitude="0.0",RestaurantLatitude="0.0",RestaurantLongitude="0.0";
                LatLng currentLatLng1,currentLatLng2=null,currentLatLng3;

                logUtils.log(ob.toString());
                for(ParseObject driverObj: ob){
                    ParseObject DriverObj = driverObj.getParseObject("DriverId");
                    arrDriverObj.add(DriverObj);
                }
                for(ParseObject orderObj: ob){
                    ParseObject OrderObj = orderObj.getParseObject("OrderId");
                    arrOrderObj.add(OrderObj);
                }
                for(ParseObject restObj: ob){
                    ParseObject RestObj = restObj.getParseObject("ResId");
                    arrDriverObj.add(RestObj);
                }

                if(arrDriverObj!=null && arrOrderObj!=null && arrRestaurantObj!=null){
                    for (ParseObject DriverObj : arrDriverObj) {
                        DriverLatitude = DriverObj.getString("Latitude");
                        DriverLongitude = DriverObj.getString("Longitude");
//                        String foodCategoryName = foodCategoryObj.fetchIfNeeded().getString("FoodCategoryName");
//                        FoodCategory fCategory = new FoodCategory(foodCategoryId, foodCategoryName);
//                        if(DriverLatitude != null && DriverLongitude!=null) {
//                            dCurrentLocation.setLatitude(Double.parseDouble(DriverLatitude));
//                            dCurrentLocation.setLatitude(Double.parseDouble(DriverLongitude));
//                        }
                        logUtils.log(DriverLatitude);
                        logUtils.log(DriverLongitude);
                    }
                    for (ParseObject OrderObj : arrOrderObj) {
                        orderNumber = OrderObj.getString("OrderNo");
                        orderDate = OrderObj.getDate("DeliveryDate");
                        CustomerLatitude = OrderObj.getString("DeliveryLatitude");
                        CustomerLongitude = OrderObj.getString("DeliveryLongitude");
//                        String foodCategoryName = foodCategoryObj.fetchIfNeeded().getString("FoodCategoryName");
//                        FoodCategory fCategory = new FoodCategory(foodCategoryId, foodCategoryName);
//                        if (CustomerLatitude != null && CustomerLongitude != null) {
//                            oCurrentLocation.setLatitude(Double.parseDouble(CustomerLatitude));
//                            oCurrentLocation.setLatitude(Double.parseDouble(CustomerLongitude));
//                        }

                        logUtils.log(CustomerLatitude);
                        logUtils.log(CustomerLongitude);
                    }
                    for (ParseObject RestObj : arrRestaurantObj) {
                        RestaurantLatitude = RestObj.getString("Latitude");
                        RestaurantLongitude = RestObj.getString("Longitude");
//                        String foodCategoryName = foodCategoryObj.fetchIfNeeded().getString("FoodCategoryName");
//                        FoodCategory fCategory = new FoodCategory(foodCategoryId, foodCategoryName);
//                        if (RestaurantLatitude != null && RestaurantLongitude != null) {
//                            rCurrentLocation.setLatitude(Double.parseDouble(RestaurantLatitude));
//                            rCurrentLocation.setLatitude(Double.parseDouble(RestaurantLongitude));
//                        }
                        logUtils.log(RestaurantLatitude);
                        logUtils.log(RestaurantLongitude);
                    }



                    MarkerOptions options = new MarkerOptions();

                    if(DriverLatitude != null && DriverLongitude != null) {

                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(DriverLatitude), Double.parseDouble(DriverLongitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_deliveryboy)));

//                        IconGenerator iconFactory1 = new IconGenerator(mContext);
//                        iconFactory1.setStyle(IconGenerator.STYLE_PURPLE);
//                        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
//                        options.anchor(iconFactory1.getAnchorU(), iconFactory1.getAnchorV());

//                         currentLatLng1 = new LatLng(Double.parseDouble(DriverLatitude),Double.parseDouble(DriverLongitude));
//                        options.position(currentLatLng1);
                    }

                    if(CustomerLatitude != null && CustomerLatitude != "0.0" && CustomerLongitude!= null && CustomerLongitude!= "0.0") {

                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(CustomerLatitude),Double.parseDouble(CustomerLongitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer)));

//                        IconGenerator iconFactory2 = new IconGenerator(mContext);
//                        iconFactory2.setStyle(IconGenerator.STYLE_PURPLE);
//                        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
//                        options.anchor(iconFactory2.getAnchorU(), iconFactory2.getAnchorV());
//
//                        currentLatLng2 = new LatLng(Double.parseDouble(CustomerLatitude),Double.parseDouble(CustomerLongitude));
//                        options.position(currentLatLng2);
                    }

                    if(RestaurantLatitude != null && RestaurantLongitude != null) {
                        IconGenerator iconFactory3 = new IconGenerator(mContext);
                        iconFactory3.setStyle(IconGenerator.STYLE_PURPLE);
                        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_restaurant));
                        options.anchor(iconFactory3.getAnchorU(), iconFactory3.getAnchorV());

                        currentLatLng3 = new LatLng(Double.parseDouble(RestaurantLatitude), Double.parseDouble(RestaurantLongitude));
                        options.position(currentLatLng3);
                    }

//                    googleMap.clear();

                    if(CustomerLatitude != null && CustomerLongitude!= null) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(CustomerLatitude),Double.parseDouble(CustomerLongitude))));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(CustomerLatitude),Double.parseDouble(CustomerLongitude)), 14));
                    }


                }

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(fm == null){
//            fm = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
//            googleMap = fm.getMap();
//        }


    }



    @Override
    public void onPause() {
        super.onPause();
//        if(fm != null)
//        googleMap.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        googleMap.clear();
    }

    private class GetLocations extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {

//            String userlocationlat = orders.getDeliveryLatitude();
//            String userlocationlong = orders.getDeliveryLongitude();

//            for(Order order:)

            List<ParseObject> ob;

            logUtils.log(orderID);

//            ParseQuery query = ParseQuery.getQuery("OrderMapping");
//            query.include("DriverId");
//            query.include("RestId");
//            query.include("CustomerId");
//            query.whereEqualTo("OrderId",ParseObject.createWithoutData("Order",orderID));

            ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderMapping");
//        ParseQuery<ParseObject> query = ParseQuery.getQuery("OrderMapping");
//        query.include("DriverId");
            query.include("Employees");
//        query.include("CustomerId");
            query.whereEqualTo("OrderId",ParseObject.createWithoutData("Orders",orderID));

//            query.include("RestId");
//            query.include("OrderId");

            try {
                ob = query.find();

                if(ob != null){
                    logUtils.log(ob.toString());
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


//            logUtils.log(orders.getOrderId());
//            logUtils.log( orders.getDeliveryLongitude());

//            orderLocation.add()


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            utils.hideLoader();

        }

    }



}
