package com.entree.entree.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.SummaryOrderAdapter;
import com.entree.entree.constructors.OrderedItems;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vamshi on 24-03-2016.
 */
public class Summary extends Fragment {

    View view;
    ListView lvOrderSummary;
    public static String orderID;
    List<ParseObject> ob;
    List<ParseObject> ob1;
    ArrayList<ParseObject> arrOrderItems = new ArrayList<ParseObject>();
    ArrayList<ParseObject> arrRes = new ArrayList<ParseObject>();
    OrderedItems ordereditems = null;
    ArrayList<OrderedItems> orderedItems = new ArrayList<OrderedItems>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_summary,container,false);
        lvOrderSummary = (ListView) view.findViewById(R.id.lvOrderSummary);
        ((TextView)view.findViewById(R.id.trackOrderNumber)).setText(Track.orderNumber);
        ((TextView)view.findViewById(R.id.trackOrderDate)).setText(Track.orderDate.toString());

        ParseQuery<ParseObject> queryO = ParseQuery.getQuery("OrderItems");
        queryO.include("ResId");
        queryO.include("OrderId");
        queryO.whereEqualTo("OrderId", ParseObject.createWithoutData("Orders", orderID));

        ParseQuery<ParseObject> query1 = ParseQuery.getQuery("OrderMapping");
        query1.include("OrderId");
        query1.include("ResId");
        query1.whereEqualTo("OrderId", ParseObject.createWithoutData("Orders", orderID));


        try {
            ob = queryO.find();
            ob1 = query1.find();

            if(ob != null && ob1 != null){
//                arrOrderItems.add((ArrayList)ob);

                String resName = " ";
                int resTotal = 0;
                ArrayList<String> foodName = new ArrayList<>();
                ArrayList<Integer> foodQty = new ArrayList<>();
                ArrayList<Integer> foodPrice = new ArrayList<>();
                int count=0;

//                for(int i = 0 ; i < ob1.size(); i++){

                    ArrayList<ParseObject> arrRes = new ArrayList<>();

                    for(ParseObject Restobj: ob1){
                        ParseObject ResObj = Restobj.getParseObject("ResId");
                        arrRes.add(ResObj);
                    }

                    for(int j = 0; j < ob.size(); j++){
                        resName = arrRes.get(count).getString("ResName");
                        resTotal = ob1.get(count).getInt("ResTotalAmount");

                        if(ob.get(j).get("ResId").equals(ob1.get(count).getParseObject("ResId"))){
                            if(j == ob.size()-1){
                                orderedItems.add(new OrderedItems(resName,resTotal,new ArrayList<String>(foodName),new ArrayList<Integer>(foodQty),new ArrayList<Integer>(foodPrice)));
//                                foodName.clear();
//                                foodPrice.clear();
//                                foodQty.clear();
                            }
                            foodName.add(ob.get(j).getString("FoodName"));
                            foodQty.add(ob.get(j).getInt("Qty"));
                            foodPrice.add(ob.get(j).getInt("Price"));
//                            count++;
                        }
                        else{

                            ordereditems = new OrderedItems(resName,resTotal,new ArrayList<String>(foodName),new ArrayList<Integer>(foodQty),new ArrayList<Integer>(foodPrice));
//                            ordereditems.setFoodName(foodName);
//                            ordereditems.setFoodPrice(foodPrice);
//                            ordereditems.setFoodQty(foodQty);

                            orderedItems.add(ordereditems);
                            count++;
//                            resName = arrRes.get(count).getString("ResName");
                            resTotal = ob1.get(count).getInt("ResTotalAmount");
                            foodName.clear();
                            foodPrice.clear();
                            foodQty.clear();
                            foodName.add(ob.get(j).getString("FoodName"));
                            foodQty.add(ob.get(j).getInt("Qty"));
                            foodPrice.add(ob.get(j).getInt("Price"));
                            if(j == ob.size()-1){
                                orderedItems.add(new OrderedItems(resName,resTotal,new ArrayList<String>(foodName),new ArrayList<Integer>(foodQty),new ArrayList<Integer>(foodPrice)));
                                foodName.clear();
                                foodPrice.clear();
                                foodQty.clear();
                            }
//                            continue;

                        }


                    }




//                }
//
//                for(ParseObject _arrRes:ob){
//                    ParseObject arrRest = _arrRes.getParseObject("ResId");
//                    arrRes.add(arrRest);
//                }
//
//                if(ob != null && arrRes.size() > 0){
//
//                }

                Log.v("Entree","OrderedItems"+orderedItems);

                SummaryOrderAdapter adapter = new SummaryOrderAdapter(getActivity(), orderedItems);
                lvOrderSummary.setAdapter(adapter);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return view;
    }


}
