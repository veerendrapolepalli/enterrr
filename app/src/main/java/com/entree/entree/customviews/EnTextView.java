package com.entree.entree.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by Jesudass on 1/23/2016.
 */
public class EnTextView extends TextView {

    private Context mContext;
    public static final int AVENIRLT_MEDIUM = 0;

    public static final String avenirMedium = "fonts/AvenirLTStd-Medium.otf";

    private static HashMap<String, Typeface> mapCreatedTypeFace = new HashMap<String, Typeface>();


    public EnTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        // setTextSize(28);

    }

    private void setFont(String fontName) {
        Typeface font = mapCreatedTypeFace.get(fontName);
        if (null == font) {
            font = Typeface.createFromAsset(mContext.getAssets(), fontName);
        }
        setTypeface(font);
            mapCreatedTypeFace.put(fontName, font);
    }


    public void setTypeFace(int type) {
        switch (type) {
            case AVENIRLT_MEDIUM:
                setFont(avenirMedium);
                break;
        }
    }
}
