package com.entree.entree.utils;

import android.content.Context;
import android.location.Address;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by jesudass on 1/15/2016.
 */
public class LocationWebApi {
    Context mContext;

    public static JSONObject getLocationInfo(String address) {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?latlng=" + address + "&sensor=false");
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static List<Address> getAddrByWeb(JSONObject jsonObject){
        List<Address> res = new ArrayList<Address>();
        try
        {
            JSONArray array = (JSONArray) jsonObject.get("results");
            for (int i = 0; i < array.length(); i++)
            {
                Double lon = new Double(0);
                Double lat = new Double(0);
                String name = "";
                try
                {
                    lon = array.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    lat = array.getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    JSONArray address_components = (JSONArray) array.getJSONObject(i).getJSONArray("address_components");

                    //address_components.length()

                    name = array.getJSONObject(i).getString("formatted_address");
                    Address addr = new Address(Locale.getDefault());
                    addr.setLatitude(lat);
                    addr.setLongitude(lon);
                    addr.setAddressLine(0, name != null ? name : "");

                    if(address_components.length()>3) {
                        addr.setPostalCode(address_components.get(address_components.length() - 1).toString());
                        addr.setAdminArea(address_components.get(address_components.length() - 2).toString());
                        addr.setLocality(address_components.get(address_components.length() - 3).toString());
                    } else {
                        if(address_components.length()>2) {
                            addr.setPostalCode(address_components.get(address_components.length() - 1).toString());
                            addr.setAdminArea(address_components.get(address_components.length() - 2).toString());
                            addr.setLocality("");
                        } else if(address_components.length()>1) {
                            addr.setPostalCode(address_components.get(address_components.length() - 1).toString());
                            addr.setAdminArea("");
                            addr.setLocality("");
                        } else {
                            addr.setPostalCode("");
                            addr.setAdminArea("");
                            addr.setLocality("");
                        }
                    }
                    addr.setFeatureName(name);

                    res.add(addr);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();

                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();

        }

        return res;
    }
}
