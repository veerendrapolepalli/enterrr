package com.entree.entree.utils;

/**
 * Created by jesudass on 12/30/2015.
 */
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.entree.entree.constructors.UserLocation;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationAddress {
    private static final String TAG = "LocationAddress";

    public static void getAddressFromLocation(final double latitude, final double longitude,
                                              final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                UserLocation u = null;
                try {
                    JSONObject response = LocationWebApi.getLocationInfo(String.valueOf(latitude) + "," + String.valueOf(longitude));

                    List < Address > addressList = geocoder.getFromLocation(
                                    latitude, longitude, 1);
                    if(addressList==null || addressList.size()<1) {
                        addressList = LocationWebApi.getAddrByWeb(response);
                    }
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        if(address.getAddressLine(0)!=null)
                            result = address.getAddressLine(0).toString();

                        u = new UserLocation(latitude,longitude,address);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {

                        message.what = 1;
                        Bundle bundle = new Bundle();
//                        result = "Latitude: " + latitude + " Longitude: " + longitude +
//                                "\n\nAddress:\n" + result;
                        bundle.putString("address", result);
                        bundle.putSerializable("user_location", u);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = "Latitude: " + latitude + " Longitude: " + longitude +
                                "\n Unable to get address for this lat-long.";
                        result = "";
                        bundle.putString("address", result);
                        bundle.putSerializable("user_location",u);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
}
