package com.entree.entree.utils;

import android.content.Context;
import android.util.Log;

/**
 * Created by jesudass on 12/29/2015.
 */
public class LogUtilities {
    private Context mContext;
    private static final String LOGTAG="ENTREELOGS";
    public LogUtilities(Context mContext) {
        this.mContext = mContext;
    }

    public void log(String msg){
        if(AppConstants.IS_DEBUG_MODE){
            Log.v(LOGTAG,LOGTAG + " : " +msg);
        }
    }
}
