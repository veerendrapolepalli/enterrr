package com.entree.entree.utils;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by jesudass on 12/30/2015.
 */
public class SnackBarUtils {
    private Context mContext;
    private CoordinatorLayout coordinatorLayout;
    private boolean setAction = false;
    private int showDuration = Snackbar.LENGTH_INDEFINITE;

    public SnackBarUtils(Context mContext,CoordinatorLayout coordinatorLayout,int showDuration){
        this.mContext = mContext;
        this.coordinatorLayout =coordinatorLayout;
        this.showDuration = showDuration;
    }

    public SnackBarUtils(Context mContext,CoordinatorLayout coordinatorLayout){
        this.mContext = mContext;
        this.coordinatorLayout =coordinatorLayout;
    }

    public void setAction(Boolean setAction){
        this.setAction = setAction;
    }

    public void showSnackbar(String msg){
        Snackbar snack = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_INDEFINITE);
        snack.setDuration(showDuration);
        if(setAction) {
            snack.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Activity) mContext).finish();
                }
            });
        }
        snack.show();
    }
}
