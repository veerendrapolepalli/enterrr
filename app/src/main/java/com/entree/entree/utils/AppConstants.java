package com.entree.entree.utils;

/**
 */
public class AppConstants {
    public static final String PARSE_APPLICATION_ID="jgTMwS01WF6dPSZTlZFlkEyIdb7UmL18LSY1Si0W";
    public static final String PARSE_CLIENT_ID="l9JTXoEj1fCyM7XlGEjzBsx3jptNLQf0CgAwO7kK";
    public static final String SENDBIRD_APPLICATION_ID="14C7E1AE-6578-4E53-8FF6-654DB02AEC21";
    public static Boolean IS_DEBUG_MODE = false;
    public static final String GOOGLE_APPLICATION_API="AIzaSyClcdI42CpHfSXxkV1t_1tb1gMVJBjXUys";
    public static final String DEBUG_GOOGLE_APPLICATION_API="AIzaSyB277oA6N_Ln-E0zbMP_NnS4dScmfJ_3W8";
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String DEFAULT_RESTAURANT_BANNER = "http://crop.alphacoders.com/images/941/960-540-94135.jpg";
    public static final int TYPE_GRID_VIEW = 0;
    public static final int TYPE_LIST_VIEW = 1;
    public static final String ORDER_NO_URL = "http://www.eatentree.com/api/max_orderno";
    public static final String TWITTER_CUSTOMER_ID ="xcXkY4tRksPzLNR7MkkeUL9fn";
    public static final String TWITTER_CUSTOMER_KEY ="pokb6UYOnjZahOfea7Rhy2ipeSsEnFIAkwtiR4ZD6zhftIMFel";
    public static final String GOOGLE_API_SEARCH_KEY = "AIzaSyCLzXyzHCtBEhWaSPvwgu1ixiZEnUBxlcs";
    public static final String BILL_URL = "http://www.eatentree.com/bill_generator.php";
    public static final String SEND_ORDER_NUMBER = "http://www.eatentree.com/broadcast/new_order_to_customer/";

    public static Double USER_LATTITUDE = 0.0;
    public static Double USER_LONGITUDE = 0.0;

}
