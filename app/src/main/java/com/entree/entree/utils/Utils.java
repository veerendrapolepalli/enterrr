package com.entree.entree.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.entree.entree.R;
import com.entree.entree.activity.EntreePreview;
import com.entree.entree.activity.MyAccountActivity;
import com.entree.entree.activity.OrderHistoryActivity;
import com.entree.entree.constructors.Restaurant;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private Context mContext;
    private SnackBarUtils snackbar;
    ProgressDialog mProgressDialog;

    public Utils(Context mContext) {
        this.mContext = mContext;
        setWindowMode();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean checkNetworkAvailabilityWithToast(CoordinatorLayout coordinatorLayout,Boolean showMsg) {
        if(isNetworkAvailable()) {
            return true;
        } else {
            if(showMsg) {
                snackbar = new SnackBarUtils(mContext,coordinatorLayout);
                snackbar.showSnackbar(mContext.getResources().getString(R.string.network_error));
            }
            return false;
        }
    }

    public void setDebugMode(){
        if(Boolean.parseBoolean(mContext.getResources().getString(R.string.debug_mode))){
            AppConstants.IS_DEBUG_MODE = true;
        }
    }

    public boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(AppConstants.EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (;;) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static Long generateOrderId(){
        long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        return number;
    }

    public static JSONObject getOrderNumber() {
        StringBuilder stringBuilder = new StringBuilder();
        try {

            HttpPost httppost = new HttpPost(AppConstants.ORDER_NO_URL);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;
            stringBuilder = new StringBuilder();


            response = client.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {

        } catch (IOException e) {

        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonObject;
    }

    public void showLoader(){
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }
    public void hideLoader(){
        try {
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
        }catch (Exception e) {

        }
    }

    public static Date getYesterdayDate(int no){
        Date givenDate = new Date(System.currentTimeMillis()-no*24*60*60*1000);
        Calendar cal = Calendar.getInstance();
        cal.setTime(givenDate);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DATE);
        int year = cal.get(Calendar.YEAR);

        String selectedDate = String.valueOf(day) + "-" + String.valueOf(month) + "-" +
                String.valueOf(year);
        DateFormat format = new SimpleDateFormat("d-M-yyyy", Locale.ENGLISH);
        try {
            givenDate = format.parse(selectedDate);
        }catch (Exception e){

        }
        Log.v("GIVEN DATE", givenDate.toString());
        return givenDate;
    }

    public void navigationOnClick(int id){
        if (id == R.id.nav_account) {

            Intent i = new Intent(mContext,MyAccountActivity.class);
            mContext.startActivity(i);

        } else if (id == R.id.nav_order) {

            Intent i = new Intent(mContext,OrderHistoryActivity.class);
            mContext.startActivity(i);

        } else if (id == R.id.nav_supprt) {

        } else if (id == R.id.nav_faq) {

        } else if (id == R.id.nav_signout) {

            ((Activity)mContext).finish();
            ((Activity)mContext).finishAffinity();
            ProgressDialog progressDialog = new ProgressDialog(mContext);
            progressDialog.show();
            ParseUser.getCurrentUser().logOut();
            Intent pIntent = new Intent(mContext, EntreePreview.class);
            mContext.startActivity(pIntent);
            progressDialog.dismiss();

        }


    }

    public static ParseObject getLoggedInCustomer(){
        ParseUser currentUser = ParseUser.getCurrentUser();
        List<ParseObject> ob = null;
        ParseObject customersObj= null;
        if (currentUser != null) {
            currentUser.getObjectId();
        }
        ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                "Customers");
        queryCustomers.whereEqualTo("UserId", currentUser);

        try {
            ob = queryCustomers.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null && ob.size()>0) {
            customersObj = (ParseObject) ob.get(0);
        }
        return customersObj;
    }

    public static List<ParseObject> getOrderItems(ParseObject orderObj){
        List<ParseObject> ob = null;
        ParseQuery<ParseObject> queryOrderItems = new ParseQuery<ParseObject>(
                "OrderItems");
        queryOrderItems.whereEqualTo("OrderId", orderObj);
        try {
            ob = queryOrderItems.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ob;
    }

    public void hideKeyBoard(){
        View view = ((Activity)mContext).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setWindowMode(){
        ((Activity)mContext).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    public Restaurant getRestaurentObject(ParseObject restaurant){
        Restaurant resObje = null;
        ParseFile ResBannerPhotoFile = (ParseFile) restaurant.get("ResBannerPhoto");
        ParseFile ResPhotoFile = (ParseFile) restaurant.get("ResPhoto");
        String Longitude = (String) restaurant.get("Longitude");
        String ResSlogan = (String) restaurant.get("ResSlogan");
        String ClosingTime = (String) restaurant.get("ClosingTime");
        String ContactEmail = (String) restaurant.get("ContactEmail");
        Boolean IsClosed = (Boolean) restaurant.get("IsClosed");
        String Website = (String) restaurant.get("Website");
        String FaxNo = (String) restaurant.get("FaxNo");
        String Pincode = (String) restaurant.get("Pincode");
        String ContactMobile = (String) restaurant.get("ContactMobile");
        String LandlineNo = (String) restaurant.get("LandlineNo");
        String Address2 = (String) restaurant.get("Address2");
        Boolean IsAvailableForDelivery = (Boolean) restaurant.get("IsAvailableForDelivery");
        String ResName = (String) restaurant.get("ResName");
        String ResTypeName = (String) restaurant.get("ResTypeName");
        String State = (String) restaurant.get("Longitude");
        String Address1 = (String) restaurant.get("Address1");
        String Latitude = (String) restaurant.get("Latitude");
        String OpeningTime = (String) restaurant.get("OpeningTime");
        String ContactPerson = (String) restaurant.get("ContactPerson");
        String City = (String) restaurant.get("City");
        String ResBannerPhoto = "";
        String ResPhoto = "";
        if (ResBannerPhotoFile != null)
            ResBannerPhoto = ResBannerPhotoFile.getUrl();
        if (ResPhotoFile != null)
            ResPhoto = ResPhotoFile.getUrl();

        String ResId = restaurant.getObjectId();
        resObje = new Restaurant(ResId, Longitude, ResSlogan, ClosingTime,
                ContactEmail, IsClosed, Website,
                FaxNo, Pincode, ContactMobile, LandlineNo,
                Address2, ResPhoto, IsAvailableForDelivery, ResName,
                ResTypeName, State, Address1, Latitude,
                OpeningTime, ContactPerson, City, ResBannerPhoto);
        return resObje;
    }

}
