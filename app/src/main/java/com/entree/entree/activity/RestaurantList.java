package com.entree.entree.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.adapter.RestaurantAdapter;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.Order;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantArray;
import com.entree.entree.fragments.Track;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.entree.entree.views.RobottoCondencedBold;
import com.entree.entree.wheel.ArrayWheelAdapter;
import com.entree.entree.wheel.OnWheelChangedListener;
import com.entree.entree.wheel.OnWheelScrollListener;
import com.entree.entree.wheel.WheelView;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RestaurantList extends BaseActivity {
    private ListView listview;
    List<ParseObject> ob;
    List<ParseObject> ob1;
    ProgressDialog mProgressDialog;
    private RestaurantAdapter resAdapter;
    private Utils utils = null;
    private LogUtilities logUtils = null;
    private RobottoCondencedBold btnLocation, btnASAP;
    private Context mContext;
    private ArrayList<Restaurant> arrRestaurant = new ArrayList<Restaurant>();
    ArrayList<String> wheelMenuDays = new ArrayList<>();
    ArrayList<String> wheelMenuTime = new ArrayList<>();
    private boolean wheelScrolled = false;
    private LinearLayout llwheel;
    private RelativeLayout relSearchRestaurant;
    private ImageView ivSearch, ivTray;
    private Boolean isWheelShown = false;
    private Boolean isLocationLayoutShown = false;
    LinearLayout llLocation,llCLocation;
    String userSelectedLocation = "";
    ParseObject customersObj;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    ListView lvUserAddress;
    Double Lattitude, Longitude;
    public static TextView tvOrderBadgeCount, tvUserName, tvEmailId, tvTrackOrder ;
    CircleImageView ivProfileicon;
    RelativeLayout relSearch;
    boolean isSearchLayoutShown = false,isOrderPlaced = false;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (tvOrderBadgeCount != null) {
            if (EntreeApplication.arrUserOtderItems != null && EntreeApplication.arrUserOtderItems.size() > 0) {
                tvOrderBadgeCount.setText("" + EntreeApplication.arrUserOtderItems.size());
                tvOrderBadgeCount.setVisibility(View.VISIBLE);
            } else {
                tvOrderBadgeCount.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        setContentView(R.layout.content_restaurant);
        mContext = this;

        pref = getSharedPreferences("MyPref",0);

        isOrderPlaced = pref.getBoolean("orderPlaced",false);

        tvTrackOrder = (TextView) findViewById(R.id.trackOrder);



        relSearch = (RelativeLayout) findViewById(R.id.relBg);
        if(isOrderPlaced){
            tvTrackOrder.setVisibility(View.VISIBLE);
        }
        getParams();
        init();
        setUtils();

        new GetUserAddress().execute();

        new GetRestaturantData().execute();
    }

    private void getParams() {
        Intent i = getIntent();
        Bundle extra = i.getExtras();
        if (extra.containsKey("RestaurentArray")) {
            RestaurantArray restArrObj = (RestaurantArray) extra.getSerializable("RestaurentArray");
            arrRestaurant = restArrObj.getRestArray();
        }if (extra.containsKey("ORDER_ID")){
            Track.orderID = extra.getString("ORDER_ID");
        }
        if (extra.containsKey("OBJECT_ID")){
            Track.objectID = extra.getString("OBJECT_ID");
        }
        if (extra.containsKey("cLocation")) {
            userSelectedLocation = extra.getString("cLocation");
        }
        if (extra.containsKey("LATTITUDE")) {
            Lattitude = extra.getDouble("LATTITUDE");
            AppConstants.USER_LATTITUDE = Lattitude;
        }
        if (extra.containsKey("LONGITUDE")) {
            Longitude = extra.getDouble("LONGITUDE");
            AppConstants.USER_LONGITUDE = Longitude;
        }
        Track.orderID = pref.getString("OrderId",null);

    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void init() {
        String UserAdd = "";
        if (getIntent().hasExtra("userAddress")) {
            UserAdd = getIntent().getExtras().getString("userAddress");

        }


        listview = (ListView) findViewById(R.id.listview);
        btnASAP = (RobottoCondencedBold) findViewById(R.id.btnASAP);
        btnLocation = (RobottoCondencedBold) findViewById(R.id.btnLocation);
        lvUserAddress = (ListView) findViewById(R.id.lvUserAddress);
        llwheel = (LinearLayout) findViewById(R.id.llWheel);
        relSearchRestaurant = (RelativeLayout) findViewById(R.id.relSearchRestaurant);
        tvTrackOrder.setOnClickListener(clicked);
        initWheelDay(R.id.wheelDay);
        initWheelTime(R.id.wheelTime);
        llLocation = (LinearLayout) findViewById(R.id.llLocation);
        llCLocation = (LinearLayout) findViewById(R.id.llCLocation);
        btnLocation.setText(UserAdd);
        btnLocation.setTextSize(16);
        btnASAP.setTextSize(16);

        tvOrderBadgeCount = (TextView) findViewById(R.id.tvOrderBadgeCount);


        llCLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RestaurantList.this,UserLocationActivity.class));
                finish();
            }
        });

        btnASAP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wheelMenuDays.clear();
                btnASAP.setTextColor(getResources().getColor(R.color.home_text));
                btnLocation.setTextColor(getResources().getColor(R.color.greyText));

                wheelMenuDays.add("Today");
                wheelMenuDays.add("Tomorrow");

                addTime();

                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_WEEK,2);

                if(calendar != null){

                    for(int i=0; i < 5; i++){
                        int day = calendar.get(Calendar.DAY_OF_WEEK);
                        switch (day){
                            case Calendar.SUNDAY:
                                wheelMenuDays.add("Sunday");
                                break;
                            case Calendar.MONDAY:
                                wheelMenuDays.add("Monday");
                                break;
                            case Calendar.TUESDAY:
                                wheelMenuDays.add("Tuesday");
                                break;
                            case Calendar.WEDNESDAY:
                                wheelMenuDays.add("Wednesday");
                                break;
                            case Calendar.THURSDAY:
                                wheelMenuDays.add("Thursday");
                                break;
                            case Calendar.FRIDAY:
                                wheelMenuDays.add("Friday");
                                break;
                            case Calendar.SATURDAY:
                                wheelMenuDays.add("Saturday");
                                break;

                        }

                        calendar.add(Calendar.DAY_OF_WEEK,1);

                    }

                }




                if (!isWheelShown) {
                    if (isLocationLayoutShown) {
                        isLocationLayoutShown = false;
                        Animation slideDown = AnimationUtils.loadAnimation(mContext,
                                R.anim.bottom_down);
                        llLocation.startAnimation(slideDown);
                        llLocation.setVisibility(View.GONE);
                    }
                    isWheelShown = true;
                    Animation bottomUp = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_up);
                    llwheel.startAnimation(bottomUp);
                    llwheel.setVisibility(View.VISIBLE);
                } else {
                    isWheelShown = false;
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_down);
                    llwheel.startAnimation(slideDown);
                    llwheel.setVisibility(View.GONE);
                    btnASAP.setTextColor(getResources().getColor(R.color.greyText));
                }

            }
        });

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnLocation.setTextColor(getResources().getColor(R.color.home_text));
                btnASAP.setTextColor(getResources().getColor(R.color.greyText));
                if (!isLocationLayoutShown) {
                    if (isWheelShown) {
                        isWheelShown = false;
                        Animation slideDown = AnimationUtils.loadAnimation(mContext,
                                R.anim.bottom_down);
                        llwheel.startAnimation(slideDown);
                        llwheel.setVisibility(View.GONE);
                    }
                    isLocationLayoutShown = true;
                    Animation slideUp = AnimationUtils.loadAnimation(mContext, R.anim.bottom_up);
                    llLocation.startAnimation(slideUp);
                    llLocation.setVisibility(View.VISIBLE);
                } else {
                    isLocationLayoutShown = false;
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_down);
                    llLocation.startAnimation(slideDown);
                    llLocation.setVisibility(View.GONE);
                    btnLocation.setTextColor(getResources().getColor(R.color.greyText));
                }
            }
        });


    }


    private View.OnClickListener clicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivSearch:
                    // Intent searchIntent = new Intent(RestaurantList.this, SearchRestaurantActivity.class);
                    //startActivity(searchIntent);
                    //overridePendingTransition(R.anim.slide_down, R.anim.stay);
                    relSearch.setVisibility(View.VISIBLE);
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.scroll_down);
                    slideDown.setInterpolator((new AccelerateDecelerateInterpolator()));
                    slideDown.setFillAfter(true);
                    relSearch.startAnimation(slideDown);
                    isSearchLayoutShown = true;
                    //llLocation.setVisibility(View.GONE);
                    break;

                case R.id.trackOrder:
                    Order order = new Order();
                    Intent i = new Intent(RestaurantList.this,TrackOrderActivity.class);
//                    startActivity(new Intent(RestaurantList.this,TrackOrderActivity.class));
                    i.putExtra("Orders", order);
                    startActivity(i);
            }

        }
    };

    // Wheel scrolled listener
    OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollStarts(WheelView wheel) {
            wheelScrolled = true;
        }

        public void onScrollEnds(WheelView wheel) {
            wheelScrolled = false;
        }

        @Override
        public void onScrollingStarted(WheelView wheel) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            // TODO Auto-generated method stub

        }
    };

    private final OnWheelChangedListener changedListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (!wheelScrolled) {
//                 updateStatus();
                //wheelMenuDays[getWheel(R.id.wheelDay).getCurrentItem()
            }
        }
    };

    private void initWheelDay(int id) {
        WheelView wheel = (WheelView) findViewById(id);
        wheel.setViewAdapter(new ArrayWheelAdapter(mContext, wheelMenuDays));
        wheel.setVisibleItems(2);
        wheel.setCurrentItem(0);
        wheel.addChangingListener(changedListener);
        wheel.addScrollingListener(scrolledListener);
    }

    private void initWheelTime(int id) {
        WheelView wheel = (WheelView) findViewById(id);
        wheel.setViewAdapter(new ArrayWheelAdapter(mContext, wheelMenuTime));
        wheel.setVisibleItems(2);
        wheel.setCurrentItem(0);
        wheel.addChangingListener(changedListener);
        wheel.addScrollingListener(scrolledListener);
    }

    /**
     * Returns wheel by Id
     *
     * @param id the wheel Id
     * @return the wheel with passed Id
     */
    private WheelView getWheel(int id) {
        return (WheelView) findViewById(id);
    }

    /**
     * Tests wheel value
     *
     * @param id the wheel Id
     * @return true if wheel value is equal to passed value
     */
    private int getWheelValue(int id) {
        return getWheel(id).getCurrentItem();
    }

    @Override
    public void onBackPressed() {
        if (isSearchLayoutShown) {
            Animation slideDown = AnimationUtils.loadAnimation(mContext,
                    R.anim.slide_in_up);
            relSearch.startAnimation(slideDown);
            relSearch.setVisibility(View.GONE);
            isSearchLayoutShown = false;
        } else {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

//        if()
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetUserAddress extends AsyncTask<Void, Void, Void> {
        List<ParseObject> ob = null;
        List<ParseObject> listCustomerAddress = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                currentUser.getObjectId();
            }
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("UserId", currentUser);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size() > 0) {
                customersObj = (ParseObject) ob.get(0);
                ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                        "CustomerAddresses");
                queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
                queryCustomerAddr.setLimit(3);

                try {
                    listCustomerAddress = queryCustomerAddr.find();
                } catch (ParseException e) {
                    //logUtils.log(e.getMessage());
                    e.printStackTrace();
                }
                if (listCustomerAddress != null && listCustomerAddress.size() > 0) {
                    for (ParseObject custAddrObj : listCustomerAddress) {
                        String addressId = custAddrObj.getObjectId();
                        ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                        String firstName = custAddrObj.getString("FirstName");
                        String lastName = custAddrObj.getString("LastName");
                        String addressTitle = custAddrObj.getString("AddressTitle");
                        String address1 = custAddrObj.getString("Address1");
                        String address2 = custAddrObj.getString("Address2");
                        String landMark = custAddrObj.getString("Landmark");
                        String city = custAddrObj.getString("City");
                        String state = custAddrObj.getString("State");
                        String pincode = custAddrObj.getString("Pincode");
                        String latitude = custAddrObj.getString("Latitude");
                        String longitude = custAddrObj.getString("Longitude");

                        CustomerAddresses custAddress = new CustomerAddresses(addressId, customersObj,
                                firstName, lastName, addressTitle, address1, address2, landMark,
                                city, state, pincode, latitude, longitude);
                        arrCustAddr.add(custAddress);
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCustAddr != null && arrCustAddr.size() > 0) {
                CustomerAddressListAdapter customerAddressListAdapter =
                        new CustomerAddressListAdapter(mContext, arrCustAddr);

                lvUserAddress.setAdapter(customerAddressListAdapter);
            }
            utils.hideLoader();
        }
    }

    private void addTime(){
        wheelMenuTime.add("09:00 AM");
        wheelMenuTime.add("09:15 AM");
        wheelMenuTime.add("09:30 AM");
        wheelMenuTime.add("09:45 AM");
        wheelMenuTime.add("10:00 AM");
        wheelMenuTime.add("10:15 AM");
        wheelMenuTime.add("10:30 AM");
        wheelMenuTime.add("10:45 AM");
        wheelMenuTime.add("11:00 AM");
        wheelMenuTime.add("11:15 AM");
        wheelMenuTime.add("11:30 AM");
        wheelMenuTime.add("11:45 AM");
        wheelMenuTime.add("12:00 PM");
        wheelMenuTime.add("12:15 PM");
        wheelMenuTime.add("12:30 PM");
        wheelMenuTime.add("12:45 PM");
        wheelMenuTime.add("01:00 PM");
        wheelMenuTime.add("01:15 PM");
        wheelMenuTime.add("01:30 PM");
        wheelMenuTime.add("01:45 PM");
        wheelMenuTime.add("02:00 PM");
        wheelMenuTime.add("02:15 PM");
        wheelMenuTime.add("02:30 PM");
        wheelMenuTime.add("03:00 PM");
        wheelMenuTime.add("03:15 PM");
        wheelMenuTime.add("03:30 PM");
        wheelMenuTime.add("03:45 PM");
        wheelMenuTime.add("04:00 PM");
        wheelMenuTime.add("04:15 PM");
        wheelMenuTime.add("04:30 PM");
        wheelMenuTime.add("04:45 PM");
        wheelMenuTime.add("05:00 PM");
        wheelMenuTime.add("05:15 PM");
        wheelMenuTime.add("05:30 PM");
        wheelMenuTime.add("05:45 PM");
        wheelMenuTime.add("06:00 PM");
        wheelMenuTime.add("06:15 PM");
        wheelMenuTime.add("06:30 PM");
        wheelMenuTime.add("06:45 PM");
        wheelMenuTime.add("07:00 PM");
        wheelMenuTime.add("07:15 PM");
        wheelMenuTime.add("07:30 PM");
        wheelMenuTime.add("07:45 PM");
        wheelMenuTime.add("08:00 PM");
        wheelMenuTime.add("08:15 PM");
        wheelMenuTime.add("08:30 PM");
        wheelMenuTime.add("08:45 PM");
        wheelMenuTime.add("09:00 PM");
        wheelMenuTime.add("09:15 PM");
        wheelMenuTime.add("09:30 PM");
        wheelMenuTime.add("09:45 PM");
        wheelMenuTime.add("10:00 PM");
        wheelMenuTime.add("10:15 PM");
        wheelMenuTime.add("10:30 PM");
        wheelMenuTime.add("10:45 PM");
        wheelMenuTime.add("11:00 PM");
        wheelMenuTime.add("11:15 PM");
        wheelMenuTime.add("11:30 PM");
        wheelMenuTime.add("11:45 PM");
    }


    // RemoteDataTask AsyncTask
    private class GetRestaturantData extends AsyncTask<Void, Void, Void> {
        String ResBannerPhoto = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (arrRestaurant == null || arrRestaurant.size() < 1) {
                // Locate the class table named "Country" in Parse.com
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                        "Restaurant");

                try {
                    ob = query.find();
                } catch (ParseException e) {
                    logUtils.log(e.getMessage());
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (ob != null) {
                for (ParseObject restaurant : ob) {
                    ParseFile ResBannerPhotoFile = (ParseFile) restaurant.get("ResBannerPhoto");
                    ParseFile ResPhotoFile = (ParseFile) restaurant.get("ResPhoto");
                    String Longitude = (String) restaurant.get("Longitude");
                    String ResSlogan = (String) restaurant.get("ResSlogan");
                    String ClosingTime = (String) restaurant.get("ClosingTime");
                    String ContactEmail = (String) restaurant.get("ContactEmail");
                    Boolean IsClosed = (Boolean) restaurant.get("IsClosed");
                    String Website = (String) restaurant.get("Website");
                    String FaxNo = (String) restaurant.get("FaxNo");
                    String Pincode = (String) restaurant.get("Pincode");
                    String ContactMobile = (String) restaurant.get("ContactMobile");
                    String LandlineNo = (String) restaurant.get("LandlineNo");
                    String Address2 = (String) restaurant.get("Address2");
                    Boolean IsAvailableForDelivery = (Boolean) restaurant.get("IsAvailableForDelivery");
                    String ResName = (String) restaurant.get("ResName");
                    String ResTypeName = (String) restaurant.get("ResTypeName");
                    String State = (String) restaurant.get("Longitude");
                    String Address1 = (String) restaurant.get("Address1");
                    String Latitude = (String) restaurant.get("Latitude");
                    String OpeningTime = (String) restaurant.get("OpeningTime");
                    String ContactPerson = (String) restaurant.get("ContactPerson");
                    String City = (String) restaurant.get("City");
                    String ResBannerPhoto = "";
                    String ResPhoto = "";
                    if (ResBannerPhotoFile != null)
                        ResBannerPhoto = ResBannerPhotoFile.getUrl();
                    if (ResPhotoFile != null)
                        ResPhoto = ResPhotoFile.getUrl();

                    String ResId = restaurant.getObjectId();
                    Restaurant rest = new Restaurant(ResId, Longitude, ResSlogan, ClosingTime,
                            ContactEmail, IsClosed, Website,
                            FaxNo, Pincode, ContactMobile, LandlineNo,
                            Address2, ResPhoto, IsAvailableForDelivery, ResName,
                            ResTypeName, State, Address1, Latitude,
                            OpeningTime, ContactPerson, City, ResBannerPhoto);
                    arrRestaurant.add(rest);
                }

                resAdapter = new RestaurantAdapter(mContext, arrRestaurant);
                listview.setAdapter(resAdapter);
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Restaurant res = arrRestaurant.get(position);

                        Intent i = new Intent(mContext, StoreActivity.class);
                        i.putExtra("RESTAURANT", res);
                        mContext.startActivity(i);
                    }
                });
            } else {
                if (arrRestaurant != null && arrRestaurant.size() > 0) {
                    resAdapter = new RestaurantAdapter(mContext, arrRestaurant);
                    listview.setAdapter(resAdapter);
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Restaurant res = arrRestaurant.get(position);
                            Intent i = new Intent(mContext, StoreActivity.class);
                            i.putExtra("RESTAURANT", res);
                            mContext.startActivity(i);
                        }
                    });
                }
            }
            mProgressDialog.dismiss();
        }
    }



}
