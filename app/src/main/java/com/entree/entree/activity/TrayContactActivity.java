package com.entree.entree.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.async.GetRestaturantData;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.CustomerParseObject;
import com.entree.entree.constructors.Customers;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantArray;
import com.entree.entree.constructors.UserLocation;
import com.entree.entree.interfacelistener.RestaurantListner;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.SnackBarUtils;
import com.entree.entree.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.maps.android.ui.IconGenerator;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrayContactActivity extends BaseActivity implements GoogleMap.OnMapLoadedCallback, OnMapReadyCallback {
    private Utils utils = null;
    private LogUtilities logUtils = null;
    EditText etAlternateNo, etAdd1, etAdd2, etAdd3;
    private static final int CHECKOUT = 1;
    Button btnContact;
    private SnackBarUtils snackbar;
    private CoordinatorLayout coordinatorLayout;
    private RelativeLayout llProceed;
    private TextView tvPrice, tvOrderBadgeCount, tvArrivesOn, tvPhoneno;
    ListView lvUserAddress;
    ParseObject customersObj;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();

    String phoneNumber = "";
    String address1 = "";
    String address2 = "";
    String address3 = "";
    String priceAmt;
    int resCount;
    ProgressDialog mProgressDialog;
    private Context mContext;
    public static UserLocation userLocation = null;
    private CustomerParseObject customerParseObject = null;
    GoogleMap googleMap;

    private TextView tvUserName, tvEmailId;
    CircleImageView ivProfileicon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        setContentView(R.layout.content_contact_info);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setTitle("Tray");
        mContext = this;
        init();
        setUI();
        new GetUserAddress().execute();
    }

    private void init() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
        if (getIntent().hasExtra("Total_Price")) {
            priceAmt = getIntent().getExtras().get("Total_Price").toString();
        } else {
            priceAmt = "0";
        }
        if (getIntent().hasExtra("Rest_Count")) {
            resCount = getIntent().getIntExtra("Rest_Count", 1);
        }
        priceAmt = getResources().getString(R.string.Rs) + " " + priceAmt;
    }

    private void setUI() {
        etAlternateNo = (EditText) findViewById(R.id.etAlternateNo);
        etAdd1 = (EditText) findViewById(R.id.etAdd1);
        etAdd2 = (EditText) findViewById(R.id.etAdd2);
        etAdd3 = (EditText) findViewById(R.id.etAdd3);
        lvUserAddress = (ListView) findViewById(R.id.lvUserAddress);
        // btnContact = (Button) findViewById(R.id.btnContact);
        tvPrice = (TextView) findViewById(R.id.tvtotalprice);
        tvArrivesOn = (TextView) findViewById(R.id.tvArrivesOn);
        tvPhoneno = (TextView) findViewById(R.id.tvPhoneno);
        tvPrice.setText(priceAmt);
        llProceed = (RelativeLayout) findViewById(R.id.llproceed);
        llProceed.setOnClickListener(onClickListener);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
//        btnContact.setOnClickListener(onClickListener);
        llProceed.setId(CHECKOUT);

        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        fm.getMapAsync(this);
        googleMap = fm.getMap();

//        googleMap.getUiSettings().setZoomControlsEnabled(false);
//        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//        googleMap.getUiSettings().setZoomControlsEnabled(false);
//        googleMap.getUiSettings().setCompassEnabled(false);
//        googleMap.getUiSettings().setZoomGesturesEnabled(false);
//        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm a");
        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        int timeToBeAdded = 0;
        if (resCount > 1) {
            timeToBeAdded = 5 * resCount;
        }

        DateFormat format2 = new SimpleDateFormat("EEEE");
        Date deliveryDate = new Date(t + ((45 + timeToBeAdded) * 60000));
        String deliveryTime = format.format(deliveryDate);
//date.setTime(deliveryDate);
        tvArrivesOn.setText(format2.format(deliveryDate) + " AT " + deliveryTime);
//        tvPhoneno.setText("+91 "+ParseUser.getCurrentUser().get("MobileNo").toString());


    }

    private void addMarker(LatLng latLng) {
        MarkerOptions options = new MarkerOptions();

        IconGenerator iconFactory = new IconGenerator(this);
        iconFactory.setStyle(IconGenerator.STYLE_PURPLE);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        LatLng currentLatLng = latLng;
        options.position(currentLatLng);
        Marker mapMarker = googleMap.addMarker(options);
        mapMarker.setDraggable(true);
        //googleMap.clear();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(13), 1000, null);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch ((v.getId())) {
                case CHECKOUT:
                    naviagetToPayment();
                    break;
                default:
                    break;
            }
        }
    };

    private void naviagetToPayment() {
        if (validateForm()) {
            saveContactData();
        }


    }

    private void openPayment() {
        if (customerParseObject != null) {
            String custId = customerParseObject.getCustomer().getObjectId();
            Intent i = new Intent(TrayContactActivity.this, PaymentActivity.class);

            i.putExtra("priceAmt", priceAmt);
            i.putExtra("CustomerId", custId);

            startActivity(i);
        }
    }

    public boolean validateForm() {
        phoneNumber = etAlternateNo.getText().toString();
        address1 = etAdd1.getText().toString();
        address2 = etAdd2.getText().toString();
        address3 = etAdd3.getText().toString();
        if (!phoneNumber.trim().equals("")) {
            if (!validCellPhone(phoneNumber) && phoneNumber.length() != 10) {
                etAlternateNo.setError("Invalid phone number");
                return false;
            }
        }
        if (address1.trim().equalsIgnoreCase("")) {
            etAdd1.setError("Please fill proper address");
            return false;
        }
        if (address2.trim().equalsIgnoreCase("")) {
            etAdd2.setError("Please fill proper address");
            return false;
        }
        if (address3.trim().equalsIgnoreCase("")) {
            etAdd3.setError("Please fill proper address");
            return false;
        }
//        if(!validateAddress()) {
//            snackbar = new SnackBarUtils(mContext, coordinatorLayout);
//            snackbar.showSnackbar("Invalid Address");
//            return false;
//        }


        return true;
    }

    private void showLoader() {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    private void hideLoader() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    private void saveContactData() {

        showLoader();
        new SaveCustomerData().execute();
    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(TrayContactActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(TrayContactActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(TrayContactActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            final LatLng currentlocation = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.addMarker(new MarkerOptions()
                    .position(currentlocation)
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
//            googleMap.addMarker(new MarkerOptions().position(currentlocation).title("Current Location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlocation, 14));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentlocation, 14));
        }
    }

    private class SaveCustomerData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Address addr = new Address(Locale.getDefault());
            String name = address1 + ", " + address2 + ", " + address3;
            addr.setAddressLine(0, name != null ? name : "");
            addr.setFeatureName(name);
            addr.setLocality(name);
            addr.setAdminArea(name);
            addr.setPostalCode(name);
            userLocation = new UserLocation(0, 0, addr);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                Customers customerobj = new Customers(currentUser, "TEST", "User", "+919991113332", phoneNumber, currentUser.getEmail());
                customerobj.sendCustomerToParse(new CustomerDataHandler(), userLocation);
            }
        }
    }

    private class CustomerDataHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    customerParseObject = (CustomerParseObject) bundle.getSerializable("CustomerParseObject");
                    break;
                default:

            }
            hideLoader();
            openPayment();
        }
    }

    private boolean validateAddress() {
        Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault());
        Barcode.GeoPoint p1 = null;
        try {
            List<Address> addresses =
                    geoCoder.getFromLocationName(address1 + "," +
                            address2 + "", 3);

            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean validCellPhone(String number) {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tray_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class GetUserAddress extends AsyncTask<Void, Void, Void> {
        List<ParseObject> ob = null;
        List<ParseObject> listCustomerAddress = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                currentUser.getObjectId();
            }
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("UserId", currentUser);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size() > 0) {
                customersObj = (ParseObject) ob.get(0);
                ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                        "CustomerAddresses");
                queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
                queryCustomerAddr.setLimit(3);

                try {
                    listCustomerAddress = queryCustomerAddr.find();
                } catch (ParseException e) {
                    //logUtils.log(e.getMessage());
                    e.printStackTrace();
                }
                if (listCustomerAddress != null && listCustomerAddress.size() > 0) {
                    for (ParseObject custAddrObj : listCustomerAddress) {
                        String addressId = custAddrObj.getObjectId();
                        ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                        String firstName = custAddrObj.getString("FirstName");
                        String lastName = custAddrObj.getString("LastName");
                        String addressTitle = custAddrObj.getString("AddressTitle");
                        String address1 = custAddrObj.getString("Address1");
                        String address2 = custAddrObj.getString("Address2");
                        String landMark = custAddrObj.getString("Landmark");
                        String city = custAddrObj.getString("City");
                        String state = custAddrObj.getString("State");
                        String pincode = custAddrObj.getString("Pincode");
                        String latitude = custAddrObj.getString("Latitude");
                        String longitude = custAddrObj.getString("Longitude");

                        CustomerAddresses custAddress = new CustomerAddresses(addressId, customersObj,
                                firstName, lastName, addressTitle, address1, address2, landMark,
                                city, state, pincode, latitude, longitude);
                        arrCustAddr.add(custAddress);
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCustAddr != null && arrCustAddr.size() > 0) {
                CustomerAddressListAdapter customerAddressListAdapter =
                        new CustomerAddressListAdapter(mContext, arrCustAddr);

                lvUserAddress.setAdapter(customerAddressListAdapter);
            }
            utils.hideLoader();
            addMarker(new LatLng(AppConstants.USER_LATTITUDE, AppConstants.USER_LONGITUDE));
        }
    }
}
