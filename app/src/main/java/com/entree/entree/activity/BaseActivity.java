package com.entree.entree.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.fragments.NavigationFragment;
import com.entree.entree.interfacelistener.FoodStatusChange;


/**
 * Created by vpol0001 on 5/1/2016.
 */
public class BaseActivity extends AppCompatActivity implements FoodStatusChange {


    Toolbar toolbar;
    public  TextView tvOrderBadgeCount;
    public TextView tvPriceDispaly;
    AppBarLayout appBarLayout;
    ImageView imgTray;
    LinearLayout searchLayout;
    FrameLayout traymainLayout;
    LinearLayout savedAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        EntreeApplication.registerFoodChangeListner(this);
    }

    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout mDrawerLayout;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.base_activity_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        if (layoutResID != R.layout.base_activity) {
            super.setContentView(R.layout.base_activity);
            appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            imgTray = (ImageView) appBarLayout.findViewById(R.id.ivTray);
            tvOrderBadgeCount =(TextView) appBarLayout.findViewById(R.id.tvOrderBadgeCount);
            searchLayout = (LinearLayout) appBarLayout.findViewById(R.id.ivSearch);
            traymainLayout = (FrameLayout) appBarLayout.findViewById(R.id.traymainLayout);
            savedAddress = (LinearLayout) appBarLayout.findViewById(R.id.savedAddress);
            searchLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BaseActivity.this,SearchRestaurantActivity.class);
                    startActivity(intent);
                }
            });
            if(tvOrderBadgeCount != null)
                tvOrderBadgeCount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(BaseActivity.this,TrayOrderSummary.class);
                        startActivity(intent);finish();

                    }
                });
            if(imgTray != null)
                imgTray.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(BaseActivity.this,TrayOrderSummary.class);
                        startActivity(intent);
                        finish();
                    }
                });
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            ViewGroup contentContainer = (ViewGroup) findViewById(R.id.content_container);
            getLayoutInflater().inflate(layoutResID,contentContainer,true);
            actionBarDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name,R.string.app_name);
            mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
            if(getSupportActionBar() != null) {
                getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
            attachNavigationFragment();
        } else {
            super.setContentView(layoutResID);
        }
    }
    ActionBarDrawerToggle actionBarDrawerToggle;


    public void attachNavigationFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.navigation_drawer_container,new NavigationFragment());
        transaction.commit();
    }


    @Override
    protected void onStart() {
        super.onStart();
        notifyFoodChange();
        notifyPriceChange();
        setVisibleLayouts();
    }

    public void setVisibleLayouts(){

        if(this instanceof UserLocationActivity){
            savedAddress.setVisibility(View.VISIBLE);
            traymainLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerLayout.openDrawer(GravityCompat.START);
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        EntreeApplication.unRegisterFoodChangeListner(this);
    }

    public void setToolbarTitle(String title){
        if(toolbar != null)
            toolbar.setTitle(title);
    }

    public void setToolbarTitle(int title){
        if(toolbar != null)
            toolbar.setTitle(title);
    }

    public void setBadgeCount(TextView textView){
        tvOrderBadgeCount = textView;
    }

    @Override
    public void notifyFoodChange() {
        if(tvOrderBadgeCount == null) return;
        tvOrderBadgeCount.setText("" + EntreeApplication.arrUserOtderItems.size());
        tvOrderBadgeCount.setVisibility(View.VISIBLE);

    }

    private int calculateTotalCost() {
        int price = 0;
        for (OrderItem item : EntreeApplication.arrUserOtderItems) {
            price = price + item.getItemTotal();
        }
        return price;
    }

    @Override
    public void notifyPriceChange() {
        if(tvPriceDispaly == null) return;
        int price = 0;
        if(EntreeApplication.arrUserOtderItems.size() > 0){
            price = calculateTotalCost();
        }
        else {
            price = 0 ;
        }

        tvPriceDispaly.setText(getResources().getString(R.string.Rs) +
                " "+price);
    }
}
