package com.entree.entree.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.entree.entree.R;
import com.entree.entree.utils.Utils;

public class SearchRestaurantActivity extends AppCompatActivity {

    private Context mContext;
    private ImageView ivSearch;
    private RelativeLayout relSearchRestaurant;

    private EditText etSearchFilter;

    private Utils utils = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
       // int width = (int) (getResources().getDisplayMetrics().widthPixels);
        //int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.80);
        // getWindow().setLayout(width,height);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.content_search_restaurant);
        mContext = this;
        utils = new Utils(mContext);
        init();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_up, R.anim.stay);
    }

    private void init() {
        etSearchFilter = (EditText) findViewById(R.id.etSearchRest);
        //etSearchFilter.setSelected(false);
        ivSearch = (ImageView) findViewById(R.id.ivSearchSubmit);
        relSearchRestaurant = (RelativeLayout) findViewById(R.id.relSearchRestaurant);
        ivSearch.getDrawable().setColorFilter(getResources().getColor(R.color.home_text), PorterDuff.Mode.MULTIPLY);
        Display display = getWindowManager().getDefaultDisplay();
        int height = display.getHeight();

        //my EditText will be smaller than full screen (80%)
        int doubleSize = (height / 5) * 4;

        // relSearchRestaurant.getLayoutParams().height = doubleSize;
        //relSearchRestaurant.invalidate();

        setupUI(findViewById(R.id.parentLayout));
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    utils.hideKeyBoard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

}
