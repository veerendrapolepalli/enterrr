package com.entree.entree.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.fragments.Chat;
import com.entree.entree.fragments.Summary;
import com.entree.entree.fragments.Track;
import com.entree.entree.loader.ImageLoader;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vamshi on 24-03-2016.
 */
public class TrackOrderActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager mViewPager;
    TextView tvUserName, tvEmailId;
    CircleImageView ivProfileicon;
    public static ImageLoader imageLoader;
    Context mContext;
    private Utils utils = null;
    private LogUtilities logUtils = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order);
        mContext = this;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        View headerView = navigationView.getHeaderView(0);

        tvUserName = (TextView) headerView.findViewById(R.id.tvUserName);
        tvEmailId = (TextView) headerView.findViewById(R.id.tvEmailId);

        ivProfileicon = (CircleImageView) headerView.findViewById(R.id.ivNavBarProfileIcon);
        tvEmailId.setText(ParseUser.getCurrentUser().getUsername());
        imageLoader = new ImageLoader(mContext);

        mViewPager = (ViewPager) findViewById(R.id.container);
        setUpViewpager(mViewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


    }

    private void setUpViewpager(ViewPager viewPager){

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new Track(),"Track");
        adapter.addFrag(new Summary(),"Summary");
        adapter.addFrag(new Chat(),"Chat");

        mViewPager.setAdapter(adapter);

    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Track.view = null;
        if(Track.googleMap != null){
            Track.googleMap = null;
        }
//        Intent intent = new Intent(TrackOrderActivity.this, RestaurantList.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
//        startActivity(intent);


//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction trans = manager.beginTransaction();
//        trans.remove(Track());
//        trans.commit();
//        manager.popBackStack();
//        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        utils.navigationOnClick(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
