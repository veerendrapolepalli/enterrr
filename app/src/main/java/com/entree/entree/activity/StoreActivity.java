package com.entree.entree.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.PlaceholderFragment;
import com.entree.entree.adapter.TabAdapter;
import com.entree.entree.constructors.FoodCategory;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.loader.ImageLoader;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class StoreActivity extends BaseActivity
         {

    private TabAdapter mTabPagerAdapter;

    private ViewPager mViewPager;
    List<ParseObject> ob;
    List<ParseObject> ob1;
    ProgressDialog mProgressDialog;
    ArrayAdapter<String> adapter;
    private Utils utils = null;
    private LogUtilities logUtils = null;
    private static Context mContext;
    private ArrayList<FoodCategory> arrFoodCategory = new ArrayList<FoodCategory>();
    private TabLayout tabLayout;
    private String resID;
    private Restaurant resObj;
    private static LinearLayout llTray;
    private static ImageView btnTray;
    private static TextView tvTotalPrice, tvOrderBadgeCount,tvViewTray;
    public static ImageLoader imageLoader;
    TextView tvUserName, tvEmailId;
    CircleImageView ivProfileicon;
    private ImageView ivTray,ivSearch;
    RelativeLayout relSearch;
    boolean isSearchLayoutShown = false;

    ArrayList<ParseObject> arrParseObject = new ArrayList<ParseObject>();
    ArrayList<String> categoryIdList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        mContext = this;

        imageLoader = new ImageLoader(mContext);
        relSearch = (RelativeLayout) findViewById(R.id.relBg);

        getIntentValues();
        init();
        setUtils();

        // new GetFoodCategoryAsync().execute();
        new GetRestauarantCategory().execute();
    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void init() {
        mViewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        llTray = (LinearLayout) findViewById(R.id.llTray);
        btnTray = (ImageView) findViewById(R.id.btnTray);
        btnTray.setBackgroundResource(R.drawable.menu_listview);
        tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        tvPriceDispaly = tvTotalPrice;
        tvOrderBadgeCount = (TextView) findViewById(R.id.tvOrderBadgeCount);
        tvPriceDispaly.setText(mContext.getResources().getString(R.string.Rs) + " " + 0);

        tvViewTray = (TextView) findViewById(R.id.View_tray);
//        llTray.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!tvTotalPrice.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Rs) + " 0")) {
//                    Intent i = new Intent(mContext, TrayOrderSummary.class);
//                    startActivity(i);
//                }
//
//            }
//        });

        tvViewTray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!tvTotalPrice.getText().toString().equalsIgnoreCase(getResources().getString(R.string.Rs) + " 0")) {
                    Intent i = new Intent(mContext, TrayOrderSummary.class);
                    startActivity(i);
//                }

            }
        });

        btnTray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TabAdapter.hMapPlaceHolder.size() > 0) {
                    if (PlaceholderFragment.type == AppConstants.TYPE_GRID_VIEW) {
                        btnTray.setAdjustViewBounds(true);

//                        btnTray.setPadding(10, 10, 10, 10);
                        btnTray.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        btnTray.setBackgroundResource(R.drawable.menu_listview);
                        PlaceholderFragment.type = AppConstants.TYPE_LIST_VIEW;

                    } else {
//                        btnTray.setPadding(10, 10, 10, 10);
                        btnTray.setAdjustViewBounds(true);
                        btnTray.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        btnTray.setBackgroundResource(R.drawable.ic_menu_gridview);
                        PlaceholderFragment.type = AppConstants.TYPE_GRID_VIEW;
                    }
                    int position = tabLayout.getSelectedTabPosition();
                    mTabPagerAdapter.updateTabView(position);
                }
            }
        });

        tvTotalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent trayIntent = new Intent(StoreActivity.this, TrayOrderSummary.class);
//                overridePendingTransition(R.anim.left_enter, R.anim.right_exit);
                startActivity(trayIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isSearchLayoutShown) {
            Animation slideDown = AnimationUtils.loadAnimation(mContext,
                    R.anim.slide_in_up);
            relSearch.startAnimation(slideDown);
            relSearch.setVisibility(View.GONE);
            isSearchLayoutShown = false;
        } else {
                super.onBackPressed();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private View.OnClickListener clicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivSearch:
                    // Intent searchIntent = new Intent(RestaurantList.this, SearchRestaurantActivity.class);
                    //startActivity(searchIntent);
                    //overridePendingTransition(R.anim.slide_down, R.anim.stay);
                    relSearch.setVisibility(View.VISIBLE);
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.scroll_down);
                    slideDown.setInterpolator((new AccelerateDecelerateInterpolator()));
                    slideDown.setFillAfter(true);
                    relSearch.startAnimation(slideDown);
                    isSearchLayoutShown = true;
                    //llLocation.setVisibility(View.GONE);
                    break;
            }
        }
    };

    private void getIntentValues() {
        Intent i = getIntent();
        if (i.hasExtra("RESTAURANT")) {
            resObj = (Restaurant) i.getSerializableExtra("RESTAURANT");
            if (resObj != null) {
                resID = resObj.getRestaurantId();
                //Toast.makeText(mContext,resID+"",Toast.LENGTH_SHORT).show();
//                resName = resObj.getResName();
                setTitle(resObj.getResName());
            }
        }
    }

    public static void updateBatch(View v){
//            tvOrderBadgeCount.setText("" + arrUserOtderItems.size());
//            tvOrderBadgeCount.setVisibility(View.VISIBLE);

    }

    public static void updateFooter(int price) {
        if (price > 0) {

            String priceinstring = String.valueOf(price);

//            tvOrderBadgeCount.setText("" + arrUserOtderItems.size());
//            tvOrderBadgeCount.setVisibility(View.VISIBLE);
//            tvTotalPrice.setText("");
            if(priceinstring.length()<2) {
                tvTotalPrice.setText(mContext.getResources().getString(R.string.Rs) +
                        " " + String.valueOf(price));
            }
            if(priceinstring.length()>2){
                tvTotalPrice.setText(mContext.getResources().getString(R.string.Rs) +
                        " " + String.valueOf(price));
            }
        } else {
//            tvOrderBadgeCount.setVisibility(View.GONE);
            tvTotalPrice.setText(mContext.getString(R.string.Rs) + " 0");
        }
    }

    private class GetRestauarantCategory extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
//            ParseQuery<ParseObject> queryRes = new ParseQuery<ParseObject>("Restaurant");
//            queryRes.Without("objectId", resID);

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Food");
            query.include("FoodCategory");
//            query.whereEqualTo("FoodCategory",ParseObject.createWithoutData("FoodCategory",null));
            query.whereEqualTo("ResId", ParseObject.createWithoutData("Restaurant",resID));
            try {
                ob = query.find();
                if (ob != null && ob.size() > 0) {
                    for (ParseObject foodObj : ob) {
                        ParseObject fdCatObj = foodObj.getParseObject("FoodCategory");
                        arrParseObject.add(fdCatObj);
                    }
                }
                if (arrParseObject != null && arrParseObject.size() > 0) {
                    for (ParseObject foodCategoryObj : arrParseObject) {
                        String foodCategoryId = foodCategoryObj.getObjectId();
                        String foodCategoryName = foodCategoryObj.fetchIfNeeded().getString("FoodCategoryName");
                        FoodCategory fCategory = new FoodCategory(foodCategoryId, foodCategoryName);
                        logUtils.log(foodCategoryId);
                        if (!categoryIdList.contains(foodCategoryId)) {
                            arrFoodCategory.add(fCategory);
                            categoryIdList.add(foodCategoryId);
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (arrFoodCategory != null && arrFoodCategory.size() > 0) {
                mTabPagerAdapter = new TabAdapter(getSupportFragmentManager(), arrFoodCategory, mContext, resObj, imageLoader);
                mViewPager.setAdapter(mTabPagerAdapter);
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.setupWithViewPager(mViewPager);
            }
            utils.hideLoader();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    private class GetFoodCategoryAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

/*
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "Food");
            query.whereEqualTo("ResId", resID);
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> object, ParseException e) {
                    if (object == null) {
                        Log.d("score", "The getFirst request failed.");
                    } else {
                        ob = object;
                    }
                }
            });
*/

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "FoodCategory");
            try {
                ob = query.find();
            } catch (ParseException e) {
                logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (ob != null) {

                for (ParseObject foodCategoryObj : ob) {
                    String foodCategoryId = foodCategoryObj.getObjectId();
                    String foodCategoryName = (String) foodCategoryObj.get("FoodCategoryName");
                    FoodCategory fCategory = new FoodCategory(foodCategoryId, foodCategoryName);
                    logUtils.log(foodCategoryId);
                    arrFoodCategory.add(fCategory);
                }

                mTabPagerAdapter = new TabAdapter(getSupportFragmentManager(), arrFoodCategory,
                        mContext, resObj, imageLoader);
                mViewPager.setAdapter(mTabPagerAdapter);
                mViewPager.setOffscreenPageLimit(mTabPagerAdapter.getCount()-1);

                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.setupWithViewPager(mViewPager);
                mProgressDialog.dismiss();
            }
        }
    }

}
