package com.entree.entree.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.Customers;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantArray;
import com.entree.entree.constructors.UserLocation;
import com.entree.entree.utils.LocationAddress;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class MapLocation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener {

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    GoogleMap googleMap;
    TextView txtCount, txtHeader;
    List<ParseObject> ob;
    ProgressDialog mProgressDialog;
    Context mContext;
    LinearLayout llSummary;
    int resCount = 0;
    private Button btnGetEntree;
    UserLocation userLocation = null;
    private ArrayList<Restaurant> arrRestaurant = new ArrayList<Restaurant>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        mContext = this;
        utils = new Utils(mContext);getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.content_map_location);


        btnGetEntree = (Button)findViewById(R.id.btnGetEntree);
        btnGetEntree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MapLocation.this, RestaurantList.class);
                startActivity(i);
                overridePendingTransition(R.anim.left_enter,R.anim.right_exit);
            }
        });
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
       // txtCount = (TextView) findViewById(R.id.txtCount);
//        txtHeader = (TextView) findViewById(R.id.tvHeader);
        llSummary = (LinearLayout) findViewById(R.id.llSummary);
        /*txtCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SaveCustomerData().execute();
            }
        });*/
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //setContentView(R.layout.activity_location_google_map);
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        googleMap = fm.getMap();
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMapClickListener(this);
        if (getIntent().hasExtra("LOCATION")) {
            mCurrentLocation = (Location) getIntent().getParcelableExtra("LOCATION");
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(),
                    getApplicationContext(), new GeocoderHandler());
            new GetRestaturantData().execute();
            //addMarker();
            //txtHeader.setText(getIntent().getStringExtra("CLocation"));
        }

    }

    private void setUtils() {
        utils = new Utils(mContext);
        // logUtils = new LogUtilities(mContext);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
*/
    private Utils utils = null;

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Toast.makeText(mContext, "" + id, Toast.LENGTH_SHORT).show();
        utils.navigationOnClick(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();

        // following four lines requires 'Google Maps Android API Utility Library'
        // https://developers.google.com/maps/documentation/android/utility/
        // I have used this to display the time as title for location markers
        // you can safely comment the following four lines but for this info
        IconGenerator iconFactory = new IconGenerator(this);
        iconFactory.setStyle(IconGenerator.STYLE_PURPLE);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng);
        Marker mapMarker = googleMap.addMarker(options);
        mapMarker.setDraggable(true);
        long atTime = mCurrentLocation.getTime();
//        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date(atTime));
//        mapMarker.setTitle(mLastUpdateTime);
//        Log.d(TAG, "Marker added.............................");
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    protected void startLocationUpdates() {
//        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
//                mGoogleApiClient, mLocationRequest, this);
//        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onStart() {
        super.onStart();
        ///Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        //Log.d(TAG, "onStop fired ..............");
        mGoogleApiClient.disconnect();
        //Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
//        mCurrentLocation = location;
//        addMarker();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

        MarkerOptions options = new MarkerOptions().snippet("new location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer))
                .anchor(0.5f, 0.5f);
        options.position(latLng);
        // options.title(latLng.latitude,latLng.longitude);
        googleMap.clear();
        googleMap.addMarker(options);
        new GetRestaturantData(true, latLng).execute();
        //  googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,
        //        13));

        LocationAddress locationAddress = new LocationAddress();
        locationAddress.getAddressFromLocation(latLng.latitude, latLng.longitude,
                getApplicationContext(), new GeocoderHandler());
    }

    private class GetRestaturantData extends AsyncTask<Void, Void, Void> {
        Boolean overrideLocation = false;
        LatLng newLatLng;

        public GetRestaturantData(boolean overideLocation, LatLng latLng) {
            this.overrideLocation = overideLocation;
            this.newLatLng = latLng;
        }

        public GetRestaturantData() {
            this.overrideLocation = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            arrRestaurant = new ArrayList<Restaurant>();
            // Locate the class table named "Country" in Parse.com
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                    "Restaurant");

            try {
//                resCount = query.count();
                ob = query.find();
            } catch (ParseException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (ob != null) {
                for (ParseObject restaurant : ob) {
                    ParseFile ResBannerPhotoFile = (ParseFile) restaurant.get("ResBannerPhoto");
                    ParseFile ResPhotoFile = (ParseFile) restaurant.get("ResPhoto");
                    String Longitude = (String) restaurant.get("Longitude");
                    String ResSlogan = (String) restaurant.get("ResSlogan");
                    String ClosingTime = (String) restaurant.get("ClosingTime");
                    String ContactEmail = (String) restaurant.get("ContactEmail");
                    Boolean IsClosed = (Boolean) restaurant.get("IsClosed");
                    String Website = (String) restaurant.get("Website");
                    String FaxNo = (String) restaurant.get("FaxNo");
                    String Pincode = (String) restaurant.get("Pincode");
                    String ContactMobile = (String) restaurant.get("ContactMobile");
                    String LandlineNo = (String) restaurant.get("LandlineNo");
                    String Address2 = (String) restaurant.get("Address2");
                    Boolean IsAvailableForDelivery = (Boolean) restaurant.get("IsAvailableForDelivery");
                    String ResName = (String) restaurant.get("ResName");
                    String ResTypeName = (String) restaurant.get("ResTypeName");
                    String State = (String) restaurant.get("State");
                    String Address1 = (String) restaurant.get("Address1");
                    String Latitude = (String) restaurant.get("Latitude");
                    String OpeningTime = (String) restaurant.get("OpeningTime");
                    String ContactPerson = (String) restaurant.get("ContactPerson");
                    String City = (String) restaurant.get("City");
                    String ResBannerPhoto = "";
                    String ResPhoto = "";
                    if (ResBannerPhotoFile != null) {
                         ResBannerPhoto = ResBannerPhotoFile.getUrl();
                         ResPhoto = ResPhotoFile.getUrl();
                    }
                    String ResId = restaurant.getObjectId();

                    float[] results = new float[1];


                    LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    if (!overrideLocation)
                        Location.distanceBetween(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), Double.parseDouble(Latitude), Double.parseDouble(Longitude), results);
                    else
                        Location.distanceBetween(newLatLng.latitude, newLatLng.longitude, Double.parseDouble(Latitude), Double.parseDouble(Longitude), results);
                    float distanceInMeters = results[0];
                    boolean isWithin10km = distanceInMeters < 10000;

                    if (!overrideLocation)
                        addMarker();

                    if (isWithin10km) {
                        if (Latitude != null && Longitude != null &&
                                !Latitude.equalsIgnoreCase("") && !Longitude.equalsIgnoreCase("")) {

                            LatLng latLng = new LatLng(Double.parseDouble(Latitude), Double.parseDouble(Longitude));
                            MarkerOptions options = new MarkerOptions().snippet("Restaurent")
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer))
                                    .anchor(0.5f, 0.5f);
                            options.position(latLng);
                            options.title(ResName);
//                            googleMap.clear();
                            googleMap.addMarker(options);
                        }
                        Restaurant rest = new Restaurant(ResId, Longitude, ResSlogan, ClosingTime,
                                ContactEmail, IsClosed, Website,
                                FaxNo, Pincode, ContactMobile, LandlineNo,
                                Address2, ResPhoto, IsAvailableForDelivery, ResName,
                                ResTypeName, State, Address1, Latitude,
                                OpeningTime, ContactPerson, City, ResBannerPhoto);
                        arrRestaurant.add(rest);
                    }
                    if (!overrideLocation) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
                    } else {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
                    }

                }
            }
            resCount = arrRestaurant.size();
            llSummary.setVisibility(View.VISIBLE);
            if (resCount > 0) {

               // txtCount.setText("See " + resCount + " Restaurants");
            } else {
               /// txtCount.setText("No Restaurants near by");
            }


            mProgressDialog.dismiss();
        }
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    userLocation = (UserLocation) bundle.getSerializable("user_location");
                    break;
                default:
                    locationAddress = null;
            }
//            txtHeader.setText(locationAddress);
        }
    }

    private class CustomerDataHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {

            }
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
            if (resCount > 0) {
                CustomerAddresses custAdress = (CustomerAddresses) message.getData().getSerializable("Customer");

                RestaurantArray restArrObj = new RestaurantArray(arrRestaurant);
                Intent i = new Intent(mContext, RestaurantList.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable("RestaurentArray", restArrObj);
                i.putExtras(mBundle);
                i.putExtra("cLocation", custAdress.getAddressTitle());
                startActivity(i);
            }
        }
    }

    private class SaveCustomerData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                Customers customerobj = new Customers(currentUser, "TEST", "User", "+919991113332", "", currentUser.getEmail());
                customerobj.sendCustomerToParse(new CustomerDataHandler(), userLocation);
            }
        }
    }

}
