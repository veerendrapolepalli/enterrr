package com.entree.entree.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.EntryItem;
import com.entree.entree.constructors.Order;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.OrderItems;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantOrders;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.constructors.UserOrder;
import com.entree.entree.interfacelistener.Items;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Profile extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private static Context mContext;
    private static Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mContext = this;
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        utils = new Utils(mContext);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int tabPosition = 0;
        ArrayList<Items> items = new ArrayList<Items>();
        ArrayList<Order> arrOrder = new ArrayList<Order>();
        UserOrder userOrderObj = null;
        ArrayList<UserOrder> arrUserOrder = new ArrayList<UserOrder>();
        ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();


        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            tabPosition = getArguments().getInt(ARG_SECTION_NUMBER);
            View rootView = null;
            if (tabPosition == 1) {
                rootView = inflater.inflate(R.layout.fragment_profile, container, false);
                ImageView profilePic = (ImageView) rootView.findViewById(R.id.profile_image);
                TextView textView = (TextView) rootView.findViewById(R.id.section_label);
                textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            } else if (tabPosition == 2) {
                rootView = inflater.inflate(R.layout.fragment_order_summary, container, false);
                LinearLayout llOrderSummary = (LinearLayout) rootView.findViewById(R.id.llOrderSummary);
                TextView tvOrderNo = (TextView) rootView.findViewById(R.id.tvOrderNo);
                TextView tvDeliveryDate = (TextView) rootView.findViewById(R.id.tvDeliveryDate);
                TextView tvDeliveryTime = (TextView) rootView.findViewById(R.id.tvDeliveryTime);

                new GetOrderSummary(llOrderSummary, inflater, container,
                        tvOrderNo, tvDeliveryDate, tvDeliveryTime).execute();

            } else if (tabPosition == 3) {
                rootView = inflater.inflate(R.layout.fragment_profile, container, false);
            }
            return rootView;
        }


        private class GetOrderSummary extends AsyncTask<Void, Void, Void> {
            LinearLayout llOrderSummary;
            LayoutInflater inflater;
            ViewGroup container;
            TextView tvOrderNo;
            TextView tvDeliveryDate;
            TextView tvDeliveryTime;
            String orderNo;
            Date deliveryDate;
            String deliveryTime;


            public GetOrderSummary(LinearLayout llOrderSummary, LayoutInflater inflater,
                                   ViewGroup container, TextView tvOrderNo, TextView tvDeliveryDate,
                                   TextView tvDeliveryTime) {
                this.llOrderSummary = llOrderSummary;
                this.inflater = inflater;
                this.container = container;
                this.tvDeliveryDate = tvDeliveryDate;
                this.tvOrderNo = tvOrderNo;
                this.tvDeliveryTime = tvDeliveryTime;

            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                utils.showLoader();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                String newstring = new SimpleDateFormat("dd MMM, yyyy").format(deliveryDate);
                tvDeliveryDate.setText(newstring);
                tvOrderNo.setText("#" + orderNo);
                tvDeliveryTime.setText(deliveryTime);
                if (items.size() > 0) {
                    for (Items i : items) {
                        View v = null;
                        if (i.isSection()) {
                            v = bindRestItemView(inflater, container, i);
                        } else {
                            v = bindFoodItemView(inflater, container, i);
                        }
                        if (v != null)
                            llOrderSummary.addView(v);
                    }

                }
                utils.hideLoader();
            }

            @Override
            protected Void doInBackground(Void... params) {
                getOrderStats();
                return null;
            }

            private View bindRestItemView(LayoutInflater inflater, ViewGroup container, Items i) {
                View restItemView = inflater.inflate(R.layout.order_summary_restaurant, container, false);
                if (restItemView != null) {
                    TextView tvRestName = (TextView) restItemView.findViewById(R.id.tvItemCategory);
                    TextView tvItemtotal = (TextView) restItemView.findViewById(R.id.itemtotal);

                    SectionItem si = (SectionItem) i;
                    tvRestName.setText(si.getTitle());
                    tvItemtotal.setText(mContext.getResources().getString(R.string.Rs) + " " + si.getPrice());
                }
                return restItemView;
            }

            private View bindFoodItemView(LayoutInflater inflater, ViewGroup container, Items i) {
                View foodItemView = inflater.inflate(R.layout.order_summary_food, container, false);
                if (foodItemView != null) {
                    TextView tvQty = (TextView) foodItemView.findViewById(R.id.tvQty);
                    TextView tvFoodName = (TextView) foodItemView.findViewById(R.id.tvFoodName);
                    TextView tvFoodPrice = (TextView) foodItemView.findViewById(R.id.tvFoodPrice);

                    EntryItem ei = (EntryItem) i;
                    OrderItem oItem = ei.getOrderItem();
                    if (oItem != null) {
                        tvQty.setText(String.valueOf(oItem.getQty()));
                        tvFoodName.setText(oItem.getFoodName());
                        tvFoodPrice.setText(mContext.getResources().getString(R.string.Rs) + " " + oItem.getItemTotal());
                    }
                }
                return foodItemView;
            }

            private List<ParseObject> getCustomerOrders(ParseObject custParseObject) {
                List<ParseObject> custOrderParseObj = null;
                ParseQuery<ParseObject> queryOrders = new ParseQuery<ParseObject>(
                        "Orders");
                queryOrders.whereEqualTo("CustomerId", custParseObject);
                queryOrders.whereEqualTo("OrderStatusLatest", "New Order");
                queryOrders.whereEqualTo("IsOrderClosed", false);
                queryOrders.setLimit(1);
                try {
                    custOrderParseObj = queryOrders.find();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return custOrderParseObj;

            }

            private ArrayList<OrderItem> parseOrderItemsObject(List<ParseObject> ob) {

                for (ParseObject orderItemsObj : ob) {

                    String orderItemId = orderItemsObj.getObjectId();
                    ParseObject orderObj = orderItemsObj.getParseObject("OrderId");
                    ParseObject resObj = orderItemsObj.getParseObject("ResId");
                    String resName = orderItemsObj.getString("ResName");

                    if (resName.equalsIgnoreCase("")) {
                        ParseQuery<ParseObject> queryRest = new ParseQuery<ParseObject>("Restaurant");
                        queryRest.whereEqualTo("objectId", resObj.getObjectId());
                        try {
                            List<ParseObject> restArray = queryRest.find();
                            resObj = restArray.get(0);
                            if (resObj.containsKey("ResName"))
                                resName = resObj.getString("ResName");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        //resName = parseResObj.getString("ResName");
                    }

                    String resId = resObj.getObjectId();

                    String foodName = orderItemsObj.getString("FoodName");
                    String foodDesc = orderItemsObj.getString("FoodDesc");
                    String foodCategory = orderItemsObj.getString("FoodCategory");

                    double qty = orderItemsObj.getDouble("Qty");
                    double price = orderItemsObj.getDouble("Price");
                    double itemTotal = orderItemsObj.getDouble("ItemTotal");

                    String vegOnly = orderItemsObj.getString("VegOnly");
                    String foodNote = orderItemsObj.getString("FoodNote");

                    OrderItem oItem = new OrderItem(orderItemId, orderObj, resObj, foodName,
                            foodDesc, foodCategory, vegOnly, (int) qty, (int) price, (int) itemTotal, foodNote, resName);
                    oItem.setResID(resId);
                    arrOrderItem.add(oItem);
                }
                return arrOrderItem;
            }

            private void getOrderStats() {
                List<ParseObject> listCustomerOrder = null;
                List<ParseObject> arrOrderItems = null;
                ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();
                ArrayList<String> arrRest = new ArrayList<String>();

                ParseObject orderObj = null;
                ParseObject customersObj = Utils.getLoggedInCustomer();
                if (customersObj != null) {
                    listCustomerOrder = getCustomerOrders(customersObj);
                    if (listCustomerOrder != null && listCustomerOrder.size() > 0) {
                        orderObj = listCustomerOrder.get(0);

                        orderNo = orderObj.getString("OrderNo");
                        deliveryDate = orderObj.getDate("DeliveryDate");
                        deliveryTime = orderObj.getString("DeliveryTime");


                        arrOrderItems = Utils.getOrderItems(orderObj);
                        parseOrderObject(listCustomerOrder);
                        if (arrOrderItems.size() > 0) {
                            arrRest = getUniqueRest(arrOrderItems);
                            generateRestOrder(arrRest);
                        }
                    }
                } else {

                }
            }

            private void generateRestOrder(ArrayList<String> arrRest) {
                ArrayList<RestaurantOrders> arrRestOrders = new ArrayList<RestaurantOrders>();
                ArrayList<Items> orderItems = new ArrayList<Items>();
                for (String resName : arrRest) {
                    double total = 0;
                    for (OrderItem oItem : arrOrderItem) {
                        String parseOrederResName = oItem.getRestName();
                        if (parseOrederResName != null && !parseOrederResName.equalsIgnoreCase("")) {
                            if (resName.equalsIgnoreCase(parseOrederResName)) {
                                total = total + oItem.getItemTotal();
                                orderItems.add(new EntryItem(oItem));
                            }
                        }

                    }
                    items.add(new SectionItem(resName, String.valueOf(total)));
                    items.addAll(orderItems);
                }
            }

            private ArrayList<String> getUniqueRest(List<ParseObject> arrOrderItems) {
                ArrayList<String> arrRest = new ArrayList<String>();
                for (ParseObject pObj : arrOrderItems) {
                    String resName = pObj.getString("ResName");
                    ParseObject resObj = pObj.getParseObject("ResId");
                    if (resName.equalsIgnoreCase("")) {
                        if (!resObj.containsKey("ResName")) {
                            ParseQuery<ParseObject> queryRest = new ParseQuery<ParseObject>("Restaurant");
                            queryRest.whereEqualTo("objectId", resObj.getObjectId());
                            try {
                                List<ParseObject> restArray = queryRest.find();
                                resObj = restArray.get(0);
                                if (resObj.containsKey("ResName"))
                                    resName = resObj.getString("ResName");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            resName = resObj.getString("ResName");
                        }

                        //resName = parseResObj.getString("ResName");
                    }

                    if (!arrRest.contains(resName)) {
                        arrRest.add(resName);
                    }
                }
                return arrRest;
            }

            private void parseOrderObject(List<ParseObject> ob) {
                ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();
                for (ParseObject orderObj : ob) {
                    String orderId = orderObj.getObjectId();
                    String orderNo = orderObj.getString("OrderNo");
                    ParseObject custObj = (ParseObject) orderObj.get("CustomerId");
                    ParseObject employeeObj = (ParseObject) orderObj.get("EmployeeId");
                    String deviceType = orderObj.getString("DeviceType");
                    double subTotalAmount = orderObj.getDouble("SubTotalAmount");
                    double taxAmount = orderObj.getDouble("TaxAmount");
                    double driverTipAmount = orderObj.getDouble("DriverTipAmount");
                    double totalAmount = orderObj.getDouble("TotalAmount");
                    String paymentMode = orderObj.getString("PaymentMode");
                    String orderStatusLatest = orderObj.getString("OrderStatusLatest");
                    String deliveryFirstName = orderObj.getString("DeliveryFirstName");
                    String deliveryLastName = orderObj.getString("DeliveryLastName");
                    String deliveryAddress1 = orderObj.getString("DeliveryAddress1");
                    String deliveryAddress2 = orderObj.getString("DeliveryAddress2");
                    String deliveryLandmark = orderObj.getString("DeliveryLandmark");
                    String deliveryCity = orderObj.getString("DeliveryCity");
                    String deliveryState = orderObj.getString("DeliveryState");
                    String deliveryPincode = orderObj.getString("DeliveryPincode");
                    String deliveryLatitude = orderObj.getString("DeliveryLatitude");
                    String deliveryLongitude = orderObj.getString("DeliveryLongitude");
                    Date createdDate = orderObj.getCreatedAt();
                    Boolean isOrderClosed = orderObj.getBoolean("IsOrderClosed");
                    Date deliveryDate = orderObj.getDate("DeliveryDate");
                    String deliveryTime = orderObj.getString("DeliveryTime");

                    List<ParseObject> listOrderitems = Utils.getOrderItems(orderObj);
                    arrOrderItem = parseOrderItemsObject(listOrderitems);

                    Order order = new Order(orderId, orderNo, custObj, employeeObj, deviceType, subTotalAmount,
                            taxAmount, driverTipAmount, totalAmount, paymentMode, orderStatusLatest,
                            deliveryFirstName, deliveryLastName, deliveryAddress1, deliveryAddress2,
                            deliveryLandmark, deliveryCity, deliveryState, deliveryPincode, deliveryLatitude,
                            deliveryLongitude, createdDate, isOrderClosed, deliveryDate, deliveryTime);
                    arrOrder.add(order);
                    userOrderObj = new UserOrder(order, arrOrderItem);
                    arrUserOrder.add(userOrderObj);
                }
            }

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Track";
                case 1:
                    return "Order Summary";
                case 2:
                    return "Chat";
            }
            return null;
        }
    }
}
