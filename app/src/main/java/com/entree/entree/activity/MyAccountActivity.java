package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.async.GetUserSavedAddress;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.interfacelistener.GetCustomerLocationListener;
import com.entree.entree.utils.EditTextFocusChangeListener;
import com.entree.entree.utils.ImagePicker;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyAccountActivity extends BaseActivity implements
        GetCustomerLocationListener {

    Context mContext;
    Utils utils;
    private ImageView ivProfilePic;
    private EditText tvname, tvEmail, tvPhone;
    private TextView  tvNameEdit, tvAddEdit;
    private EditText etName, etEmail, etPhone, etcode;
    private Button btnSaveName, btnResend;
    private ListView lvAddress;
    RelativeLayout relUser;
    LinearLayout llEditLayout, llUserDetails;
    ParseObject customersObj;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    ScrollView scrollViewLayout;

    private static final int PICK_IMAGE_ID = 0001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        setContentView(R.layout.content_my_account);
        mContext = this;
        init();
        initListener();
        new GetUserSavedAddress(this,this).execute();
        // new GetUserAddress().execute();
    }

    public void onPickImage(View view) {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(this);
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_ID:
                Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                // TODO use bitmap
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void init() {
        tvname = (EditText) findViewById(R.id.tvUserName);
        tvEmail = (EditText) findViewById(R.id.tvUserEmail);
        tvPhone = (EditText) findViewById(R.id.tvUserPhone);
        ivProfilePic = (ImageView) findViewById(R.id.profile_image);
        tvNameEdit = (TextView) findViewById(R.id.tvEditAction);
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhone = (EditText) findViewById(R.id.etphone);
        etcode = (EditText) findViewById(R.id.etcode);
        lvAddress = (ListView) findViewById(R.id.lvUserAddress);
        relUser = (RelativeLayout) findViewById(R.id.lluser);
        llUserDetails = (LinearLayout) findViewById(R.id.llUserDetails);
        llEditLayout = (LinearLayout) findViewById(R.id.llUserDetailsEdit);
        scrollViewLayout = (ScrollView) findViewById(R.id.scrollViewLayout);
    }

    private void initListener() {
        tvNameEdit.setOnClickListener(onclick);
        ivProfilePic.setOnClickListener(onclick);
        EditTextFocusChangeListener listener = new EditTextFocusChangeListener(scrollViewLayout);
        etcode.setOnFocusChangeListener(listener);
        etName.setOnFocusChangeListener(listener);
        etEmail.setOnFocusChangeListener(listener);
        etPhone.setOnFocusChangeListener(listener);
    }

    private View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.tvEditAction:
                   /* Toast.makeText(mContext, "this is clicked", Toast.LENGTH_SHORT).show();
                    llUserDetails.setVisibility(View.GONE);
                    scrollViewLayout.setVisibility(View.VISIBLE);
                    Animation bottomUp = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_up);
                    llEditLayout.startAnimation(bottomUp);
                    llEditLayout.setVisibility(View.VISIBLE);*/
//                    Intent editIntent = new Intent(MyAccountActivity.this,EditProfileActivity.class);
//                    startActivity(editIntent);
                    break;
                case R.id.tvEditaddAction:

                    break;
            }
        }
    };



    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public void onCustomerLocationFetched(ArrayList<CustomerAddresses> arrCustomerAddress) {
        CustomerAddressListAdapter customerAddressListAdapter =
                new CustomerAddressListAdapter(mContext, arrCustomerAddress);
        lvAddress.setAdapter(customerAddressListAdapter);

    }

    private class GetUserAddress extends AsyncTask<Void, Void, Void> {
        List<ParseObject> ob = null;
        List<ParseObject> listCustomerAddress = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                currentUser.getObjectId();
            }
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("UserId", currentUser);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size() > 0) {
                customersObj = (ParseObject) ob.get(0);
                ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                        "CustomerAddresses");
                queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
                queryCustomerAddr.setLimit(3);

                try {
                    listCustomerAddress = queryCustomerAddr.find();
                } catch (ParseException e) {
                    //logUtils.log(e.getMessage());
                    e.printStackTrace();
                }
                if (listCustomerAddress != null && listCustomerAddress.size() > 0) {
                    for (ParseObject custAddrObj : listCustomerAddress) {
                        String addressId = custAddrObj.getObjectId();
                        ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                        String firstName = custAddrObj.getString("FirstName");
                        String lastName = custAddrObj.getString("LastName");
                        String addressTitle = custAddrObj.getString("AddressTitle");
                        String address1 = custAddrObj.getString("Address1");
                        String address2 = custAddrObj.getString("Address2");
                        String landMark = custAddrObj.getString("Landmark");
                        String city = custAddrObj.getString("City");
                        String state = custAddrObj.getString("State");
                        String pincode = custAddrObj.getString("Pincode");
                        String latitude = custAddrObj.getString("Latitude");
                        String longitude = custAddrObj.getString("Longitude");

                        CustomerAddresses custAddress = new CustomerAddresses(addressId, customersObj,
                                firstName, lastName, addressTitle, address1, address2, landMark,
                                city, state, pincode, latitude, longitude);
                        arrCustAddr.add(custAddress);
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCustAddr != null && arrCustAddr.size() > 0) {
                CustomerAddressListAdapter customerAddressListAdapter =
                        new CustomerAddressListAdapter(mContext, arrCustAddr);

                lvAddress.setAdapter(customerAddressListAdapter);
            }
            utils.hideLoader();
        }
    }



}
