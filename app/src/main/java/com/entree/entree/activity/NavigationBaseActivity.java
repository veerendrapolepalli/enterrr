package com.entree.entree.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import com.entree.entree.R;
import com.entree.entree.fragments.NavigationFragment;


/**
 * Created by vpol0001 on 5/1/2016.
 */
public class NavigationBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout mDrawerLayout;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        if (layoutResID != R.layout.base_activity) {
            super.setContentView(R.layout.base_activity);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            ViewGroup contentContainer = (ViewGroup) findViewById(R.id.content_container);
            getLayoutInflater().inflate(layoutResID,contentContainer,true);
            actionBarDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name,R.string.app_name);
            mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
            if(getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
            attachNavigationFragment();
        } else {
            super.setContentView(layoutResID);
        }
    }
    ActionBarDrawerToggle actionBarDrawerToggle;


    public void attachNavigationFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.navigation_drawer_container,new NavigationFragment());
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawerLayout.openDrawer(GravityCompat.START);
        return super.onOptionsItemSelected(item);
    }
}
