package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.SnackBarUtils;
import com.entree.entree.utils.Utils;
import com.entree.entree.views.RobottoCondencedBold;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUp extends AppCompatActivity {
    private Context mContext;
    private static final int BTN_SIGN_UP = 1;
    private Utils utils = null;
    private LogUtilities logUtils = null;
    private RobottoCondencedBold btnSignUp;
    private EditText etEmail, etPwd, etName, etMobileNo;
    private CoordinatorLayout coordinatorLayout;
    private SnackBarUtils snackbar;
    private ProgressBar progressBar;
    Typeface font_avenir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        setContentView(R.layout.activity_sign_up);
        overridePendingTransition(R.anim.bottom_top, R.anim.top_bottom);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        font_avenir = Typeface.createFromAsset(mContext.getAssets(), "fonts/AvenirLTStd-Medium.otf");
        setUtils();
        setUI();
        initListener();
    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void setUI() {
        btnSignUp = (RobottoCondencedBold) findViewById(R.id.btnSignUp);
        etEmail = (EditText) findViewById(R.id.input_email);
        etPwd = (EditText) findViewById(R.id.input_password);
        etName = (EditText) findViewById(R.id.input_name);
        etMobileNo = (EditText) findViewById(R.id.input_mobileno);

        btnSignUp.setTypeface(font_avenir);
        etEmail.setTypeface(font_avenir);
        etPwd.setTypeface(font_avenir);
        etName.setTypeface(font_avenir);
        etMobileNo.setTypeface(font_avenir);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void initListener() {
        btnSignUp.setId(BTN_SIGN_UP);
        btnSignUp.setOnClickListener(onClickListener);
        setupUI(findViewById(R.id.coordinatorLayout));
        etMobileNo.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    utils.hideKeyBoard();
                    btnSignUp.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case BTN_SIGN_UP:
                    signUpUser();
                    break;
                default:
                    break;
            }
        }
    };

    private void signUpUser() {
        final String email = etEmail.getText().toString();
        String pwd = etPwd.getText().toString();
        String name = etName.getText().toString();
        String phoneNo = etMobileNo.getText().toString();

        if (utils.checkNetworkAvailabilityWithToast(coordinatorLayout, true)) {
            if (formValidate(email, pwd, name, phoneNo)) {
                showProgressbar();
                final ParseUser user = new ParseUser();
                user.setUsername(name);
                user.setPassword(pwd);
                user.setEmail(email);

                user.put("MobileNo", phoneNo);


                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        hideprogressBar();
                        if (e == null) {
                            // Hooray! Let them use the app now.
                            user.saveInBackground();
                            Intent i = new Intent(mContext, UserLocationActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra("Email", email);
                            startActivity(i);
                            finish();
                        } else {
                            snackbar = new SnackBarUtils(mContext, coordinatorLayout);
                            snackbar.showSnackbar(e.getMessage());
                            logUtils.log(e.getMessage());
                            // Sign up didn't succeed. Look at the ParseException
                            // to figure out what went wrong
                        }
                    }
                });
            }
        }
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    utils.hideKeyBoard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private boolean formValidate(String email, String pass, String name, String mbileNo) {
        if (email == null || email.equalsIgnoreCase("")) {
            etEmail.setError("Email cannot be left empty");
            return false;
        }
        if (!utils.isValidEmail(email)) {
            etEmail.setError("Invalid Email");
            return false;
        }
        if (pass == null || pass.equalsIgnoreCase("")) {
            etPwd.setError("Password cannot be left empty");
            return false;
        }
        if (pass == null || pass.equalsIgnoreCase("")) {
            etPwd.setError("Password cannot be left empty");
            return false;
        }
        if (name == null || name.equalsIgnoreCase("")) {
            etName.setError("Forgot to enter your name");
            return false;
        }
       /* if (!cnfPwd.equals(pass)) {
            etConfPwd.setError("Password mismatch");
            return false;
        }*/
        return true;
    }

    private void showProgressbar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideprogressBar() {
        progressBar.setVisibility(View.GONE);
    }

}
