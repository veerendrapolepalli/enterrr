package com.entree.entree.activity;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.Environment;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.classes.Amount;
import com.citrus.sdk.classes.CitrusException;
import com.citrus.sdk.classes.Month;
import com.citrus.sdk.classes.Year;
import com.citrus.sdk.payment.CreditCardOption;
import com.citrus.sdk.payment.NetbankingOption;
import com.citrus.sdk.payment.PaymentType;
import com.citrus.sdk.response.CitrusError;
import com.entree.entree.R;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.CustomerParseObject;
import com.entree.entree.constructors.Order;
import com.entree.entree.constructors.UserLocation;
import com.entree.entree.interfacelistener.OrderCompleteListener;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PaymentActivity extends BaseActivity {

    private ArrayAdapter<String> adapter;
    AutoCompleteTextView autoFill = null;
    private String bankId, bankNme;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public EditText etName,etCardNo,etMonth,etCvv,etYear;

    boolean isCreditOrDebit = false;
    boolean isNetBanking = false;
    boolean isNativewallet = false;
    boolean isCod = false;

    public static String OrderId = " ";
    public static String ObjectId = " ";

    String arrIds[] = {"CID016", "CID002", "CID080", "CID046", "CID021", "CID019", "CID051", "CID045",
            "CID023", "CID003", "CID025", "CID024", "CID006", "CID026", "CID009", "CID010", "CID001", "CID050",
            "CID011", "CID008", "CID027", "CID028", "CID029", "CID031", "CID033", "CID032", "CID044", "CID036", "CID005",
            "CID013", "CID012", "CID014", "CID015", "CID043", "CID037", "CID007", "CID041", "CID042", "CID004",
            "CID053", "CID070", "CID090", "CID064", "CID038"};
    String arrBanks[] = {
            "Andhra Bank", "AXIS Bank", "Axis Corporate Bank", "Bank Of Baroda",
            "Bank of Maharashtra", "Bank of India", "Canara Bank", "Catholic Syrian Bank",
            "Central Bank of India", "CITI Bank", "Corporation Bank", "City Union Bank",
            "DEUTSCHE Bank", "DCB Bank Personal", "Federal Bank", "HDFC Bank", "ICICI Bank",
            "ICICI Corporate Bank", "IDBI Bank", "Indian Bank", "Indian Overseas Bank", "Induslnd Bank",
            "ING VYSA", "Karnataka Bank", "Kotak Mahindra Bank", "Karur Vysya Bank", "PNB Retail", "PNB Corporate",
            "SBI Bank", "State Bank of Bikaner and Jaipur", "State Bank of Hyderabad", "State Bank of Mysore", "State Bank of Travancore",
            "State Bank of Patiala", "South Indian Bank", "Union Bank Of India", "United Bank of India", "Vijaya Bank", "YES Bank", "Cosmos Bank",
            "UCO Bank", "HSBC", "RBL", "SCB"
    };
    private ExpandableListView exPaymentMenu;
    private TextView totalPrice;
    private String priceAmt;

    LinearLayout mCreditCardLayout;
    LinearLayout mCreditCardContent;
    LinearLayout llcod;
    ValueAnimator mAnimator;
    private boolean contentShown = false;
    private Context mContext;
    private UserLocation uLocation;
    ParseObject customersObj, employeeObj;
    private String CustomerId = "";
    JSONObject orderIdObj;
    Boolean reOrder = false;
    HashMap<String, List<String>> listDataChild;
    Utils utils = null;
    private CustomerParseObject customerParseObject;
    List<ParseObject> ob = null;
    OrderCompleteListener orderCompleteListener;

    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    CitrusClient citrusClient;

    private RelativeLayout llproceed;

    LogUtilities logutils=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_payment);
        mContext = this;
        logutils = new LogUtilities(mContext);
        pref = getSharedPreferences("MyPref",0);
        initCitrus();
        init();
        setupUI(findViewById(R.id.drawer_layout));
        parseIntent();
        initUtils();
        initListener();
//        new GetOrderNumber().execute();
    }

    private void initCitrus() {
        citrusClient = CitrusClient.getInstance(mContext);
        citrusClient.init("i2hb68bqsq-signup",
                "8d91a4ea63d07191364d26b1ad52bef9",
                "i2hb68bqsq-signin",
                "2af00262067c3202fad7673c430f5bb6",
                "eatentree",
                Environment.SANDBOX);
        citrusClient.enableLog(true);
    }

    private void initUtils() {
        utils = new Utils(mContext);
    }

    private void parseIntent() {
        Intent i = getIntent();

        if (i.hasExtra("priceAmt")) {
            priceAmt = i.getStringExtra("priceAmt").toString();
        } else {
            priceAmt = "0";
        }
        if (i.hasExtra("CustomerId")) {
            CustomerId = i.getStringExtra("CustomerId").toString();
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("objectId", CustomerId);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size() > 0) {
                customersObj = (ParseObject) ob.get(0);
            }
        }
        if (i.hasExtra("Reorder")) {
            reOrder = i.getBooleanExtra("Reorder", false);
        }
//        Bundle extras = i.getExtras();
//        if(extras!=null && extras.containsKey("CustomerParseObject")) {
//            customerParseObject = (CustomerParseObject) extras.getSerializable("CustomerParseObject");
//        }

        uLocation = (UserLocation) TrayContactActivity.userLocation;

    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    utils.hideKeyBoard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private void init() {
        prepareListData();

        exPaymentMenu = (ExpandableListView) findViewById(R.id.expandableListMenu);
        listAdapter = new ExpandableListAdapter(this, listDataHeader);
        // setting list adapter
        exPaymentMenu.setAdapter(listAdapter);
        /*llcod = (LinearLayout) findViewById(R.id.llCod);
        llcod.setOnClickListener(onclick);
        mCreditCardLayout = (LinearLayout) findViewById(R.id.llCreditCard);
        mCreditCardContent = (LinearLayout) findViewById(R.id.llCreditCardContent);*/
        priceAmt = getIntent().getExtras().getString("Total_Price");
        priceAmt = getResources().getString(R.string.Rs) + " " + priceAmt;
        totalPrice = (TextView) findViewById(R.id.tvtotalprice);
        llproceed = (RelativeLayout) findViewById(R.id.llproceed);
        llproceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCreditOrDebit) {
                    makeCardPayment();
                } else if (isNetBanking) {
                    proceedToInternetBanking();
                } else if (isNativewallet) {

                } else if (isCod) {
                    // createAndShowAlertDialog("You Have Selected Cash On Delivery");
                }
            }
        });
        exPaymentMenu.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                switch (groupPosition) {
//                    case 1:
//                        proceedToInternetBanking();
//                        break;
                    case 3:
                        createAndShowAlertDialog("You Have Selected Cash On Delivery");
                        break;
                }
                return false;
            }
        });

        exPaymentMenu.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                return true;
            }
        });
        /*mCreditCardContent.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        mCreditCardContent.getViewTreeObserver().removeOnPreDrawListener(this);
                        mCreditCardContent.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        mCreditCardContent.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(0, mCreditCardContent.getMeasuredHeight());
                        return true;
                    }
                });


        mCreditCardLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!contentShown) {
                    expand();
                } else {
                    collapse();
                }
            }
        });*/
    }

    private void initListener() {
        orderCompleteListener = new OrderCompleteListener() {
            @Override
            public void onOrderSynced() {

                editor = pref.edit();

                editor.putBoolean("orderPlaced", true);
                editor.putString("OrderId",OrderId);
                editor.commit();

                RestaurantList.tvTrackOrder.setVisibility(View.VISIBLE);

                Intent i = new Intent(mContext, RestaurantList.class);
                i.putExtra("ORDER_COMPLETE", true);
                i.putExtra("ORDER_ID",OrderId);
                i.putExtra("OBJECT_ID",ObjectId);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                EntreeApplication.arrUserOtderItems.clear();
            }
        };
    }


    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        // Adding child data
        listDataHeader.add("CREDIT/DEBIT CARD");
        listDataHeader.add("NETBANKING");
        listDataHeader.add("NATIVE WALLET");
        listDataHeader.add("CASH ON DELIVERY");

        List<String> cCard = new ArrayList<String>();
        List<String> netBank = new ArrayList<String>();
        List<String> wallet = new ArrayList<String>();
        List<String> cod = new ArrayList<String>();

        listDataChild.put(listDataHeader.get(0), cCard);
        listDataChild.put(listDataHeader.get(0), netBank);
        listDataChild.put(listDataHeader.get(0), wallet);
        listDataChild.put(listDataHeader.get(0), cod);
    }


    private void expand() {
        //set Visible
        contentShown = true;
        mCreditCardContent.setVisibility(View.VISIBLE);


        mAnimator.start();
    }

    private void collapse() {
        contentShown = false;
        int finalHeight = mCreditCardLayout.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, mCreditCardLayout.getHeight());

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                mCreditCardContent.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


   /* private View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.llCod:
                    createAndShowAlertDialog("You Have Selected Cash On Delivery");
                    break;
            }
        }
    };*/

    private void createAndShowAlertDialog(String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage("Total payable price " + priceAmt);
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO
                if (reOrder) {

                } else {
                    saveOrder();
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }




    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);


        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = mCreditCardLayout.getLayoutParams();
                layoutParams.height = value;
                mCreditCardLayout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    private String getBankId(int position) {
        return arrIds[position];
    }

    private void proceedToInternetBanking() {

        NetbankingOption netbankingOption = new NetbankingOption(bankNme, bankId);

        Amount amount = new Amount(priceAmt.substring(2,priceAmt.length()));
        // Init Net Banking PaymentType
        try {
            PaymentType.PGPayment pgPayment1 = new PaymentType.PGPayment(amount, AppConstants.BILL_URL, netbankingOption, new CitrusUser("vamshi@datasisar.com","9036627235"));

            citrusClient.pgPayment((PaymentType.PGPayment)pgPayment1, new Callback<TransactionResponse>() {

                @Override
                public void success(TransactionResponse transactionResponse) {

//                    mContext.onPaymentComplete(transactionResponse);
                    saveOrder();
                }

                @Override
                public void error(CitrusError error) {
                    Log.v("Error in Citrus pay",error.toString());
                }
            });
        } catch (CitrusException e) {
            Log.e("Payment : ---->","Unsuccessful");
            e.printStackTrace();
        }
    }

    private void makeCardPayment() {


        String CardName,CardNumber,expirymonth,expiryYear,cvvCode;

        CardName = pref.getString("CardTextName",null);
        CardNumber = pref.getString("CardNumber", null);
        expirymonth = pref.getString("Cardexpirymonth", null);
        expiryYear = pref.getString("CardexpiryYear", null);
        cvvCode = pref.getString("CardcvvCode", null);
//
        CreditCardOption creditCardOption = new CreditCardOption(CardName, CardNumber, cvvCode, Month.getMonth(expirymonth), Year.getYear(expiryYear));
        logutils.log(CardName);

        Amount amount = new Amount(priceAmt.substring(2,priceAmt.length()));
        // Init PaymentType
        try {
            PaymentType.PGPayment pgPayment1 = new PaymentType.PGPayment(amount, AppConstants.BILL_URL, creditCardOption, new CitrusUser("vamshi@datasisar.com", "9036627235"));
            citrusClient.pgPayment((PaymentType.PGPayment)pgPayment1, new Callback<TransactionResponse>() {

                @Override
                public void success(TransactionResponse transactionResponse) {
                    Log.v("Payment status", "---> " + transactionResponse);
                    saveOrder();
                }

                @Override
                public void error(CitrusError error) {
                    Log.v("Payment error", "---> " + error);
                }
            });
        } catch (CitrusException e) {
            e.printStackTrace();

        }

    }

    private void saveOrder() {
//        String OrderNo = Utils.generateOrderId().toString();
        String OrderNo = "";
        if (orderIdObj != null) {
            try {
//                OrderNo = orderIdObj.get("o_id").toString();
                logutils.log(OrderNo);
            } catch (Exception e) {

            }
        }
        ParseUser currentUser = ParseUser.getCurrentUser();
        employeeObj = getEmployee();
        String deviceType = "ANDROID";
        float subTotal = Float.parseFloat(priceAmt.replace(getResources().getString(R.string.Rs), "").trim());
        float taxAmount = 80;
        float driverTip = 30;
        float totalAmount = subTotal + taxAmount + driverTip;
        String paymentMode = "COD";
        String orderStatus = "New Order";
        String restStatus = "New Order";
        String driverStatus = "New Order";
        String dFName = currentUser.getUsername();
        String dLName = "Parker";
        String address1 = uLocation.getUserAddress().getAddressLine(0);
        String address2 = uLocation.getUserAddress().getAddressLine(0);
        String landMark = "Bangalore";
        String city = "Bangalore";
        String state = "Karnataka";
        String pinCode = "385021";
        String latitude = "12.979555919016926";
        String longitude = "77.61510551001504";
        Boolean isOrderClosed = false;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();

        Date deliveryDate = new Date(t + (45 * 60000));

        //Date deliveryDate = new Date(System.currentTimeMillis()+45*60*1000);
        String deliveryTime = format.format(deliveryDate);


        Order order = new Order(OrderNo, customersObj, employeeObj, deviceType, subTotal,
                taxAmount, driverTip, totalAmount, paymentMode, orderStatus, restStatus,
                driverStatus, dFName, dLName, address1, address2, landMark, city, state, pinCode,
                latitude, longitude, isOrderClosed, deliveryDate, deliveryTime);
        order.setOrderCompleteListener(orderCompleteListener);
        order.createOrder();

    }

    private ParseObject getEmployee() {
        ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                "Employees");
        try {
            ob = queryCustomers.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null && ob.size() > 0) {
            employeeObj = (ParseObject) ob.get(0);
            return employeeObj;
        }
        return employeeObj;
    }


    private class GetOrderNumber extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            orderIdObj = Utils.getOrderNumber();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter implements AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

        String CardName, CardNumber, expirymonth, expiryYear, cvvCode;
        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<String>> _listDataChild;

        public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        public ExpandableListAdapter(Context context, List<String> listDataHeader) {
            this._context = context;
            this._listDataHeader = listDataHeader;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            bankNme = parent.getItemAtPosition(position).toString();
            int index = -1;
            for (int i=0;i<arrBanks.length;i++) {
                if (arrBanks[i].equals(bankNme)) {
                    index = i;
                    break;
                }
            }
            bankId = getBankId(index);

            autoFill.setText(bankNme);
            //Toast.makeText(mContext,bankId,Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            bankId = getBankId(position);
//            bankNme = parent.getItemAtPosition(position).toString();
//            autoFill.setText(bankNme);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

            InputMethodManager imm = (InputMethodManager) getSystemService(
                    INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataHeader.get(groupPosition);

        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            editor = pref.edit();

            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            switch (groupPosition) {
                case 0:
                    convertView = infalInflater.inflate(R.layout.credit_card_layout, null);
                    etName = (EditText) convertView.findViewById(R.id.etCardName);
                    etCardNo = (EditText) convertView.findViewById(R.id.etCardNo);
                    etMonth = (EditText) convertView.findViewById(R.id.etCardDate);
                    etCvv = (EditText) convertView.findViewById(R.id.etCardCVV);
                    etYear = (EditText) convertView.findViewById(R.id.etCardYear);

                    etName.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            CardName = s.toString();
                            editor.putString("CardTextName",CardName);
                            editor.commit();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    etCardNo.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            CardNumber = s.toString();
                            editor.putString("CardNumber",CardNumber);
                            editor.commit();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    etMonth.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            expirymonth = s.toString();
                            editor.putString("Cardexpirymonth",expirymonth);
                            editor.commit();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    etCvv.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            cvvCode = s.toString();
                            editor.putString("CardcvvCode",cvvCode);
                            editor.commit();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                    etYear.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            expiryYear = s.toString();
                            editor.putString("CardexpiryYear",expiryYear);
                            editor.commit();
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });


                    isCreditOrDebit = true;
                    if (isNativewallet) {
                        isNativewallet = false;
                    } else if (isNetBanking) {
                        isNetBanking = false;
                    } else if (isCod) {
                        isCod = false;
                    }
                    break;
                case 1:
                    convertView = infalInflater.inflate(R.layout.netbanking, null);
                    autoFill = (AutoCompleteTextView) convertView.findViewById(R.id.autoBanksList);

                    //Create adapter
                    adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_dropdown_item_1line, arrBanks);

                    autoFill.setThreshold(1);

                    //Set adapter to AutoCompleteTextView
                    autoFill.setAdapter(adapter);
                    autoFill.setOnItemSelectedListener(this);
                    autoFill.setOnItemClickListener(this);
                    isNetBanking = true;
                    if (isNativewallet) {
                        isNativewallet = false;
                    } else if (isCreditOrDebit) {
                        isCreditOrDebit = false;
                    } else if (isCod) {
                        isCod = false;
                    }
                    break;
                case 2:
                    convertView = infalInflater.inflate(R.layout.native_wallet, null);
                    break;
                case 3:
                    convertView = infalInflater.inflate(R.layout.cod_layout, null);
                    isCod = true;
                    if (isNativewallet) {
                        isNativewallet = false;
                    } else if (isCreditOrDebit) {
                        isCreditOrDebit = false;
                    } else if (isNetBanking) {
                        isNetBanking = false;
                    }
                    break;
            }

            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return 1;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            //System.out.println(groupPosition);
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }


            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setText(headerTitle);

            ImageView ivMenuImg = (ImageView) convertView.findViewById(R.id.ivMenuicon);
            ivMenuImg.setBackground(getResources().getDrawable(R.drawable.dark_cricle));
            if (isExpanded) {
                ivMenuImg.setBackground(getResources().getDrawable(R.drawable.circle));

            } else {
                ivMenuImg.setBackground(getResources().getDrawable(R.drawable.dark_cricle));
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public String CardTextName(){
            return CardName;
        }


    }




}
