package com.entree.entree.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.entree.entree.R;

public class EditProfileActivity extends AppCompatActivity {

    private TextView box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        overridePendingTransition(R.anim.bottom_top, R.anim.top_bottom);

    }
}
