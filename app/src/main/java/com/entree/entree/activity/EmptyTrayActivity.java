package com.entree.entree.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.entree.entree.R;

public class EmptyTrayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty_tray);
    }
}
