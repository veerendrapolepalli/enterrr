package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.OrderHistoryAdapter;
import com.entree.entree.constructors.Order;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.OrderItems;
import com.entree.entree.constructors.RestaurantOrders;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.constructors.UserOrder;
import com.entree.entree.interfacelistener.Items;
import com.entree.entree.utils.Utils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderHistoryActivity extends BaseActivity{
    ArrayList<Order> arrOrder = new ArrayList<Order>();
    ArrayList<Items> items = new ArrayList<Items>();
    ArrayList<UserOrder> arrUserOrder = new ArrayList<UserOrder>();
    Context mContext;
    Utils utils;
    UserOrder userOrderObj = null;
    ListView lvOrderSummary ;
    public static RestaurantOrders restOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_order_history);

        setTitle("Order History");

        mContext = this;
        utils = new Utils(mContext);

        lvOrderSummary = (ListView) findViewById(R.id.lvOrderSummary);

        lvOrderSummary.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Items i = items.get(position);
                if(!i.isSection()){

                    OrderItems oItems = (OrderItems) i;
                    restOrder = oItems.getOrderItem();
                    Intent intent = new Intent(mContext,OrderHistorySummary.class);
                    intent.putExtra("SELECTEDORDER",true);
                    startActivity(intent);
                }
            }
        });
        new GetUserOrderData().execute();
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.order_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getOrderStats(){
        List<ParseObject> ob = null;
        ParseObject customersObj = getLoggedInCustomer();
        if(customersObj!=null) {
            ob = getCustomerOrders(customersObj);
            if(ob!=null && ob.size()>0){
                parseOrderObject(ob);
                if(arrUserOrder!=null && arrUserOrder.size()>0) {
                    filterOrderHistory();
                }
            }
        } else {

        }
    }

    private ParseObject getLoggedInCustomer(){
        ParseUser currentUser = ParseUser.getCurrentUser();
        List<ParseObject> ob = null;
        ParseObject customersObj= null;
        if (currentUser != null) {
            currentUser.getObjectId();
        }
        ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                "Customers");
        queryCustomers.whereEqualTo("UserId", currentUser);

        try {
            ob = queryCustomers.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null && ob.size()>0) {
            customersObj = (ParseObject) ob.get(0);
        }
        return customersObj;
    }

    private List<ParseObject> getCustomerOrders(ParseObject custParseObject){
        List<ParseObject> ob = null;
        ParseQuery<ParseObject> queryOrders = new ParseQuery<ParseObject>(
                "Orders");
        queryOrders.whereEqualTo("CustomerId", custParseObject);
        try {
            ob = queryOrders.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ob;

    }
    private List<ParseObject> getOrderItems(ParseObject orderObj){
        List<ParseObject> ob = null;
        ParseQuery<ParseObject> queryOrderItems = new ParseQuery<ParseObject>(
                "OrderItems");
        queryOrderItems.whereEqualTo("OrderId", orderObj);
        try {
            ob = queryOrderItems.find();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ob;
    }

    private ArrayList<OrderItem> parseOrderItemsObject(List<ParseObject> ob){
        ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();

        for(ParseObject orderItemsObj:ob){

            String orderItemId = orderItemsObj.getObjectId();
            ParseObject orderObj = (ParseObject) orderItemsObj.get("OrderId");
            ParseObject resObj = (ParseObject) orderItemsObj.get("ResId");
            String resName = orderItemsObj.getString("ResName");
            if(resName.equalsIgnoreCase("")) {
                if(!resObj.containsKey("ResName")) {
                    ParseQuery<ParseObject> queryRest = new ParseQuery<ParseObject>("Restaurant");
                    queryRest.whereEqualTo("objectId", resObj.getObjectId());
                    try {
                        List<ParseObject> restArray = queryRest.find();
                        resObj = restArray.get(0);
                        if (resObj.containsKey("ResName"))
                            resName = resObj.getString("ResName");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    resName = resObj.getString("ResName");
                }

            }




            String resId = resObj.getObjectId();

            String foodName = orderItemsObj.getString("FoodName");
            String foodDesc = orderItemsObj.getString("FoodDesc");
            String foodCategory = orderItemsObj.getString("FoodCategory");

            double qty = orderItemsObj.getDouble("Qty");
            double price = orderItemsObj.getDouble("Price");
            double itemTotal = orderItemsObj.getDouble("ItemTotal");

            String vegOnly = orderItemsObj.getString("VegOnly");
            String foodNote = orderItemsObj.getString("FoodNote");

            OrderItem oItem = new OrderItem(orderItemId,orderObj,resObj,foodName,
                    foodDesc,foodCategory,vegOnly,(int)qty,(int)price,(int)itemTotal,foodNote,resName);
            oItem.setResID(resId);
            oItem.setResObj(utils.getRestaurentObject(resObj));
            arrOrderItem.add(oItem);
        }
        return arrOrderItem;
    }

    private void parseOrderObject(List<ParseObject> ob){
        ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();
        for(ParseObject orderObj:ob){
            String orderId = orderObj.getObjectId();
            String orderNo = orderObj.getString("OrderNo");
            ParseObject custObj = (ParseObject) orderObj.get("CustomerId");
            ParseObject employeeObj = (ParseObject) orderObj.get("EmployeeId");
            String deviceType = orderObj.getString("DeviceType");
            double subTotalAmount = orderObj.getDouble("SubTotalAmount");
            double taxAmount = orderObj.getDouble("TaxAmount");
            double driverTipAmount = orderObj.getDouble("DriverTipAmount");
            double totalAmount = orderObj.getDouble("TotalAmount");
            String paymentMode = orderObj.getString("PaymentMode");
            String orderStatusLatest = orderObj.getString("OrderStatusLatest");
            String deliveryFirstName = orderObj.getString("DeliveryFirstName");
            String deliveryLastName = orderObj.getString("DeliveryLastName");
            String deliveryAddress1 = orderObj.getString("DeliveryAddress1");
            String deliveryAddress2 = orderObj.getString("DeliveryAddress2");
            String deliveryLandmark = orderObj.getString("DeliveryLandmark");
            String deliveryCity = orderObj.getString("DeliveryCity");
            String deliveryState = orderObj.getString("DeliveryState");
            String deliveryPincode = orderObj.getString("DeliveryPincode");
            String deliveryLatitude = orderObj.getString("DeliveryLatitude");
            String deliveryLongitude = orderObj.getString("DeliveryLongitude");
            Date createdDate = orderObj.getCreatedAt();
            Boolean isOrderClosed = orderObj.getBoolean("IsOrderClosed");
            Date deliveryDate = orderObj.getDate("DeliveryDate");
            String deliveryTime = orderObj.getString("DeliveryTime");

            List<ParseObject> listOrderitems = getOrderItems(orderObj);
            arrOrderItem = parseOrderItemsObject(listOrderitems);

            Order order = new Order(orderId,orderNo,custObj,employeeObj,deviceType,subTotalAmount,
                    taxAmount,driverTipAmount,totalAmount,paymentMode,orderStatusLatest,
                    deliveryFirstName,deliveryLastName,deliveryAddress1,deliveryAddress2,
                    deliveryLandmark,deliveryCity,deliveryState,deliveryPincode,deliveryLatitude,
                    deliveryLongitude,createdDate,isOrderClosed,deliveryDate,deliveryTime);
            arrOrder.add(order);
            userOrderObj = new UserOrder(order,arrOrderItem);
            arrUserOrder.add(userOrderObj);
        }
    }

    private void filterOrderHistory(){
        Date previousDay = Utils.getYesterdayDate(0);
        Date historyDay = Utils.getYesterdayDate(1);
        ArrayList<UserOrder> arrTodayOrder = new ArrayList<UserOrder>();
        ArrayList<UserOrder> arrYesterdaysOrder = new ArrayList<UserOrder>();
        ArrayList<UserOrder> arrOlderOrder = new ArrayList<UserOrder>();
        for(UserOrder userOrderObj:arrUserOrder){
            Order order = userOrderObj.getuOrder();
            if(order.getCreatedDate().after(previousDay)||
                    order.getCreatedDate().equals(previousDay)){
                arrTodayOrder.add(userOrderObj);
            } else if (order.getCreatedDate().after(historyDay)||
                    order.getCreatedDate().equals(historyDay)){
                arrYesterdaysOrder.add(userOrderObj);
            } else {
                arrOlderOrder.add(userOrderObj);
            }
        }
        if(arrTodayOrder!=null && arrTodayOrder.size()>0) {
            items.add(new SectionItem("Today", ""));
            filterOrderByRestaurant(arrTodayOrder);
        }
        if(arrYesterdaysOrder!=null && arrYesterdaysOrder.size()>0) {
            items.add(new SectionItem("Yesterday", ""));
            filterOrderByRestaurant(arrYesterdaysOrder);
        }
        if(arrOlderOrder!=null && arrOlderOrder.size()>0) {
            items.add(new SectionItem("Past", ""));
            filterOrderByRestaurant(arrOlderOrder);
        }

    }

    private ArrayList<RestaurantOrders> filterOrderByRestaurant(ArrayList<UserOrder> arrUserOrder){
        ArrayList<String> arrRest = new ArrayList<String>();
        ArrayList<RestaurantOrders> arrRestOrders = new ArrayList<RestaurantOrders>();
        for(UserOrder uOrder : arrUserOrder) {
            ArrayList<OrderItem> arrOrderItems = uOrder.getArrOrderItem();
            for (OrderItem oItem : arrOrderItems) {
                String resName = oItem.getRestName();
                if (!arrRest.contains(resName)) {
                    arrRest.add(resName);
                }
            }
        }

        for(String resName :arrRest) {
            ArrayList<OrderItem> arrOrderItem = new ArrayList<OrderItem>();
            double total = 0;
            for(UserOrder uOrder : arrUserOrder) {
                ArrayList<OrderItem> arrOrderItems = uOrder.getArrOrderItem();
                for (OrderItem oItem : arrOrderItems) {
                    if (resName.equalsIgnoreCase(oItem.getRestName())) {
                        arrOrderItem.add(oItem);
                        total = total + (double) oItem.getItemTotal();
                    }
                }
            }
            items.add(new OrderItems(new RestaurantOrders(resName,arrOrderItem,total)));
            arrRestOrders.add(new RestaurantOrders(resName,arrOrderItem,total));
        }

        return arrRestOrders;
    }
    private void getRestaurant(){}

    private class GetUserOrderData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(items.size()>0) {
                OrderHistoryAdapter orderHistoryAdapter = new OrderHistoryAdapter(mContext,items);
                lvOrderSummary.setAdapter(orderHistoryAdapter);
            }
            utils.hideLoader();

        }

        @Override
        protected Void doInBackground(Void... params) {
            getOrderStats();
            return null;
        }
    }
}
