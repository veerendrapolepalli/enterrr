package com.entree.entree.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.UserLocation;
import com.entree.entree.utils.JSONParser;
import com.entree.entree.utils.LocationAddress;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.SnackBarUtils;
import com.entree.entree.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class Search extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ConnectionCallbacks, OnConnectionFailedListener {
    private static final String TAG_RESULT = "predictions";
    private static final int BTN_SEARCH_LOCATION = 1;
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    protected AutoCompleteTextView etSearch_location;
    String url;
    JSONObject json;
    JSONArray contacts = null;
    String[] search_text;
    ArrayList<String> names;
    ArrayList<SelectedLocation> arrselectedLocation = new ArrayList<SelectedLocation>();
    ArrayAdapter<String> adp;
    ProgressDialog pdialog;
    String browserKey = "AIzaSyCLzXyzHCtBEhWaSPvwgu1ixiZEnUBxlcs";
    UserLocation userLocation = null;
    TextView tvName, tvEmail;
    ImageView ivProfileIcon;
    private Context mContext;
    private Utils utils = null;
    private LogUtilities logUtils = null;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private ImageButton btnSrc;
    ParseObject customersObj;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    ListView lvUserAddress;
    private SnackBarUtils snackbar;
    private CoordinatorLayout coordinatorLayout;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        RelativeLayout relSearchLayout;
        setupUI((RelativeLayout) findViewById(R.id.relSearchLayout));
        mContext = this;
        setUtils();
        setUI();
        setComponents();
        buildGoogleApiClient();
        new GetUserAddress().execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {

            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(),
                    getApplicationContext(), new GeocoderHandler());
//            etSearch_location.setText(String.format("%s: %f", "LATITUDE",
//                    mLastLocation.getLatitude()) + " &  " + String.format("%s: %f", "LONGITUDE",
//                    mLastLocation.getLongitude()));
        } else {
            //Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        logUtils.log("Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        logUtils.log("Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        utils.navigationOnClick(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void hidekeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Search.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void setUI() {
        etSearch_location = (AutoCompleteTextView) findViewById(R.id.etSearch_location);
        lvUserAddress = (ListView) findViewById(R.id.lvUserAddress);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        btnSrc = (ImageButton) findViewById(R.id.btnSrc);
//        tvName = (TextView) findViewById(R.id.tvUserName);
//        tvEmail = (TextView) findViewById(R.id.tvEmailId);
//        ivProfileIcon = (ImageView) findViewById(R.id.ivProfileicon);
        if (getIntent().hasExtra("Email")) {
            // tvEmail.setText(getIntent().getExtras().getString("Email"));
        }

        etSearch_location.setThreshold(0);
        names = new ArrayList<String>();

        etSearch_location.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                search_text = etSearch_location.getText().toString().split(",");
                if (mLastLocation != null) {
                    String latnlong = mLastLocation.getLatitude() + "," + mLastLocation.getLongitude();
                    url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + URLEncoder.encode(search_text[0]) + "&location=" + latnlong + "6&radius=500&sensor=true&key=" + browserKey;
                    if (search_text.length <= 1) {
                        names = new ArrayList<String>();
                        Log.d("URL", url);
                        paserdata parse = new paserdata();
                        parse.execute();
                    }
                }

            }
        });

        etSearch_location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hidekeyboard(v);
                }
            }
        });

    }

    private void setComponents() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        navigationView.setNavigationItemSelectedListener(this);
        btnSrc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLastLocation!=null) {
                    Intent i = new Intent(getApplicationContext(), MapLocation.class);
                    i.putExtra("LATITUDE", mLastLocation.getLatitude());
                    i.putExtra("LONGITUDE", mLastLocation.getLongitude());
                    i.putExtra("LOCATION", mLastLocation);
                    startActivity(i);
                } else {
                    Toast.makeText(mContext,"Cannot retrieve your location",Toast.LENGTH_LONG).show();
                }

            }
        });
        etSearch_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                if (arrselectedLocation != null && arrselectedLocation.size() > 0) {

                    pdialog = new ProgressDialog(mContext);
                    SelectedLocation select = arrselectedLocation.get(pos);
                    String detailUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=%s";
                    detailUrl = String.format(detailUrl, select.placeId, browserKey);
                    hideSoftKeyboard(Search.this);
                    pdialog.setTitle("Loading your Location");
                    pdialog.show();
                    new GetPlaceDetails(detailUrl).execute();
                }
            }
        });
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    userLocation = (UserLocation) bundle.getSerializable("user_location");
                    break;
                default:
                    locationAddress = null;
            }
            etSearch_location.setText(locationAddress);
        }
    }

    public class paserdata extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            json = jParser.getJSONFromUrl(url.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    arrselectedLocation = new ArrayList<SelectedLocation>();
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String placeId = c.getString("place_id");
                        Log.d("description", description);
                        arrselectedLocation.add(new SelectedLocation(placeId, description));
                        names.add(description);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            adp = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_list_item_1, names) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setTextColor(Color.BLACK);
                    return view;
                }
            };
            etSearch_location.setAdapter(adp);


        }
    }

    private class GetUserAddress extends  AsyncTask<Void,Void,Void> {
        List<ParseObject> ob= null;
        List<ParseObject> listCustomerAddress= null;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }
        @Override
        protected Void doInBackground(Void... params) {

            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                currentUser.getObjectId();
            }
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("UserId", currentUser);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size()>0) {
                customersObj = (ParseObject) ob.get(0);
                ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                        "CustomerAddresses");
                queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
                queryCustomerAddr.setLimit(3);

                try {
                    listCustomerAddress = queryCustomerAddr.find();
                } catch (ParseException e) {
                    //logUtils.log(e.getMessage());
                    e.printStackTrace();
                }
                if(listCustomerAddress!=null && listCustomerAddress.size()>0) {
                    for(ParseObject custAddrObj : listCustomerAddress) {
                        String addressId = custAddrObj.getObjectId();
                        ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                        String firstName = custAddrObj.getString("FirstName");
                        String lastName = custAddrObj.getString("LastName");
                        String addressTitle = custAddrObj.getString("AddressTitle");
                        String address1 = custAddrObj.getString("Address1");
                        String address2 = custAddrObj.getString("Address2");
                        String landMark = custAddrObj.getString("Landmark");
                        String city = custAddrObj.getString("City");
                        String state = custAddrObj.getString("State");
                        String pincode = custAddrObj.getString("Pincode");
                        String latitude = custAddrObj.getString("Latitude");
                        String longitude = custAddrObj.getString("Longitude");

                        CustomerAddresses custAddress = new CustomerAddresses(addressId,customersObj,
                                firstName,lastName,addressTitle,address1,address2,landMark,
                                city,state,pincode,latitude,longitude);
                        arrCustAddr.add(custAddress);
                    }

                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(arrCustAddr!=null && arrCustAddr.size()>0) {
                CustomerAddressListAdapter customerAddressListAdapter =
                        new CustomerAddressListAdapter(mContext, arrCustAddr);

                lvUserAddress.setAdapter(customerAddressListAdapter);
            }
            utils.hideLoader();
        }
    }

    public class GetPlaceDetails extends AsyncTask<Void, Integer, Void> {
        String detailUrl = "";
        JSONObject placeDetail;

        public GetPlaceDetails(String detailUrl) {
            this.detailUrl = detailUrl;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            json = jParser.getJSONFromUrl(detailUrl.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts
                    placeDetail = json.getJSONObject("result");
                    if (placeDetail != null) {
                        placeDetail = placeDetail.getJSONObject("geometry");
                        if (placeDetail != null) {
                            placeDetail = placeDetail.getJSONObject("location");
                            if (placeDetail != null) {
                                String lat = placeDetail.getString("lat");
                                String lng = placeDetail.getString("lng");

                                mLastLocation.setLatitude(Double.parseDouble(lat));
                                mLastLocation.setLongitude(Double.parseDouble(lng));

                                Intent i = new Intent(getApplicationContext(), MapLocation.class);
                                i.putExtra("LATITUDE", mLastLocation.getLatitude());
                                i.putExtra("LONGITUDE", mLastLocation.getLongitude());
                                i.putExtra("LOCATION", mLastLocation);
                                startActivity(i);
                            }
                        }
                    }
                    Log.d("GEOMETRY", placeDetail.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            adp = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_list_item_1, names) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setTextColor(Color.BLACK);
                    return view;
                }
            };
            etSearch_location.setAdapter(adp);
            pdialog.dismiss();

        }
    }

    public class SelectedLocation {
        public String placeId;
        public String placeName;

        public SelectedLocation(String placeId, String placeName) {
            this.placeId = placeId;
            this.placeName = placeName;
        }

    }

}