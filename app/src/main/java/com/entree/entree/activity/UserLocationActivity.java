package com.entree.entree.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.async.GetRestaturantData;
import com.entree.entree.async.GetSearchedLocationAsync;
import com.entree.entree.async.GetSelectedLocationDetailsAsync;
import com.entree.entree.async.GetUserSavedAddress;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantArray;
import com.entree.entree.constructors.SelectedLocation;
import com.entree.entree.interfacelistener.GetCustomerLocationListener;
import com.entree.entree.interfacelistener.RestaurantListner;
import com.entree.entree.interfacelistener.SearchedLocationListener;
import com.entree.entree.interfacelistener.SelectedPlaceDetailListener;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.Utils;
import com.entree.entree.views.AvenirBook;
import com.entree.entree.views.AvenirLight;
import com.entree.entree.views.RobottoCondencedBold;
import com.entree.entree.views.RobottowCondenced;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserLocationActivity extends BaseActivity implements ConnectionCallbacks, OnConnectionFailedListener,
        GoogleMap.OnMapClickListener,OnMapReadyCallback {
    GoogleMap googleMap;
    RobottowCondenced btnGetEntree;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    Context mContext;
    Utils utils = null;
    RestaurantListner restaurantListner;
    CustomerAddressListAdapter customerAddressListAdapter;

    GetCustomerLocationListener custAddressListener;
    SearchedLocationListener searchedLocationListener;
    SelectedPlaceDetailListener selectedPlaceDetailListener;

    private ArrayList<Restaurant> arrRestaurant = new ArrayList<Restaurant>();
    private ArrayList<CustomerAddresses> arrCustomerAddress = new ArrayList<CustomerAddresses>();
    private Typeface avenir_font;
    private RobottoCondencedBold btnElsewhere, btnSavedAdds;
    private LinearLayout llSearchLoc, llUserSavedAdds;
    private Boolean isElsewherell = false;
    private Boolean isUserSavedAdds = false;
    private EditText etSearchLoc;
    private AvenirLight tvHeader, tvTip, tvNoAddress;
    private AvenirBook tvSearchSubmit;
    private ListView lvSavedAdds;
    ArrayList<SelectedLocation> arrSelectedLocation;
    ArrayList<String> placeNames;
    private ImageButton currentlocaiton;
    private PlaceAutocompleteFragment autocompleteFragment;

    public static String userAddress = "";
    private LogUtilities logUtils = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_map_location);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        avenir_font = Typeface.createFromAsset(mContext.getAssets(), "fonts/AvenirLTStd-Medium.otf");
        checkForPlayServices();
        setUtils();
        setTitle("Location");
        setGoogleClient();
        initUI();
        createListener();
        createOnClickListener();
        setItemClickListener();
        new GetUserSavedAddress(mContext, custAddressListener).execute();
        //setUserLocation();

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void checkForPlayServices() {
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
    }

    protected synchronized void setGoogleClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    private void createListener() {
        restaurantListner = new RestaurantListner() {
            @Override
            public void returnRestaurantList(ArrayList<Restaurant> arrRest) {
                arrRestaurant = arrRest;
                if (arrRestaurant.size() > 0) {
                    btnGetEntree.setEnabled(true);
                    btnGetEntree.setText(getResources().getString(R.string.get_entry));
                } else {
                    btnGetEntree.setEnabled(false);
                    btnGetEntree.setText(getResources().getString(R.string.get_entry_empty));
                }
                Log.v("RESTAURANT ARRAY", "ARRAY SIZE :- " + arrRestaurant.size());
            }
        };


        custAddressListener = new GetCustomerLocationListener() {
            @Override
            public void onCustomerLocationFetched(ArrayList<CustomerAddresses> arrCustomerAdd) {
                arrCustomerAddress = arrCustomerAdd;
                if (arrCustomerAddress.size() > 0) {
                    customerAddressListAdapter =
                            new CustomerAddressListAdapter(mContext, arrCustomerAddress, true);
                    lvSavedAdds.setAdapter(customerAddressListAdapter);
                }
            }
        };

        searchedLocationListener = new SearchedLocationListener() {
            @Override
            public void onSearchComplete(ArrayList<SelectedLocation> arrselectedLocation, ArrayList<String> names) {
                if (arrselectedLocation != null && arrselectedLocation.size() > 0) {
                    SelectedLocation select = arrselectedLocation.get(0);
                    new GetSelectedLocationDetailsAsync(mContext, select.placeId, selectedPlaceDetailListener).execute();
                }
            }
        };

        selectedPlaceDetailListener = new SelectedPlaceDetailListener() {
            @Override
            public void onFetchedDetailComplete(String lattitude, String longitude) {
                mCurrentLocation.setLatitude(Double.parseDouble(lattitude));
                mCurrentLocation.setLongitude(Double.parseDouble(longitude));
                updateRestaurentList();
            }
        };
    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void createOnClickListener() {
        btnGetEntree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnGetEntree.isEnabled()) {
                    RestaurantArray restArrObj = new RestaurantArray(arrRestaurant);
                    Intent i = new Intent(mContext, RestaurantList.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putSerializable("RestaurentArray", restArrObj);
                    i.putExtras(mBundle);
//                    i.putExtra("LATTITUDE", mCurrentLocation.getLatitude());
//                    i.putExtra("LONGITUDE", mCurrentLocation.getLongitude());
//                    i.putExtra("userAddress", userAddress);
                    startActivity(i);
                }
            }
        });
        btnElsewhere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnGetEntree.setVisibility(View.GONE);
                if (!isElsewherell) {
                    if (isUserSavedAdds) {
                        isUserSavedAdds = false;
                        Animation slideDown = AnimationUtils.loadAnimation(mContext,
                                R.anim.bottom_down);
                        llUserSavedAdds.startAnimation(slideDown);
                        llUserSavedAdds.setVisibility(View.GONE);
                    }
                    isElsewherell = true;
                    Animation slideUp = AnimationUtils.loadAnimation(mContext, R.anim.bottom_up);
                    llSearchLoc.startAnimation(slideUp);
                    llSearchLoc.setVisibility(View.VISIBLE);
                } else {
                    isElsewherell = false;
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_down);
                    llSearchLoc.startAnimation(slideDown);
                    llSearchLoc.setVisibility(View.GONE);
                    btnGetEntree.setVisibility(View.VISIBLE);
                }
            }
        });


        savedAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lvSavedAdds.getVisibility() != View.VISIBLE) {
                    lvSavedAdds.setVisibility(View.VISIBLE);
                }
                else{
                    lvSavedAdds.setVisibility(View.GONE);
                }
                /*btnGetEntree.setVisibility(View.GONE);
                if (!isUserSavedAdds) {
                    if (isElsewherell) {
                        isElsewherell = false;
                        Animation slideDown = AnimationUtils.loadAnimation(mContext,
                                R.anim.bottom_down);
                        llSearchLoc.startAnimation(slideDown);
                        llSearchLoc.setVisibility(View.GONE);
                    }
                    isUserSavedAdds = true;
                    Animation slideUp = AnimationUtils.loadAnimation(mContext, R.anim.bottom_up);
                    llUserSavedAdds.startAnimation(slideUp);
                    llUserSavedAdds.setVisibility(View.VISIBLE);
                    if (arrCustomerAddress != null && arrCustomerAddress.size() > 0) {
                        lvSavedAdds.setVisibility(View.VISIBLE);
                        tvNoAddress.setVisibility(View.GONE);
                    } else {
                        lvSavedAdds.setVisibility(View.GONE);
                        tvNoAddress.setVisibility(View.VISIBLE);
                    }
                } else {
                    isUserSavedAdds = false;
                    Animation slideDown = AnimationUtils.loadAnimation(mContext,
                            R.anim.bottom_down);
                    llUserSavedAdds.startAnimation(slideDown);
                    llUserSavedAdds.setVisibility(View.GONE);
                    btnGetEntree.setVisibility(View.VISIBLE);
                }*/
            }
        });

        tvSearchSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchedLocation = etSearchLoc.getText().toString();
                if (searchedLocation.length() > 0) {
                    new GetSearchedLocationAsync(mContext, searchedLocation, searchedLocationListener).execute();
                }
            }
        });

        currentlocaiton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentLocation != null) {

                    logUtils.log(Double.toString(mCurrentLocation.getLatitude()));
                    logUtils.log(Double.toString(mCurrentLocation.getLongitude()));

                    LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    CameraUpdate cameraUpdate = null;
                    cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
                    googleMap.animateCamera(cameraUpdate);
//                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                }
            }
        });
    }

    private void setItemClickListener() {
        lvSavedAdds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                CustomerAddresses custAddress = arrCustomerAddress.get(position);
                if (custAddress != null) {


                    logUtils.log(Double.toString(Double.parseDouble(custAddress.getLatitude())));
                    logUtils.log(Double.toString(Double.parseDouble(custAddress.getLongitude())));

                    Double lattitude = Double.parseDouble(custAddress.getLatitude());
                    Double longitude = Double.parseDouble(custAddress.getLongitude());
                    mCurrentLocation.setLatitude(lattitude);
                    mCurrentLocation.setLatitude(longitude);
                    updateRestaurentList();
                }
            }
        });
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    private void initUI() {
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        fm.getMapAsync(this);
//        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        googleMap = fm.getMap();
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMapClickListener(this);
        //btnGetEntree = (Button) findViewById(R.id.btnLocation);
        btnSavedAdds = (RobottoCondencedBold) findViewById(R.id.btnASAP);
        btnElsewhere = (RobottoCondencedBold) findViewById(R.id.btnLocation);
        currentlocaiton = (ImageButton) findViewById(R.id.currentLocation);
        //btnSavedAdds.setTypeface(avenir_font);
        /// btnElsewhere.setTypeface(avenir_font);
        btnGetEntree = (RobottowCondenced) findViewById(R.id.btnGetEntree);
        btnGetEntree.setTypeface(avenir_font);
        llSearchLoc = (LinearLayout) findViewById(R.id.llSearchLoc);
        llUserSavedAdds = (LinearLayout) findViewById(R.id.llSavedAdds);

        lvSavedAdds = (ListView) findViewById(R.id.lvSavedadds);

        tvHeader = (AvenirLight) findViewById(R.id.tvLocheader);
        tvTip = (AvenirLight) findViewById(R.id.tvTipText);
        tvNoAddress = (AvenirLight) findViewById(R.id.tvSavedAdds);
        tvSearchSubmit = (AvenirBook) findViewById(R.id.tvSubmitSearch);
        etSearchLoc = (EditText) findViewById(R.id.etSearchLoc);
        tvNoAddress.setTypeface(avenir_font);
        tvSearchSubmit.setTypeface(avenir_font);
        tvTip.setTypeface(avenir_font);
        tvSearchSubmit.setTypeface(avenir_font);
        tvHeader.setTypeface(avenir_font);
        etSearchLoc.setTypeface(avenir_font);

//        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
//                // TODO: Get info about the selected place.
//                logUtils.log(place.getName().toString());
//            }
//
//            @Override
//            public void onError(Status status) {
//                // TODO: Handle the error.
//                logUtils.log(status.toString());
//            }
//        });

    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurrentLocation != null) {
            updateRestaurentList();
        }
    }

    private void updateRestaurentList() {
        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        new GetRestaturantData(mContext, false, currentLatLng, restaurantListner).execute();
        addMarker();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";
        String knownName = "";

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);


            if (addresses != null) {

                Address returnedAddress = addresses.get(0);
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                knownName = addresses.get(0).getAddressLine(1);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress
                            .append(returnedAddress.getAddressLine(i)).append(
                            ",");
                }
                strAdd = strReturnedAddress.toString();
                tvTip.setText(strAdd);
                Log.w("Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("Current loction address", "Canont get Address!");
        }
        return knownName;
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();

        IconGenerator iconFactory = new IconGenerator(this);
        iconFactory.setStyle(IconGenerator.STYLE_PURPLE);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        logUtils.log(Double.toString(mCurrentLocation.getLatitude()));
        logUtils.log(Double.toString(mCurrentLocation.getLongitude()));

        LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        options.position(currentLatLng);
        googleMap.clear();

        //Marker mapMarker = googleMap.addMarker(options);
        //mapMarker.setDraggable(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 18));
        //GeoLocation geoLocation = new GeoLocation(this); // or (this) if called from an activity and not from a fragment
        //mCurrentLocation.lookUpAddress(googleMap);
        userAddress = getCompleteAddressString(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());


    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        MarkerOptions options = new MarkerOptions().snippet("new location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer))
                .anchor(0.5f, 0.5f);
        options.position(latLng);
        // options.title(latLng.latitude,latLng.longitude);
        googleMap.clear();
        // LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        new GetRestaturantData(mContext, false, latLng, restaurantListner).execute();
        //new GetRestaturantData(true, latLng).execute();

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        userAddress = getCompleteAddressString(latLng.latitude, latLng.longitude);
        //googleMap.addMarker(options);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            final LatLng currentlocation = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.addMarker(new MarkerOptions()
                    .position(currentlocation)
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
//            googleMap.addMarker(new MarkerOptions().position(currentlocation).title("Current Location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentlocation,14));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentlocation,14));
            new GetRestaturantData(this, false, currentlocation, new RestaurantListner() {

                @Override
                public void returnRestaurantList(ArrayList<Restaurant> arrRestaurant) {
                    LatLngBounds BOUNDS = new LatLngBounds(new LatLng(77.580643, 12.972442), new LatLng(77.611392, 13.000878));
//                    if (BOUNDS.contains(currentlocation)) {
//                    }
                }
            }).execute();
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

    }
}
