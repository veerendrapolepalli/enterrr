package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.OrderSummaryAdapter;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.EntryItem;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.constructors.RestaurantOrders;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.interfacelistener.Items;
import com.entree.entree.interfacelistener.TrayUpdateListener;
import com.entree.entree.utils.Utils;
import com.parse.ParseUser;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrayOrderSummary extends BaseActivity {

    ArrayList<Items> items = new ArrayList<Items>();
    public static ListView lvOrderSummary;
    Context mContext;
    ArrayList<OrderItem> arrOrder = new ArrayList<OrderItem>();
    int totalPrice = 0;
    TextView tvTotalPrice;
    boolean isRestSummary = false;
    RestaurantOrders restOrders;
    RelativeLayout rlProceed;
    TrayUpdateListener onTrayUpdate = null;
    public static RelativeLayout emptyTray;
    public static LinearLayout emptyTraytext, llcontactdetails;
    public static RelativeLayout relEmptyLayout;
    public static LinearLayout llNoOrders;
    public static LinearLayout llOrderFooter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_order_summary);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        relEmptyLayout = (RelativeLayout) findViewById(R.id.relEmptylayout);
        lvOrderSummary = (ListView) findViewById(R.id.lvOrderSummary);
        //emptyTray = (RelativeLayout) findViewById(R.id.relEmptylayout);
        // emptyTraytext = (LinearLayout) findViewById(R.id.emptytraytext);
        llNoOrders = (LinearLayout) findViewById(R.id.llcontactdetails);
        llcontactdetails = (LinearLayout) findViewById(R.id.llcontactdetails);
        llOrderFooter = (LinearLayout) findViewById(R.id.llOrderFooter);
        rlProceed = (RelativeLayout) findViewById(R.id.llproceed);

        rlProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, TrayContactActivity.class);
                i.putExtra("Total_Price", totalPrice);
                startActivity(i);
            }
        });
        tvTotalPrice = (TextView) findViewById(R.id.tvtotalprice);

        mContext = this;
        initListener();
        setTitle("Tray");
        parseIntent();
        if (arrOrder.size() <=0) {
            relEmptyLayout.setVisibility(View.VISIBLE);
            rlProceed.setVisibility(View.GONE);
            llcontactdetails.setVisibility(View.GONE);
        } else {
            relEmptyLayout.setVisibility(View.GONE);
            rlProceed.setVisibility(View.VISIBLE);
            llcontactdetails.setVisibility(View.VISIBLE);
        }
        if (arrOrder.size() > 0)
            getOrderBasedOnRestaurant();

    }

    private void initListener() {
        onTrayUpdate = new TrayUpdateListener() {
            @Override
            public void onTrayUpdate() {
                if (EntreeApplication.arrUserOtderItems.size() > 0) {
                    arrOrder = EntreeApplication.arrUserOtderItems;
                    getOrderBasedOnRestaurant();

//                    arrOrder.clear();
//                    totalPrice = 0;
                }
                else {
                    llNoOrders.setVisibility(View.GONE);
                    llOrderFooter.setVisibility(View.GONE);
                    relEmptyLayout.setVisibility(View.VISIBLE);

                }
            }
        };
    }

    private void parseIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra("SELECTEDORDER")) {
            if (intent.getBooleanExtra("SELECTEDORDER", false)) {
                restOrders = OrderHistoryActivity.restOrder;
                arrOrder = restOrders.getOrderItems();
                isRestSummary = true;
            } else {
                if (EntreeApplication.arrUserOtderItems.size() > 0) {
                    arrOrder = EntreeApplication.arrUserOtderItems;
                }
            }
        } else {
            if (EntreeApplication.arrUserOtderItems.size() > 0) {
                arrOrder = EntreeApplication.arrUserOtderItems;
            }
        }
    }

    private int getNavigationBarHeigth() {
        Resources resources = mContext.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private void getOrderBasedOnRestaurant() {
        ArrayList<String> arrRestID = new ArrayList<String>();
        items = new ArrayList<Items>();
        totalPrice = 0;
        for (OrderItem item : arrOrder) {
            ArrayList<Items> orderItems = new ArrayList<Items>();

            String resId = item.getResID();
            if (!arrRestID.contains(resId)) {
                arrRestID.add(resId);
                Restaurant resObj = item.getResObj();
                ArrayList<OrderItem> arrRestOrders = rest(resId);
                int price = 0;
                //items.add(new SectionItem(resObj.getResName(),String.valueOf(price)));
                for (OrderItem order : arrRestOrders) {
                    totalPrice = totalPrice + order.getItemTotal();
                    orderItems.add(new EntryItem(order));
                    price = price + order.getItemTotal();
                }
                if (isRestSummary) {
                    items.add(new SectionItem(restOrders.getRestName(), String.valueOf(restOrders.getTotal()), null));
                } else {
                    items.add(new SectionItem(resObj.getResName(), String.valueOf(price), resObj));
                }
                items.addAll(orderItems);
            }
        }
        if (items != null) {
            // emptyTray.setVisibility(View.GONE);
            //  emptyTraytext.setVisibility(View.GONE);
            lvOrderSummary.setVisibility(View.VISIBLE);
            OrderSummaryAdapter adapter = new OrderSummaryAdapter(getApplicationContext(), items, onTrayUpdate);
            lvOrderSummary.setAdapter(adapter);
        } else {
            lvOrderSummary.setVisibility(View.GONE);
            emptyTray.setVisibility(View.VISIBLE);
            emptyTraytext.setVisibility(View.VISIBLE);
        }
        tvTotalPrice.setText(mContext.getResources().getString(R.string.Rs) + " " + totalPrice);
    }

    private ArrayList<OrderItem> rest(String resID) {
        ArrayList<OrderItem> restOrder = new ArrayList<OrderItem>();
        for (OrderItem item : arrOrder) {
            if (item.getResID() != null && item.getResID().equalsIgnoreCase(resID)) {
                restOrder.add(item);
            }
        }
        return restOrder;
    }

    private Utils utils = null;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        items.clear();
    }
}
