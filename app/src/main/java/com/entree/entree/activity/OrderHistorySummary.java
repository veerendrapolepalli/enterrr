package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.adapter.OrderHistorySummaryAdapter;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.EntryItem;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.RestaurantOrders;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.constructors.UserOrder;
import com.entree.entree.interfacelistener.Items;
import com.entree.entree.utils.Utils;
import com.parse.ParseUser;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderHistorySummary extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Context mContext;
    Utils utils;
    UserOrder userOrderObj = null;
    ListView lvOrderSummary;
    ArrayList<OrderItem> arrOrder = new ArrayList<OrderItem>();
    int totalPrice = 0;
    TextView tvTotalPrice;
    boolean isRestSummary = false;
    Button btnRedo;
    RestaurantOrders restOrders;
    ArrayList<Items> items = new ArrayList<Items>();
    String priceAmt = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_summary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = this;
        utils = new Utils(mContext);

        lvOrderSummary = (ListView) findViewById(R.id.lvOrderSummary);
        btnRedo = (Button) findViewById(R.id.btnRedo);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        TextView tvUserName = (TextView) headerView.findViewById(R.id.tvUserName);
        TextView tvEmailId = (TextView) headerView.findViewById(R.id.tvEmailId);
        CircleImageView ivProfileicon = (CircleImageView) headerView.findViewById(R.id.ivNavBarProfileIcon);
        tvEmailId.setText(ParseUser.getCurrentUser().getUsername());
        parseIntent();
        setOnClickEvents();
        new SetOrderSummary().execute();
    }

    private void parseIntent() {
        Intent intent = getIntent();
        if (intent.hasExtra("SELECTEDORDER")) {
            if (intent.getBooleanExtra("SELECTEDORDER", false)) {
                restOrders = OrderHistoryActivity.restOrder;
                arrOrder = restOrders.getOrderItems();
                isRestSummary = true;
            }
        }
    }

    private void setOnClickEvents() {
        btnRedo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ReorderFlow reorderFlow = new ReorderFlow(mContext, items, priceAmt);
                EntreeApplication.arrUserOtderItems.clear();
                EntreeApplication.arrUserOtderItems.addAll(arrOrder);
                Intent itnet = new Intent(OrderHistorySummary.this,TrayOrderSummary.class);
                startActivity(itnet);


            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.order_history_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        utils.navigationOnClick(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class SetOrderSummary extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (items.size() > 0) {
                OrderHistorySummaryAdapter orderHistorySummaryAdapter =
                        new OrderHistorySummaryAdapter(mContext, items);
                lvOrderSummary.setAdapter(orderHistorySummaryAdapter);
            }
            utils.hideLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {
            items.add(new SectionItem(restOrders.getRestName(), String.valueOf(restOrders.getTotal())));
            priceAmt = getResources().getString(R.string.Rs) + " " + String.valueOf(restOrders.getTotal());
            for (OrderItem oItem : arrOrder) {
                items.add(new EntryItem(oItem));
            }
            return null;
        }
    }
}
