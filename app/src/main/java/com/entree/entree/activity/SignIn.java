package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LogUtilities;
import com.entree.entree.utils.SnackBarUtils;
import com.entree.entree.utils.Utils;
import com.entree.entree.views.RobottoCondencedBold;
import com.entree.entree.views.RobottowCondenced;
import com.facebook.CallbackManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseQuery;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.ArrayList;
import java.util.List;

public class SignIn extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Utils utils = null;
    private LogUtilities logUtils = null;
    private static final int BTN_SIGN_IN = 1;
    private CoordinatorLayout coordinatorLayout;
    private Context mContext;
    private RobottoCondencedBold btnSignIn;
    private EditText etEmail, etPwd;
    private ProgressBar progressBar;
    private SnackBarUtils snackbar;
    public static String Email_id;
    private ImageButton fbLoginBtn;
    private ImageButton twitterLogin, btnGooglePlus;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 0;
    private ConnectionResult mConnectionResult;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private Typeface avenir_font;
    private RobottowCondenced tvForgotpwd;
    public static CallbackManager callbackmanager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        setContentView(R.layout.activity_sign_in);
        overridePendingTransition(R.anim.bottom_top, R.anim.top_bottom);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mContext = this;
        setUtils();
        setUI();
        initListener();
        initGooglePlus();
        initSocialLogin();
    }

    private void setUtils() {
        utils = new Utils(mContext);
        logUtils = new LogUtilities(mContext);
    }

    private void setUI() {
        avenir_font = Typeface.createFromAsset(mContext.getAssets(), "fonts/AvenirLTStd-Medium.otf");
        btnSignIn = (RobottoCondencedBold) findViewById(R.id.btnSignIn);
        etEmail = (EditText) findViewById(R.id.input_email);
        tvForgotpwd = (RobottowCondenced) findViewById(R.id.tvForgotpwd);
        etPwd = (EditText) findViewById(R.id.input_password);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        fbLoginBtn = (ImageButton) findViewById(R.id.btnFBLogin);
        twitterLogin = (ImageButton) findViewById(R.id.btnTwitter);
        btnGooglePlus = (ImageButton) findViewById(R.id.btnGooglePlus);
        setupUI(findViewById(R.id.coordinatorLayout));

        //btnSignIn.setTypeface(avenir_font);
        etEmail.setTypeface(avenir_font);
        etPwd.setTypeface(avenir_font);

        tvForgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fIntent = new Intent(SignIn.this, ForgotPasswordActivity.class);
                startActivity(fIntent);
            }
        });
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    utils.hideKeyBoard();
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    private void initGooglePlus() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();
    }

    private void initSocialLogin() {
        ParseTwitterUtils.initialize(AppConstants.TWITTER_CUSTOMER_ID, AppConstants.TWITTER_CUSTOMER_KEY);
    }


    // Private method to handle Facebook login and callback
    private void onFblogin() {
        final List<String> permissions = new ArrayList<String>();
        permissions.add("public_profile");
        permissions.add("user_status");
        permissions.add("user_friends");

        ParseFacebookUtils.logInWithReadPermissionsInBackground(SignIn.this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                } else if (user.isNew()) {
                    signInUsers();
                    Log.d("MyApp", "User signed up and logged in through Facebook!");
                } else {
                    signInUsers();
                    Log.d("MyApp", "User logged in through Facebook!");
                }
            }
        });
    }

    //private method to handle twitter
    private void twitterSignIn() {
        ParseTwitterUtils.logIn(this, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d("MyApp", "Uh oh. The user cancelled the Twitter login.");
                } else if (user.isNew()) {
                    signInUsers();
                    Log.d("MyApp", "User signed up and logged in through Twitter!");
                } else {
                    signInUsers();
                    Log.d("MyApp", "User logged in through Twitter!");
                }
            }
        });
    }

    private void signInUsers() {
        Intent i = new Intent(mContext, UserLocationActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void initListener() {
        btnSignIn.setId(BTN_SIGN_IN);
        btnSignIn.setOnClickListener(onClickListener);
        etPwd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    utils.hideKeyBoard();
                    btnSignIn.performClick();
                    return true;
                }
                return false;
            }
        });
        twitterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                twitterSignIn();
            }
        });

        fbLoginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Call private metho
                onFblogin();
            }
        });
        btnGooglePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case BTN_SIGN_IN:

                    signInUser();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
        // Get user's information
//        getProfileInformation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    /**
     * Sign-in into google
     */
    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    /**
     * Method to resolve any signin errors
     */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     */
    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);

                final String personName = currentPerson.getDisplayName();
                final String personPhotoUrl = currentPerson.getImage().getUrl();
                final String personGooglePlusProfile = currentPerson.getUrl();
                final String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                final String pwd = currentPerson.getId();

                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo("username", email);
                query.findInBackground(new FindCallback<ParseUser>() {
                    public void done(List<ParseUser> objects, ParseException e) {
                        if (e == null) {
                            if (objects.size() < 1) {
                                final ParseUser user = new ParseUser();
                                user.setUsername(email);
                                user.setPassword(pwd);
                                user.setEmail(email);


                                user.signUpInBackground(new SignUpCallback() {
                                    public void done(ParseException e) {
                                        if (e != null) {
                                            snackbar = new SnackBarUtils(mContext, coordinatorLayout);
                                            snackbar.showSnackbar(e.getMessage());
                                            logUtils.log(e.getLocalizedMessage());
                                        } else {
                                            user.put("Name", personName);
                                            user.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    if (e != null) {
                                                        snackbar = new SnackBarUtils(mContext, coordinatorLayout);
                                                        snackbar.showSnackbar(e.getMessage());
                                                        logUtils.log(e.getLocalizedMessage());
                                                    } else {
                                                        signInUsers();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                showProgressbar();
                                ParseUser.logInInBackground(email, pwd, new LogInCallback() {
                                    public void done(ParseUser user, ParseException e) {
                                        hideprogressBar();
                                        if (user != null) {
                                            signInUsers();
                                            // Hooray! The user is logged in.
                                        } else {
                                            snackbar = new SnackBarUtils(mContext, coordinatorLayout);
                                            snackbar.showSnackbar(e.getMessage());
                                            logUtils.log(e.getLocalizedMessage());
                                            // Signup failed. Look at the ParseException to see what happened.
                                        }
                                    }
                                });
                            }
                        } else {

                        }
                    }
                });

                Log.e("GOOGLE LOGIN RESULT", "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);

            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Async Task Class
    class SignInAsync extends AsyncTask<String, String, String> {

        // Show Progress bar before downloading Music
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // Download Music File from Internet
        @Override
        protected String doInBackground(String... f_url) {
            int count;


            return null;
        }

        // While Downloading Music File
        protected void onProgressUpdate(String... progress) {

        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(String file_url) {
            // Dismiss the dialog after the Music file was downloaded

        }

    }

    private void signInUser() {
       /* String regexStr = "^[0-9]*$";
        if (utils.checkNetworkAvailabilityWithToast(coordinatorLayout, true)) {
            final String email = etEmail.getText().toString();
            final String pwd = etPwd.getText().toString();
           // Toast.makeText(mContext, email + pwd, Toast.LENGTH_SHORT).show();
            if (formValidate(email, pwd)) {
                showProgressbar();
                if (email.contains("@")) {
                    ParseQuery<ParseUser> query = ParseUser.getQuery();
                    query.whereEqualTo("email", email);
                    query.getFirstInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser object, ParseException e) {
                            if (object != null) {
                                hideprogressBar();
                                ParseUser.logInInBackground(object.getString("username"), pwd, new LogInCallback() {
                                    public void done(ParseUser user, ParseException e) {
                                        if (user != null) {
                                            Intent i = new Intent(mContext, UserLocationActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.putExtra("Email", email);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            snackbar = new SnackBarUtils(mContext, coordinatorLayout, 3000);
                                            snackbar.showSnackbar(e.getMessage());
                                            logUtils.log(e.getLocalizedMessage());
                                            Log.v("login", "fail");
                                        }
                                    }
                                });
                            } else {
                                snackbar = new SnackBarUtils(mContext, coordinatorLayout, 3000);
                                snackbar.showSnackbar(e.getMessage());
                                logUtils.log(e.getLocalizedMessage());
                            }
                        }
                    });
                } else if(email.matches(regexStr)){
                    ParseQuery<ParseUser> query = ParseUser.getQuery();
                    query.whereEqualTo("MobileNo", email);
                    query.getFirstInBackground(new GetCallback<ParseUser>() {
                        @Override
                        public void done(ParseUser object, ParseException e) {
                            if (object != null) {
                                hideprogressBar();
                                ParseUser.logInInBackground(object.getString("username"), pwd, new LogInCallback() {
                                    public void done(ParseUser user, ParseException e) {
                                        if (user != null) {
                                            Intent i = new Intent(mContext, UserLocationActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.putExtra("Email", email);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            snackbar = new SnackBarUtils(mContext, coordinatorLayout, 3000);
                                            snackbar.showSnackbar(e.getMessage());
                                            logUtils.log(e.getLocalizedMessage());
                                            Log.v("login", "fail");
                                        }
                                    }
                                });
                            } else {
                                snackbar = new SnackBarUtils(mContext, coordinatorLayout, 3000);
                                snackbar.showSnackbar(e.getMessage());
                                logUtils.log(e.getLocalizedMessage());
                            }
                        }
                    });
                }else {*/
                    hideprogressBar();
                    ParseUser.logInInBackground(etEmail.getText().toString(), etPwd.getText().toString(), new LogInCallback() {
                        public void done(ParseUser user, ParseException e) {
                            Log.v("user--", "" + user);
                            if (user != null) {
                                Intent i = new Intent(mContext, UserLocationActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                i.putExtra("Email", email);
                                startActivity(i);
                                finish();
                                // Hooray! The user is logged in.
                            } else {
                                snackbar = new SnackBarUtils(mContext, coordinatorLayout, 3000);
                                snackbar.showSnackbar(e.getMessage());
                                logUtils.log(e.getLocalizedMessage());
                            }
                        }
                    });
//                }
//            }
//        }
    }

    private boolean formValidate(String email, String pass) {
        if (email == null || email.equalsIgnoreCase("")) {
            etEmail.setError("Email cannot be left empty");
            return false;
        }
       /* if (!utils.isValidEmail(email)) {
            etEmail.setError("Invalid Email");
            return false;
        }*/
        if (pass == null || pass.equalsIgnoreCase("")) {
            etPwd.setError("Password cannot be left empty");
            return false;
        }
        return true;
    }

    private void showProgressbar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideprogressBar() {
        progressBar.setVisibility(View.GONE);
    }


}
