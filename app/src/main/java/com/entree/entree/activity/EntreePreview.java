package com.entree.entree.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.entree.entree.R;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.utils.LocationManager_check;
import com.entree.entree.utils.SnackBarUtils;
import com.entree.entree.utils.Utils;
import com.entree.entree.views.RobottoCondencedBold;
import com.entree.entree.views.RobottowCondenced;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;

import io.fabric.sdk.android.Fabric;

public class EntreePreview extends AppCompatActivity {
    private RobottoCondencedBold signIn, signUp;
    private Context mContext;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private FrameLayout frameSplashImage;
    private RelativeLayout rlHome;
    private ImageView ivSplashImage;
    private static final int LOGIN_ID = 1;
    private static final int SIGN_UP_ID = 2;
    private Utils utils = null;
    private LocationManager_check locationManagerCheck = null;
    private SnackBarUtils snackbar;
    Typeface avenirFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MultiDex.install(getBaseContext());
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_entree_preview);
        mContext = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        initialiseUI();
        setUtils();


    }


    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(mContext);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(mContext);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if(resultCode == RESULT_OK) {
        locationManagerCheck = new LocationManager_check(mContext);
        if (locationManagerCheck.isLocationServiceAvailable()) {
            frameSplashImage.postDelayed(new Runnable() {
                @Override
                public void run() {

                    initParse();
                    initListener();
                }
            }, 3000);
        } else {
            finish();
        }
        //}
    }

    private void initParse() {
        if (utils.checkNetworkAvailabilityWithToast(coordinatorLayout, true)) {
            try {

                Parse.enableLocalDatastore(this);
//                Parse.initialize(this, AppConstants.PARSE_APPLICATION_ID, AppConstants.PARSE_CLIENT_ID);
                Parse.initialize(new Parse.Configuration.Builder(this)
                                .applicationId(AppConstants.PARSE_APPLICATION_ID)
                                .clientKey(AppConstants.PARSE_CLIENT_ID)
                                .server("http://betatesting.eatentree.com/parse/")
                                .build());
                ParseFacebookUtils.initialize(mContext);
                ParseTwitterUtils.initialize("xcXkY4tRksPzLNR7MkkeUL9fn", "pokb6UYOnjZahOfea7Rhy2ipeSsEnFIAkwtiR4ZD6zhftIMFel");
            } catch (Exception e) {

            }
            checkIfUserLoggedIn();
        } else {
            snackbar = new SnackBarUtils(mContext, coordinatorLayout);
            snackbar.setAction(true);
            snackbar.showSnackbar("Please connect to Network");
        }
    }

    private void checkIfUserLoggedIn() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            Intent i = new Intent(mContext, UserLocationActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
            // do stuff with the user
        } else {

            rlHome.setVisibility(View.VISIBLE);
            // show the signup or login screen
        }
    }

    private void setUtils() {
        utils = new Utils(mContext);
        locationManagerCheck = new LocationManager_check(mContext);
        utils.setDebugMode();
        rlHome.setVisibility(View.GONE);
        enableLocation();

    }

    private void enableLocation() {
        if (locationManagerCheck.isLocationServiceAvailable()) {

            if (locationManagerCheck.getProviderType() == 3) {
                locationManagerCheck.createLocationServiceError(EntreePreview.this);
            } else if (locationManagerCheck.getProviderType() == 2) {
                snackbar = new SnackBarUtils(mContext, coordinatorLayout);
                snackbar.setAction(true);
                snackbar.showSnackbar("Please connect to Network");

            } else {
                frameSplashImage.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        frameSplashImage.setVisibility(View.GONE);
                        initParse();
                        initListener();
                    }
                }, 3000);
            }
        } else {
            locationManagerCheck.createLocationServiceError(EntreePreview.this);
        }
    }

    private void initialiseUI() {
        avenirFont = Typeface.createFromAsset(mContext.getAssets(),"fonts/AvenirLTStd-Medium.otf");
        TextView tvBestFood = (TextView)findViewById(R.id.tvBestFood);
        TextView splashtext = (TextView)findViewById(R.id.tvSplashText);
        tvBestFood.setTypeface(avenirFont);
        splashtext.setTypeface(avenirFont);
        signIn = (RobottoCondencedBold) findViewById(R.id.signIn);
        signUp = (RobottoCondencedBold) findViewById(R.id.signUp);
        signIn.setTypeface(avenirFont);
        signUp.setTypeface(avenirFont);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        frameSplashImage = (FrameLayout) findViewById(R.id.frameSplashImage);
//        ivSplashImage = (ImageView) findViewById(R.id.splashImage);
        rlHome = (RelativeLayout) findViewById(R.id.rlHome);
    }

    private void initListener() {
        signIn.setId(LOGIN_ID);
        signUp.setId(SIGN_UP_ID);

        signIn.setOnClickListener(onClickListener);
        signUp.setOnClickListener(onClickListener);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case LOGIN_ID:
                    startSignIn();
                    break;
                case SIGN_UP_ID:
                    startSignUp();
                    break;
                default:
                    break;
            }
        }
    };

    private void startSignIn() {
        Intent i = new Intent(mContext, SignIn.class);
//        Intent i = new Intent(mContext,RestaurantList.class);
        startActivity(i);
        //finish();
    }

    private void startSignUp() {
        Intent i = new Intent(mContext, SignUp.class);
        startActivity(i);
        //finish();
    }

}
