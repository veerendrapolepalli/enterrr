package com.entree.entree.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.entree.entree.R;
import com.entree.entree.activity.StoreActivity;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.Food;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.interfacelistener.FoodChangeListener;
import com.entree.entree.interfacelistener.TrayUpdateListener;
import com.entree.entree.utils.AppConstants;
import com.entree.entree.views.AvenirBook;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by cYbEr on 1/7/2016.
 */
public class FoodGridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Food> arrFood = new ArrayList<Food>();
    private LayoutInflater inflater;
//    DisplayImageOptions options;

    private FoodChangeListener fcListener;
    private TrayUpdateListener trayUpdateListener;
    private Restaurant resObj;
    Boolean enableGridView = true;
    boolean doSwitch = true;

    AvenirBook foodqty;

    private SparseArray<ViewHolder> holderMap = new SparseArray<ViewHolder>();

    private int orderCost = 0;

    private static final int FOOD_FRAME_CLICK = 0;
    private static final int BTN_ADD_CLICK = 1;
    private static final int BTN_REMOVE_CLICK = 2;
    private static final int BTN_CANCEL_CLICK = 3;
    int Position;

    public FoodGridAdapter(Context mContext, ArrayList<Food> arrFood, Restaurant resObj, ImageLoader imageLoad) {
        this.mContext = mContext;
        this.arrFood = arrFood;
        this.resObj = resObj;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setLayoutType(int type) {
        switch (type) {
            case AppConstants.TYPE_GRID_VIEW:
                this.enableGridView = true;
                break;
            case AppConstants.TYPE_LIST_VIEW:
                this.enableGridView = false;
                break;
        }
    }

    @Override
    public int getCount() {
        return arrFood.size();
    }

    @Override
    public Food getItem(int position) {
        return arrFood.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Food food = arrFood.get(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.food_grid_item, null);
            holder = new ViewHolder();

            holder.foodImage = (ImageView) convertView.findViewById(R.id.foodImage);
            holder.foodName = (AvenirBook) convertView.findViewById(R.id.foodName);
            holder.foodPrice = (AvenirBook) convertView.findViewById(R.id.foodPrice);
            foodqty = (AvenirBook) convertView.findViewById(R.id.qty);
            holder.foodDesc = (AvenirBook) convertView.findViewById(R.id.foodDesc);
            holder.foodquantity = (AvenirBook) convertView.findViewById(R.id.qty);
            holder.vegOnly = (ImageView) convertView.findViewById(R.id.ivvegIndicator);
            holder.VetnNonVeg = (ImageView) convertView.findViewById(R.id.ivNonVegIndicator);

            holder.ffFoodContent = (FrameLayout) convertView.findViewById(R.id.ffFoodContent);
            holder.llAddFood = (LinearLayout) convertView.findViewById(R.id.llAddFood);
            holder.btnRemoveFood = (Button) convertView.findViewById(R.id.btnRemoveFood);
            holder.btnAddFood = (Button) convertView.findViewById(R.id.btnAddFood);
            holder.btnCancel = (Button) convertView.findViewById(R.id.btnCancel);

            foodqty.setTypeface(Typeface.DEFAULT_BOLD);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.ffFoodContent.setTag(position);
        holder.btnAddFood.setTag(position);
        holder.btnRemoveFood.setTag(position);
        holder.btnCancel.setTag(position);

        holder.ffFoodContent.setId(FOOD_FRAME_CLICK);
        holder.btnRemoveFood.setId(BTN_REMOVE_CLICK);
        holder.btnAddFood.setId(BTN_ADD_CLICK);
        holder.btnCancel.setId(BTN_CANCEL_CLICK);

        holder.ffFoodContent.setOnClickListener(onclick);
        holder.btnRemoveFood.setOnClickListener(onclick);
        holder.btnAddFood.setOnClickListener(onclick);
        holder.btnCancel.setOnClickListener(onclick);


        String imageUri = food.getFoodImage();

        if (imageUri != null && !imageUri.equalsIgnoreCase("")) {
            Picasso.with(parent.getContext()).load(imageUri).placeholder(R.drawable.placeholderimg).fit().into(holder.foodImage);
        }

        if (food.getVegOnly().equalsIgnoreCase("Y")) {
            holder.vegOnly.setVisibility(View.VISIBLE);
            holder.VetnNonVeg.setVisibility(View.GONE);
        } else {
            holder.VetnNonVeg.setVisibility(View.VISIBLE);
            holder.vegOnly.setVisibility(View.GONE);
        }

//        holder.foodquantity.setText(qty);
        holder.foodName.setText(food.getFoodName());
        holder.foodDesc.setText(food.getFoodDesc());
        holder.foodPrice.setText(mContext.getResources().getString(R.string.Rs) + " " + String.valueOf(food.getPrice()));

        holderMap.put(position, holder);


        return convertView;
    }

    public void setOnDataChangeListener(FoodChangeListener onDataChangeListener) {
        fcListener = onDataChangeListener;
        int price = calculateTotalCost();
        if (fcListener != null)
            fcListener.onDataChanged(price);
    }

    private View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case FOOD_FRAME_CLICK:
                    setAddView(v);
                    break;
                case BTN_ADD_CLICK:
                    setPositon((Integer)v.getTag());
                    addOrderItem(v);
                    StoreActivity.updateBatch(v);
                    break;
                case BTN_CANCEL_CLICK:
                    setPositon((Integer)v.getTag());
                    removeAddView(v);
                    break;
                case BTN_REMOVE_CLICK:
                    setPositon((Integer)v.getTag());
                    removeOrderItem(v);
                    StoreActivity.updateBatch(v);
                    break;
                default:
                    break;
            }
        }
    };

    private void setPositon(int position){
        this.Position = position;
    }

    private int getPosition(){
        return Position;
    }

    private void setAddView(View v) {

        int position = (Integer) v.getTag();

        LinearLayout llParent = (LinearLayout) v.getParent();
        LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
        LinearLayout llFooter = (LinearLayout) llParent.findViewById(R.id.llOrderFooter);



        if (llAddNew.getVisibility() == View.GONE) {
            llAddNew.setVisibility(View.VISIBLE);
            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.transHomeColor));
        }else{
            llAddNew.setVisibility(View.GONE);
            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.listViewFooterLayout));
        }
    }

    private OrderExists checkIfItemExists(String foodId) {
        for (OrderItem item : EntreeApplication.arrUserOtderItems) {
            if (item.getFoodId() != null && item.getFoodId().equalsIgnoreCase(foodId)) {
                return new OrderExists(EntreeApplication.arrUserOtderItems.indexOf(item), true);
            }
        }
        return null;
    }

    private void addOrderItem(View v) {
        int position = getPosition();
        Food fdObj = arrFood.get(position);
        ParseObject fc = fdObj.getFoodCategory();
        ViewHolder holder = holderMap.get(position);
//        AvenirBook foodqty = (AvenirBook)v.findViewById(R.id.qty);
//        View v1= position;

        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());

        if (oExists != null && oExists.exists) {
            OrderItem oItem = EntreeApplication.arrUserOtderItems.get(oExists.position);
            int price = oItem.getPrice();
            int qty = oItem.getQty();
            int itemTotal = oItem.getItemTotal();
            qty = qty + 1;
            itemTotal = qty * price;
            EntreeApplication.arrUserOtderItems.get(oExists.position).setItemTotal(itemTotal);
            EntreeApplication.arrUserOtderItems.get(oExists.position).setQty(qty);
            holder.foodquantity.setText("x".concat(String.valueOf(qty)));
        } else {

            try {

                String foodcategory = (String)fc.fetchIfNeeded().get("FoodCategoryName");
                OrderItem oItem = new OrderItem(null, resObj, fdObj.getFoodName(),
                        fdObj.getFoodDesc(), foodcategory ,
                        fdObj.getVegOnly(), 1, (int) fdObj.getPrice(),
                        (int) fdObj.getPrice(), "", fdObj.getFoodId(), resObj.getRestaurantId());
                EntreeApplication.arrUserOtderItems.add(oItem);
                holder.foodquantity.setVisibility(View.VISIBLE);
                holder.foodquantity.setText("x".concat(String.valueOf(1)));
                } catch (com.parse.ParseException e) {
                e.printStackTrace();
            }

        }
        int price = calculateTotalCost();
        if (fcListener != null) {
            fcListener.onDataChanged(price);
        }
    }

    private void removeOrderItem(View v) {
        int position = getPosition();
        Food fdObj = arrFood.get(position);
        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());
//        AvenirBook foodqty = (AvenirBook)v;
        ViewHolder holder = holderMap.get(position);

        if (oExists != null && oExists.exists) {
            OrderItem oItem = EntreeApplication.arrUserOtderItems.get(oExists.position);
            int price = oItem.getPrice();
            int qty = oItem.getQty();
            int itemTotal = oItem.getItemTotal();
            if (qty > 1) {
                qty = qty - 1;
                itemTotal = qty * price;
                EntreeApplication.arrUserOtderItems.get(oExists.position).setItemTotal(itemTotal);
                EntreeApplication.arrUserOtderItems.get(oExists.position).setQty(qty);
                holder.foodquantity.setText("x".concat(String.valueOf(qty)));
            } else if (qty == 1) {
                holder.foodquantity.setText("");
                holder.foodquantity.setVisibility(View.GONE);
                EntreeApplication.arrUserOtderItems.remove(oExists.position);
            }
        }


        int price = calculateTotalCost();
        if (fcListener != null) {
            fcListener.onDataChanged(price);
        }
    }

    private void removeAddView(View v) {

        int position = getPosition();
        Food fdObj = arrFood.get(position);
        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());

        ViewHolder holder = holderMap.get(position);

        if (oExists != null && oExists.exists) {
            EntreeApplication.arrUserOtderItems.remove(oExists.position);

            int price = calculateTotalCost();
            if (fcListener != null) {
                fcListener.onDataChanged(price);
            }
            holder.foodquantity.setVisibility(View.GONE);
            LinearLayout llParent = (LinearLayout) v.getParent();
            LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
            LinearLayout llFooter = (LinearLayout) ((LinearLayout) llAddNew.getParent()).findViewById(R.id.llOrderFooter);
            llFooter.setBackgroundColor(Color.parseColor("#85ffffff"));
            llAddNew.setVisibility(View.GONE);


        }else {

            LinearLayout llParent = (LinearLayout) v.getParent();
            LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
            LinearLayout llFooter = (LinearLayout) ((LinearLayout) llAddNew.getParent()).findViewById(R.id.llOrderFooter);
            llFooter.setBackgroundColor(Color.parseColor("#e5fdfdfd"));
            llAddNew.setVisibility(View.GONE);
        }
    }

    private int calculateTotalCost() {
        int price = 0;
        for (OrderItem item : EntreeApplication.arrUserOtderItems) {
            price = price + item.getItemTotal();
        }
        return price;
    }

    public class ViewHolder {
        AvenirBook foodName;
        AvenirBook foodDesc;
        AvenirBook foodPrice;
        AvenirBook foodquantity;
        ImageView foodImage;
        FrameLayout ffFoodContent;
        LinearLayout llAddFood;
        Button btnRemoveFood;
        Button btnAddFood;
        Button btnCancel;
        ImageView vegOnly, VetnNonVeg;
    }

    private class OrderExists {
        public int position;
        public Boolean exists;

        public OrderExists(int position, Boolean exists) {
            this.position = position;
            this.exists = exists;
        }
    }



}
