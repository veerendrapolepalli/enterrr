package com.entree.entree.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entree.entree.constructors.FoodCategory;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.interfacelistener.UpdateFoodView;
import com.entree.entree.loader.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jesudass on 1/6/2016.
 */
public class TabAdapter extends FragmentPagerAdapter {
    private ArrayList<FoodCategory> arrFoodcategory;
    private Context mContext;
    private String resID;
    public Restaurant resObj;
    public LinearLayout llTray;
    public ImageButton btnTray;
    public TextView tvTotalPrice;
    private UpdateFoodView updateFoodView;
    public static HashMap<Integer,PlaceholderFragment> hMapPlaceHolder = new  HashMap<Integer,PlaceholderFragment>();
    ImageLoader imageLoader;

    public TabAdapter(FragmentManager fm,ArrayList<FoodCategory> arrFoodcategory,Context mContext
            ,Restaurant resObj,ImageLoader imageLoader) {
        super(fm);
        this.arrFoodcategory = arrFoodcategory;
        this.mContext = mContext;
        this.resObj=resObj;
        this.imageLoader = imageLoader;
    }

    public void setOnDataChangeListener(UpdateFoodView onDataChangeListener) {
        updateFoodView = onDataChangeListener;
        if (updateFoodView != null)
            updateFoodView.onViewChanged();
    }

    public void updateTabView(int position){
        PlaceholderFragment pFragment = hMapPlaceHolder.get(position);
        pFragment.notifyDataSetChanged();
    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        PlaceholderFragment pFragment = new PlaceholderFragment();
        pFragment =  pFragment.newInstance(position + 1,arrFoodcategory.get(position),resObj,imageLoader);
        if(hMapPlaceHolder.containsKey(position)) {
            hMapPlaceHolder.remove(position);
        }
        hMapPlaceHolder.put(position,pFragment);
        return pFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return arrFoodcategory.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return arrFoodcategory.get(position).getFoodCategoryname();
    }
}


