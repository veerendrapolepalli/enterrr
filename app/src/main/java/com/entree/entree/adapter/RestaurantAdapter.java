package com.entree.entree.adapter;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.loader.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by jesudass on 1/5/2016.
 */
public class RestaurantAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Restaurant> arrRestaurant = new ArrayList<Restaurant>();
    private LayoutInflater inflater;

    private ImageLoader imageLoader;
    int width = 0;
    int height = 0;



    public RestaurantAdapter(Context mContext,ArrayList<Restaurant> arrRestaurant){
        this.mContext = mContext;
        this.arrRestaurant = arrRestaurant;
        this.inflater = LayoutInflater.from(mContext);

        if (null == imageLoader) {
            imageLoader = new ImageLoader(mContext);
        }


        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;
        height=100;


    }
    @Override
    public int getCount() {
        return arrRestaurant.size();
    }

    @Override
    public Restaurant getItem(int position) {
        return arrRestaurant.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Restaurant res = arrRestaurant.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.restaurant_list_item, null);
            // Locate the ImageView in listview_item.xml
            holder.resBannerImage = (ImageView) convertView.findViewById(R.id.resBannerImage);
            holder.resName =(TextView) convertView.findViewById(R.id.resName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String imageUri= res.getResBannerPhoto();

        Picasso.with(parent.getContext()).load(imageUri).placeholder(R.drawable.loading_image).fit().into(holder.resBannerImage);
        holder.resName.setText(res.getResName());
        return convertView;
    }


    public class ViewHolder {
        TextView resName;
        ImageView resBannerImage;
    }
}
