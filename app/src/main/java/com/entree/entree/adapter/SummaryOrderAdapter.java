package com.entree.entree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.OrderedItems;

import java.util.ArrayList;

/**
 *
 */
public class SummaryOrderAdapter extends BaseAdapter {
    Context mContext;
//    ArrayList<Items> itemList = new ArrayList<Items>();
    ArrayList<OrderedItems> orderItems;
    private LayoutInflater inflator;
    private static final int FOOD_FRAME_CLICK = 0;
    private static final int BTN_ADD_CLICK = 1;
    private static final int BTN_REMOVE_CLICK = 2;
    private static final int BTN_CANCEL_CLICK = 3;
    private static final int BTN_ORDER_VIEW = 4;
    ArrayList<String> itemName = new ArrayList<>();
    ArrayList<Integer> itemQty = new ArrayList<>();
    ArrayList<Integer> itemPrice = new ArrayList<>();
    //    TrayUpdateListener onTrayUpdate = null;
    int Position;
    PlaceholderFragment placeholderFragment = new PlaceholderFragment();

//    public SummaryOrderAdapter(Context mContext, ArrayList<Items> itemList) {
//        this.mContext = mContext;
//        this.itemList = itemList;
//        inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }

    public SummaryOrderAdapter(Context mContext, ArrayList<OrderedItems> orderedItemses) {
        this.mContext = mContext;
        this.orderItems = orderedItemses;
//        this.onTrayUpdate =onTrayUpdate;
        inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orderItems.size();
    }

    @Override
    public OrderedItems getItem(int position) {
        return orderItems.get(position);
    }

//    @Override
//    public Items getItem(int position) {
//        return ob.get(position);
//    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        View v1 = convertView;
        final ViewHolder holder = new ViewHolder();;
//        final Items i = orderItems.get(position);
        if(v == null){

        if (orderItems != null) {
//            if (i.isSection()) {
//                SectionItem si = (SectionItem) i;
                v = inflator.inflate(R.layout.summary_order_adapter, null);
//            RelativeLayout tvItemCategoryname = (RelativeLayout) v.findViewById(R.id.tvItemCategoryname);
                TextView tvItemCategory = (TextView) v.findViewById(R.id.tvItemCategory);
                TextView tvItemTotal = (TextView) v.findViewById(R.id.itemtotal);
//                holder.tvViewOrder = (ImageView) v.findViewById(R.id.view_detail_order);
//            ListView items_details = (ListView)v.findViewById(R.id.items_details);

                tvItemCategory.setText(orderItems.get(position).getResName());
//                holder.tvViewOrder.setTag(position);
//                holder.tvViewOrder.setId(BTN_ORDER_VIEW);
//                holder.tvViewOrder.setOnClickListener(onclick);
                tvItemTotal.setText(mContext.getResources().getString(R.string.Rs) + "  " + orderItems.get(position).getResTotal());
                holder.foodQty = (TextView) v.findViewById(R.id.tvQty);
                holder.foodName = (TextView) v.findViewById(R.id.tvFoodName);
                holder.foodPrice = (TextView) v.findViewById(R.id.tvFoodPrice);
                v.setTag(holder);

//            } else {
//                EntryItem ei = (EntryItem) i;
////                v = inflator.inflate(R.layout.order_summary_food, null);
//                TextView tvQty = (TextView) v.findViewById(R.id.tvQty);
//                TextView tvFoodName = (TextView) v.findViewById(R.id.tvFoodName);
//                TextView tvFoodPrice = (TextView) v.findViewById(R.id.tvFoodPrice);

//                holder.ffFoodContent = (LinearLayout) v.findViewById(R.id.llParentView);
//                holder.llAddFood = (LinearLayout) v.findViewById(R.id.llAddFood);
//                holder.btnRemoveFood = (Button) v.findViewById(R.id.btnRemoveFood);
//                holder.btnAddFood = (Button) v.findViewById(R.id.btnAddFood);
//                holder.btnCancel = (Button) v.findViewById(R.id.btnCancel);
//
//                holder.ffFoodContent.setTag(position);
//                holder.btnAddFood.setTag(position);
//                holder.btnRemoveFood.setTag(position);
//                holder.btnCancel.setTag(position);
//
//                holder.ffFoodContent.setId(FOOD_FRAME_CLICK);
//                holder.btnRemoveFood.setId(BTN_REMOVE_CLICK);
//                holder.btnAddFood.setId(BTN_ADD_CLICK);
//                holder.btnCancel.setId(BTN_CANCEL_CLICK);
//
//                holder.ffFoodContent.setOnClickListener(onclick);
//                holder.btnRemoveFood.setOnClickListener(onclick);
//                holder.btnAddFood.setOnClickListener(onclick);
//                holder.btnCancel.setOnClickListener(onclick);

//                OrderItem orderItem = ei.getOrderItem();



//            }

            itemName = orderItems.get(position).getFoodName();
            itemQty = orderItems.get(position).getFoodQty();
            itemPrice = orderItems.get(position).getFoodPrice();


//            OrderDetailsSummaryAdapter adapter = new OrderDetailsSummaryAdapter(mContext,itemName,itemPrice,itemQty);
//            items_details.setAdapter(adapter);

            for(int i=0;i<itemName.size();i++) {

//                    v = inflator.inflate(R.layout.order_summary_food, tvItemCategoryname );



                holder.foodName.setText(itemName.get(i));
                holder.foodPrice.setText(mContext.getResources().getString(R.string.Rs) + "  " + itemPrice.get(i));
                holder.foodQty.setText("x" + itemQty.get(i));
            }


        }
        }
//        convertView.removeAllViews();

//        else {
//            holder = (ViewHolder)v.getTag();
//        }



//        for(int i=0;i<itemName.size();i++) {
//
////                    v = inflator.inflate(R.layout.order_summary_food, tvItemCategoryname );
//
//            holder.foodName.setText(itemName.get(i));
//            holder.foodPrice.setText(mContext.getResources().getString(R.string.Rs) + "  " + itemPrice.get(i));
//            holder.foodQty.setText("x" + itemQty.get(i));
//        }



        return v;
    }

    private void setPosition(int position){
        this.Position = position;
    }

    private int getPosition(){
        return Position;
    }

//    private View.OnClickListener onclick = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case FOOD_FRAME_CLICK:
////                    setPosition((Integer)v.getTag());
//                    setAddView(v);
//                    break;
//                case BTN_ADD_CLICK:
//                    setPosition((Integer)v.getTag());
//                    addOrderItem(v);
//                    break;
//                case BTN_CANCEL_CLICK:
//                    setPosition((Integer)v.getTag());
//                    removeAddView(v);
//                    break;
//                case BTN_REMOVE_CLICK:
//                    setPosition((Integer)v.getTag());
//                    removeOrderItem(v);
//                    break;
//                case BTN_ORDER_VIEW:
//                    setPosition((Integer)v.getTag());
//                    showViewOrder(v);
//                    break;
//
//                default:
//                    break;
//            }
//        }
//    };
//
//    private void setAddView(View v) {
////        LinearLayout llAddNew = (LinearLayout) v.findViewById(R.id.llAddFood);
////        if (llAddNew.getVisibility() == View.GONE) {
////            llAddNew.setVisibility(View.VISIBLE);
////        }
//////        if (llAddNew.getVisibility() == View.VISIBLE) {
//////            llAddNew.setVisibility(View.GONE);
//////        }
//
////        LinearLayout llParent = (LinearLayout) v.getParent();
//        LinearLayout llAddNew = (LinearLayout) v.findViewById(R.id.llAddFood);
////        LinearLayout llFooter = (LinearLayout) llParent.findViewById(R.id.llOrderFooter);
//
//
//
//        if (llAddNew.getVisibility() == View.GONE) {
//            llAddNew.setVisibility(View.VISIBLE);
////            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.transHomeColor));
//        }
////        else{
////            llAddNew.setVisibility(View.GONE);
//////            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.listViewFooterLayout));
////        }
//
//    }
//
//    private void addOrderItem(View v) {
//        int position = getPosition();
//        Items i = itemList.get(position);
//        if(i!=null && !i.isSection()){
//            EntryItem ei = (EntryItem) i;
//            OrderItem orderItem = ei.getOrderItem();
//            if(orderItem!=null) {
//                if(StoreActivity.arrUserOtderItems.contains(orderItem)) {
//                    int index = StoreActivity.arrUserOtderItems.indexOf(orderItem);
//                    if(index>-1) {
//                        int totalPrice = 0;
//                        int qty = orderItem.getQty();
//                        int price = orderItem.getPrice();
//                        int itemTotal = orderItem.getItemTotal();
//                        if(qty>0) {
//                            qty = qty +1;
//                            itemTotal = qty*price;
//                            StoreActivity.arrUserOtderItems.get(index).setQty(qty);
//                            StoreActivity.arrUserOtderItems.get(index).setItemTotal(itemTotal);
//                            notifyDataSetChanged();
//                            for (OrderItem item : StoreActivity.arrUserOtderItems) {
//                                totalPrice = totalPrice + item.getItemTotal();
//                            }
//                            StoreActivity.updateFooter(totalPrice);
//                        } else {
//                            //   StoreActivity.arrUserOtderItems.remove(orderItem);
//                        }
//                        if(onTrayUpdate!=null)
//                            onTrayUpdate.onTrayUpdate();
//                    }
//                }
//            }
//        }
//    }
//
//    private void showViewOrder(View v){
//        int position = getPosition();
//        ViewHolder holder = new ViewHolder();
//        holder.ffFoodContent1 = (LinearLayout) v.findViewById(R.id.llParentView);
////        LinearLayout llAddNew = (LinearLayout) v.getParent();
//        if (holder.ffFoodContent1.getVisibility() == View.GONE) {
//            holder.ffFoodContent1.setVisibility(View.VISIBLE);
////            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.transHomeColor));
//        }else{
//            holder.ffFoodContent.setVisibility(View.GONE);
//        }
//    }
//
//    private void removeOrderItem(View v){
//        int position = getPosition();
//        Items i = itemList.get(position);
//        if(i!=null && !i.isSection()){
//            EntryItem ei = (EntryItem) i;
//            OrderItem orderItem = ei.getOrderItem();
//            if(orderItem!=null) {
//                if(StoreActivity.arrUserOtderItems.contains(orderItem)) {
//                    int index = StoreActivity.arrUserOtderItems.indexOf(orderItem);
//                    if(index>-1) {
//                        int totalPrice = 0;
//                        int qty = orderItem.getQty();
//                        int price = orderItem.getPrice();
//                        int itemTotal = orderItem.getItemTotal();
//                        if(qty>1) {
//                            qty = qty-1;
//                            itemTotal = qty*price;
//                            StoreActivity.arrUserOtderItems.get(index).setQty(qty);
//                            StoreActivity.arrUserOtderItems.get(index).setItemTotal(itemTotal);
//                            notifyDataSetChanged();
//                            for (OrderItem item : StoreActivity.arrUserOtderItems) {
//                                totalPrice = totalPrice + item.getItemTotal();
//                            }
//                            StoreActivity.updateFooter(totalPrice);
//                        } else {
//                            StoreActivity.arrUserOtderItems.remove(orderItem);
//                            notifyDataSetChanged();
////                            for (OrderItem item : StoreActivity.arrUserOtderItems) {
////                                totalPrice = totalPrice + item.getItemTotal();
////                            }
////                            StoreActivity.updateFooter(itemTotal);
//                            for (OrderItem item : StoreActivity.arrUserOtderItems) {
//                                totalPrice = totalPrice + item.getItemTotal();
//                            }
//                            StoreActivity.updateFooter(itemTotal);
//                            if(StoreActivity.arrUserOtderItems.size() == 0) {
//                                StoreActivity.updateFooter(0);
//                            }
//
//
//                        }
//                        if(onTrayUpdate!=null)
//                            onTrayUpdate.onTrayUpdate();
//                    }
//                }
//            }
//        }
//    }
//
//    private void removeAddView(View v) {
//
//        int position = getPosition();
//        Items i = itemList.get(position);
//        if(i!=null && !i.isSection()){
//            EntryItem ei = (EntryItem) i;
//            OrderItem orderItem = ei.getOrderItem();
//            if(orderItem!=null) {
//                if(StoreActivity.arrUserOtderItems.contains(orderItem)) {
//                    int index = StoreActivity.arrUserOtderItems.indexOf(orderItem);
//                    if(index>-1) {
//                        int totalPrice = 0;
//                        int qty = orderItem.getQty();
//                        int price = orderItem.getPrice();
//                        int itemTotal = orderItem.getItemTotal();
////                        if(qty>1) {
////                            qty = qty -1;
////                            itemTotal = qty*price;
////                            StoreActivity.arrUserOtderItems.get(index).setQty(qty);
////                            StoreActivity.arrUserOtderItems.get(index).setItemTotal(itemTotal);
////                            notifyDataSetChanged();
////                        } else {
//                            StoreActivity.arrUserOtderItems.remove(orderItem);
//                            notifyDataSetChanged();
//                            for (OrderItem item : StoreActivity.arrUserOtderItems) {
//                                totalPrice = totalPrice + item.getItemTotal();
//                            }
//                            StoreActivity.updateFooter(totalPrice);
//
//                            if(StoreActivity.arrUserOtderItems.size() == 0) {
//                                StoreActivity.updateFooter(0);
//                            }
////                        }
//                        if(onTrayUpdate!=null)
//                            onTrayUpdate.onTrayUpdate();
//
//                        LinearLayout llParent = (LinearLayout) v.getParent();
//                        LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
//                        llAddNew.setVisibility(View.GONE);
//                    }
//                }
//            }
//        }
//
//
//    }

    public class ViewHolder {
        TextView foodName;
        TextView foodQty;
        TextView foodPrice;
//        ImageView foodImage;
//        LinearLayout ffFoodContent;
//        LinearLayout ffFoodContent1;
//        ImageView tvViewOrder;
//        LinearLayout llAddFood;
//        Button btnRemoveFood;
//        Button btnAddFood;
//        Button btnCancel;
    }
}
