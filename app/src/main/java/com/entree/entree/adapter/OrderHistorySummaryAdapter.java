package com.entree.entree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.EntryItem;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.interfacelistener.Items;

import java.util.ArrayList;

/**
 * Created by jesudass on 1/24/2016.
 */
public class OrderHistorySummaryAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Items> arrItems = new ArrayList<Items>();
    LayoutInflater inflator;
    public OrderHistorySummaryAdapter(Context mContext,ArrayList<Items> arrItems){
        this.mContext = mContext;
        this.arrItems = arrItems;
        inflator = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return arrItems.size();
    }

    @Override
    public Items getItem(int position) {
        return arrItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final Items i = arrItems.get(position);
        if (i != null) {
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;
                v = inflator.inflate(R.layout.order_summary_restaurant, null);
                TextView tvItemCategory = (TextView) v.findViewById(R.id.tvItemCategory);
                TextView tvItemTotal = (TextView) v.findViewById(R.id.itemtotal);

                tvItemCategory.setText(si.getTitle());
                tvItemTotal.setText(mContext.getResources().getString(R.string.Rs) + "  " + si.getPrice());
            } else {
                EntryItem ei = (EntryItem)i;
                v = inflator.inflate(R.layout.order_summary_food, null);
                TextView tvQty = (TextView) v.findViewById(R.id.tvQty);
                TextView tvFoodName = (TextView) v.findViewById(R.id.tvFoodName);
                TextView tvFoodPrice = (TextView) v.findViewById(R.id.tvFoodPrice);

                OrderItem orderItem = ei.getOrderItem();
                tvFoodName.setText(orderItem.getFoodName());
                tvFoodPrice.setText(mContext.getResources().getString(R.string.Rs) + "  " + orderItem.getItemTotal());
                tvQty.setText("x"+orderItem.getQty());
            }
        }
        return v;
    }
}
