package com.entree.entree.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.activity.StoreActivity;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.Food;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.interfacelistener.FoodChangeListener;
import com.entree.entree.utils.AppConstants;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by jesudass on 1/7/2016.
 */
public class FoodListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Food> arrFood = new ArrayList<Food>();
    private LayoutInflater inflater;
    private FoodChangeListener fcListener;
    private Restaurant resObj;
    Boolean enableGridView = true;
    boolean doSwitch = true;
    private int Position;
    private int orderCost = 0;

    private static final int FOOD_FRAME_CLICK = 0;
    private static final int BTN_ADD_CLICK = 1;
    private static final int BTN_REMOVE_CLICK = 2;
    private static final int BTN_CANCEL_CLICK = 3;
    private ArrayList<Integer> selectedPostion = new ArrayList<>();

    public FoodListAdapter(Context mContext, ArrayList<Food> arrFood, Restaurant resObj) {
        this.mContext = mContext;
        this.arrFood = arrFood;
        this.resObj = resObj;
        this.inflater = LayoutInflater.from(mContext);
    }

    public void setLayoutType(int type) {
        switch (type) {
            case AppConstants.TYPE_GRID_VIEW:
                this.enableGridView = true;
                break;
            case AppConstants.TYPE_LIST_VIEW:
                this.enableGridView = false;
                break;
        }
    }

    @Override
    public int getCount() {
        return arrFood.size();
    }

    @Override
    public Food getItem(int position) {
        return arrFood.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        Food food = arrFood.get(position);

        if (convertView == null) {
            holder = new ViewHolder();


            convertView = inflater.inflate(R.layout.food_list_item, null);


            holder.foodName = (TextView) convertView.findViewById(R.id.foodName);
            holder.foodPrice = (TextView) convertView.findViewById(R.id.foodPrice);
            holder.foodDesc = (TextView) convertView.findViewById(R.id.foodDesc);
            holder.vegOnly = (ImageView) convertView.findViewById(R.id.ivvegIndicator);
            holder.VetnNonVeg = (ImageView) convertView.findViewById(R.id.ivNonVegIndicator);

            holder.llFoodFooter = (LinearLayout) convertView.findViewById(R.id.llOrderFooter);
            holder.ffFoodContent = (FrameLayout) convertView.findViewById(R.id.ffFoodContent);
            holder.llAddFood = (LinearLayout) convertView.findViewById(R.id.llAddFood);
            holder.btnRemoveFood = (Button) convertView.findViewById(R.id.btnRemoveFood);
            holder.btnAddFood = (Button) convertView.findViewById(R.id.btnAddFood);
            holder.btnCancel = (Button) convertView.findViewById(R.id.btnCancel);




            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.btnAddFood.setTag(position);
        holder.btnRemoveFood.setTag(position);
        holder.btnCancel.setTag(position);

        holder.ffFoodContent.setId(FOOD_FRAME_CLICK);
        holder.btnRemoveFood.setId(BTN_REMOVE_CLICK);
        holder.btnAddFood.setId(BTN_ADD_CLICK);
        holder.btnCancel.setId(BTN_CANCEL_CLICK);
        setAddView(holder.ffFoodContent,selectedPostion.contains(position));

        holder.ffFoodContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!selectedPostion.contains(position))
                    selectedPostion.add(new Integer(position));
                else
                    selectedPostion.remove(new Integer(position));
                notifyDataSetChanged();
            }
        });
        holder.btnRemoveFood.setOnClickListener(onclick);
        holder.btnAddFood.setOnClickListener(onclick);
        holder.btnCancel.setOnClickListener(onclick);

        //String imageUri = food.getFoodImage();
        //imageLoader.displayImage(imageUri, holder.foodImage, options);


        String imageUri = food.getFoodImage();

//        if (imageUri != null && !imageUri.equalsIgnoreCase("")) {
//            imageLoader.displayImage(imageUri, holder.foodImage);
//        }
        //imageLoader.DisplayImage(imageUri, R.drawable.placeholderimg, holder.foodImage);

        if (food.getVegOnly().equalsIgnoreCase("Y")) {
            holder.vegOnly.setVisibility(View.VISIBLE);
            holder.VetnNonVeg.setVisibility(View.GONE);
        } else {
            holder.VetnNonVeg.setVisibility(View.VISIBLE);
            holder.vegOnly.setVisibility(View.GONE);
        }

        holder.foodName.setText(food.getFoodName());
        holder.foodDesc.setText(food.getFoodDesc());
        holder.foodPrice.setText(mContext.getResources().getString(R.string.Rs) + " " + String.valueOf(food.getPrice()));

        return convertView;
    }

    public void setOnDataChangeListener(FoodChangeListener onDataChangeListener) {
        fcListener = onDataChangeListener;
        int price = calculateTotalCost();
        if (fcListener != null)
            fcListener.onDataChanged(price);
    }

    private void setPositon(int position){
        this.Position = position;
    }

    private int getPosition(View view){
        Position = (Integer) view.getTag();
        return Position;
    }

    private View.OnClickListener onclick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case FOOD_FRAME_CLICK:
                    setAddView(v,false);
                    break;
                case BTN_ADD_CLICK:
                    setPositon((Integer)v.getTag());
                    addOrderItem(v);
                    EntreeApplication.notifyFoodChange();
                    break;
                case BTN_CANCEL_CLICK:
                    setPositon((Integer)v.getTag());
                    removeAddView(v);
                    break;
                case BTN_REMOVE_CLICK:
                    setPositon((Integer)v.getTag());
                    removeOrderItem(v);
                    EntreeApplication.notifyFoodChange();
                    break;
                default:
                    break;
            }
        }
    };

    private void setAddView(View v,boolean isSelected) {
        LinearLayout llParent = (LinearLayout) v.getParent();
//        LinearLayout llFoodFooter = (LinearLayout) llParent.findViewById(R.id.llOrderFooter);
        LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
        LinearLayout llFooter = (LinearLayout) llParent.findViewById(R.id.llOrderFooter);
        if (isSelected) {
            llAddNew.setVisibility(View.VISIBLE);
            llFooter.setBackgroundColor(mContext.getResources().getColor(R.color.ordergreen));
        }else{
            llAddNew.setVisibility(View.GONE);
            llFooter.setBackgroundResource(R.color.listViewFooterLayout);
        }
    }

    private OrderExists checkIfItemExists(String foodId) {
        for (OrderItem item : EntreeApplication.arrUserOtderItems) {
            if (item.getFoodId() != null && item.getFoodId().equalsIgnoreCase(foodId)) {
                return new OrderExists(EntreeApplication.arrUserOtderItems.indexOf(item), true);
            }
        }
        return null;
    }

    private void addOrderItem(View v) {
        int position = getPosition(v);
        Food fdObj = arrFood.get(position);
        ParseObject fc = fdObj.getFoodCategory();

        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());

        if (oExists != null && oExists.exists) {
            OrderItem oItem = EntreeApplication.arrUserOtderItems.get(oExists.position);
            int price = oItem.getPrice();
            int qty = oItem.getQty();
            int itemTotal = oItem.getItemTotal();
            qty = qty + 1;
            itemTotal = qty * price;
            EntreeApplication.arrUserOtderItems.get(oExists.position).setItemTotal(itemTotal);
            EntreeApplication.arrUserOtderItems.get(oExists.position).setQty(qty);
        } else {
            OrderItem oItem = null;
            try {
                oItem = new OrderItem(null, resObj, fdObj.getFoodName(),
                        fdObj.getFoodDesc(), (String) fc.fetchIfNeeded().get("FoodCategoryName"),
                        fdObj.getVegOnly(), 1, (int) fdObj.getPrice(), (int) fdObj.getPrice(), "", fdObj.getFoodId(), resObj.getRestaurantId());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            EntreeApplication.arrUserOtderItems.add(oItem);
        }
        int price = calculateTotalCost();
        if (fcListener != null) {
            fcListener.onDataChanged(price);
        }
    }

    private void removeOrderItem(View v) {
        int position = getPosition(v);
        Food fdObj = arrFood.get(position);
        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());
        if (oExists != null && oExists.exists) {
            OrderItem oItem = EntreeApplication.arrUserOtderItems.get(oExists.position);
            int price = oItem.getPrice();
            int qty = oItem.getQty();
            int itemTotal = oItem.getItemTotal();
            if (qty > 1) {
                qty = qty - 1;
                itemTotal = qty * price;
                EntreeApplication.arrUserOtderItems.get(oExists.position).setItemTotal(itemTotal);
                EntreeApplication.arrUserOtderItems.get(oExists.position).setQty(qty);
            } else if (qty == 1) {
                EntreeApplication.arrUserOtderItems.remove(oExists.position);
            }
        }


        int price = calculateTotalCost();
        if (fcListener != null) {
            fcListener.onDataChanged(price);
        }
    }

    private void removeAddView(View v) {
        int position = getPosition(v);
        Food fdObj = arrFood.get(position);
        OrderExists oExists = checkIfItemExists(fdObj.getFoodId());

        if (oExists != null && oExists.exists) {
            EntreeApplication.arrUserOtderItems.remove(oExists.position);

            int price = calculateTotalCost();
            if (fcListener != null) {
                fcListener.onDataChanged(price);
            }
            selectedPostion.remove((Integer) Position);
//            holder.foodquantity.setVisibility(View.GONE);
           /* LinearLayout llParent = (LinearLayout) v.getParent();
            LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
            LinearLayout llFooter = (LinearLayout) llAddNew.findViewById(R.id.llOrderFooter);
            llFooter.setBackgroundColor(Color.parseColor("#85ffffff"));
            llAddNew.setVisibility(View.GONE);*/


        }else {

           /* LinearLayout llParent = (LinearLayout) v.getParent();
            LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
            LinearLayout llFooter = (LinearLayout) ((LinearLayout) llAddNew.getParent()).findViewById(R.id.llOrderFooter);
            llFooter.setBackgroundColor(Color.parseColor("#e5fdfdfd"));
            llAddNew.setVisibility(View.GONE);*/
        }
//
//        LinearLayout llParent = (LinearLayout) v.getParent();
//        LinearLayout llAddNew = (LinearLayout) llParent.findViewById(R.id.llAddFood);
//        LinearLayout llFooter = (LinearLayout) ((LinearLayout) llAddNew.getParent()).findViewById(R.id.llOrderFooter);
//        llFooter.setBackgroundColor(Color.parseColor("#e5fdfdfd"));
//        llAddNew.setVisibility(View.GONE);
    }

    private int calculateTotalCost() {
        int price = 0;
        for (OrderItem item : EntreeApplication.arrUserOtderItems) {
            price = price + item.getItemTotal();
        }
        return price;
    }

    public class ViewHolder {
        TextView foodName;
        TextView foodDesc;
        TextView foodPrice;
        ImageView foodImage;
        FrameLayout ffFoodContent;
        LinearLayout llAddFood;
        Button btnRemoveFood;
        Button btnAddFood;
        Button btnCancel;
        LinearLayout llFoodFooter;
        ImageView vegOnly, VetnNonVeg;
    }

    private class OrderExists {
        public int position;
        public Boolean exists;

        public OrderExists(int position, Boolean exists) {
            this.position = position;
            this.exists = exists;
        }
    }
}
