package com.entree.entree.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.activity.StoreActivity;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.Food;
import com.entree.entree.constructors.FoodCategory;
import com.entree.entree.constructors.Restaurant;
import com.entree.entree.interfacelistener.FoodChangeListener;
import com.entree.entree.loader.ImageLoader;
import com.entree.entree.utils.AppConstants;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesudass on 1/8/2016.
 */
public class PlaceholderFragment extends Fragment {
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_FOOD_CATEGORY = "food_category";
    private static final String ARG_SELECTED_RESTAURANT = "selected_restaurant";
    private GridView gridListView;


    private TextView tvTotalPrice;
    private Context mContext;
    public FoodListAdapter foodListAdapter;
    private Restaurant resObj;
    public static int type = AppConstants.TYPE_GRID_VIEW;
    private LinearLayout llTray;
    private ImageButton btnTray;

    public PlaceholderFragment() {
        //this.mContext = getActivity().getApplicationContext();
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public PlaceholderFragment newInstance(int sectionNumber, FoodCategory foodsCategory,
                                           Restaurant resObj,ImageLoader imageLoad) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putSerializable(ARG_SELECTED_RESTAURANT, resObj);
        args.putSerializable(ARG_SECTION_FOOD_CATEGORY, foodsCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_store, container, false);
        gridListView = (GridView) rootView.findViewById(R.id.gridListView);
        resObj = (Restaurant) getArguments().getSerializable(ARG_SELECTED_RESTAURANT);

        new GetFoodAsync((FoodCategory) getArguments().getSerializable(ARG_SECTION_FOOD_CATEGORY), resObj).execute();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity.getApplicationContext();
    }

    public void notifyDataSetChanged() {
            gridListView.setVisibility(View.VISIBLE);
    }

    private class GetFoodAsync extends AsyncTask<Void, Void, Void> {
        private FoodCategory foodCategory;
        private ProgressDialog mProgressDialog;
        private ArrayList<Food> arrFood = new ArrayList<Food>();
        private List<ParseObject> ob;
        private Restaurant resObj;


        public GetFoodAsync(FoodCategory fdCategory, Restaurant resObj) {
            this.foodCategory = fdCategory;
            this.resObj = resObj;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

//            ParseQuery<ParseObject> queryFCat = ParseQuery.getQuery("FoodCategory");
//            queryFCat.whereEqualTo("objectId", foodCategory.getFoodCategoryId());
//
//            ParseObject.createWithoutData("FootCategory", foodCategory.getFoodCategoryId().toString());

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Food");
//            query.whereMatchesQuery("FoodCategory", queryFCat);
//            query.include("FoodCategory");
            query.whereEqualTo("FoodCategory", ParseObject.createWithoutData("FoodCategory", foodCategory.getFoodCategoryId().toString()));
            query.whereEqualTo("ResId", ParseObject.createWithoutData("Restaurant", resObj.getRestaurantId().toString()));

//            if (resObj.getRestaurantId() != null && !resObj.getRestaurantId().equalsIgnoreCase("")) {
//                ParseQuery<ParseObject> queryRes = ParseQuery.getQuery("Restaurant");
//                queryRes.whereEqualTo("objectId", resObj.getRestaurantId());
//                query.whereMatchesQuery("ResId", queryRes);
//            }

            try {
                ob = query.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (ob != null) {
                for (ParseObject foodObj : ob) {

                    ParseFile FoodImageFile = foodObj.getParseFile("FoodImage");
                    ParseObject fdCatObj = foodObj.getParseObject("FoodCategory");

                    String foodId = foodObj.getObjectId();
                    String foodName = foodObj.getString("FoodName");
                    String FoodDesc = foodObj.getString("FoodDesc");
                    String VegOnly = foodObj.getString("VegOnly");
                    String FoodAddon = foodObj.getString("FoodAddon");
                    String FoodImage = "";
                    if (FoodImageFile != null)
                        FoodImage = (String) FoodImageFile.getUrl();

                    int Price = foodObj.getInt("Price");
                    Boolean IsRecommended = foodObj.getBoolean("IsRecommended");
                    Boolean IsActive = foodObj.getBoolean("IsActive");
                    Boolean IsSpecial = foodObj.getBoolean("IsSpecial");
                    Boolean IsGluten = foodObj.getBoolean("IsGluten");
                    Boolean IsNut = foodObj.getBoolean("IsNut");
                    Boolean IsSpicy = foodObj.getBoolean("IsSpicy");

                    Food fBooks = new Food(foodId, resObj, foodName,
                            FoodDesc, fdCatObj, Price,
                            VegOnly, FoodAddon, FoodImage,
                            IsActive, IsRecommended, IsSpecial,
                            IsGluten, IsNut, IsSpicy);

                    arrFood.add(fBooks);
                }
                foodListAdapter = new FoodListAdapter(mContext, arrFood, resObj);
                foodListAdapter.setOnDataChangeListener(new FoodChangeListener() {
                    @Override
                    public void onDataChanged(int price) {
                        EntreeApplication.notifyFoodChange();
                    }
                });

                gridListView.setAdapter(foodListAdapter);
            }
            mProgressDialog.dismiss();
        }
    }

}
