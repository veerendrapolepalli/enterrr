package com.entree.entree.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.OrderItems;
import com.entree.entree.constructors.RestaurantOrders;
import com.entree.entree.constructors.SectionItem;
import com.entree.entree.interfacelistener.Items;

import java.util.ArrayList;

/**
 * Created by jesudass on 1/9/2016.
 */
public class OrderHistoryAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<Items> itemList = new ArrayList<Items>();
    private LayoutInflater inflator;

    public OrderHistoryAdapter(Context mContext, ArrayList<Items> itemList) {
        this.mContext = mContext;
        this.itemList = itemList;
        inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Items getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final Items i = itemList.get(position);
        if (i != null) {
            if (i.isSection()) {
                SectionItem si = (SectionItem) i;
                v = inflator.inflate(R.layout.order_history_restaurant, null);
                TextView tvItemCategory = (TextView) v.findViewById(R.id.tvItemCategory);
                TextView tvItemTotal = (TextView) v.findViewById(R.id.itemtotal);
                View divider = (View) v.findViewById(R.id.divider);

                tvItemTotal.setVisibility(View.INVISIBLE);
                divider.setVisibility(View.INVISIBLE);
                tvItemCategory.setText(si.getTitle());

            } else {
                OrderItems ei = (OrderItems) i;
                v = inflator.inflate(R.layout.order_history_food, null);
                TextView tvRest = (TextView) v.findViewById(R.id.tvRest);
                TextView tvFoodPrice = (TextView) v.findViewById(R.id.tvFoodPrice);
                RestaurantOrders restOrder = ei.getOrderItem();

                String next = "<font size=18px color='#ec723f'>></font>";
                String previous = "<font color='#f2f2f2'>"+mContext.getResources().getString(R.string.Rs) + " " +
                        String.valueOf(restOrder.getTotal())+"</font>";


                tvRest.setText(restOrder.getRestName());
                tvFoodPrice.setText(mContext.getResources().getString(R.string.Rs) + " " +
                        String.valueOf(restOrder.getTotal()));

//                tvFoodName.setText(orderItem.getFoodName());
//                tvFoodPrice.setText(mContext.getResources().getString(R.string.Rs) + "  " + orderItem.getItemTotal());
//                tvQty.setText("x"+orderItem.getQty());
            }
        }
        return v;
    }
}
