package com.entree.entree.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.entree.entree.R;
import com.entree.entree.constructors.CustomerAddresses;

import java.util.ArrayList;

/**
 * Created by jesudass on 1/24/2016.
 */
public class CustomerAddressListAdapter extends BaseAdapter {

    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    private Context mContext;
    LayoutInflater inflater;
    Boolean overrideView = false;

    public CustomerAddressListAdapter(Context context, ArrayList<CustomerAddresses> arrCustAddr) {
        this.mContext = context;
        this.arrCustAddr = arrCustAddr;
        this.inflater = LayoutInflater.from(mContext);
    }

    public CustomerAddressListAdapter(Context context, ArrayList<CustomerAddresses> arrCustAddr, Boolean overrideView) {
        this.mContext = context;
        this.arrCustAddr = arrCustAddr;
        this.inflater = LayoutInflater.from(mContext);
        this.overrideView = overrideView;
    }

    @Override
    public int getCount() {
        return arrCustAddr.size();
    }

    @Override
    public CustomerAddresses getItem(int position) {
        return arrCustAddr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        CustomerAddresses custAddress = getItem(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.address_item, null);
            viewHolder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            viewHolder.llUserAddressParent = (LinearLayout) convertView.findViewById(R.id.llUserAddressParent);
            viewHolder.ivLocationIcon = (ImageView) convertView.findViewById(R.id.ivLocationIcon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvAddress.setText(custAddress.getAddress1());
        if (overrideView) {
            Resources res = mContext.getResources();
            viewHolder.llUserAddressParent.setBackground(null);
            viewHolder.llUserAddressParent.setBackgroundColor(res.getColor(R.color.greenText));
            viewHolder.tvAddress.setTextColor(Color.WHITE);
            viewHolder.ivLocationIcon.setColorFilter(res.getColor(R.color.baseWhite));
            ViewGroup.LayoutParams params = viewHolder.ivLocationIcon.getLayoutParams();
        }
        return convertView;
    }

    public class ViewHolder {
        public TextView tvAddress;
        public LinearLayout llUserAddressParent;
        public ImageView ivLocationIcon;
    }
}
