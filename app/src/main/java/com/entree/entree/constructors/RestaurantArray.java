package com.entree.entree.constructors;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jesudass on 19-Jan-16.
 */
public class RestaurantArray implements Serializable {
    private ArrayList<Restaurant> arrRest = new ArrayList<Restaurant>();

    public RestaurantArray(ArrayList<Restaurant> arrRest) {
        this.arrRest = arrRest;
    }
    public ArrayList<Restaurant> getRestArray(){
        return this.arrRest;
    }
}