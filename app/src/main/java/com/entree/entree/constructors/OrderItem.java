package com.entree.entree.constructors;

import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jesudass on 1/9/2016.
 */
public class OrderItem implements Serializable {
    String orderItemId = "";
    ParseObject orderObj;
    Restaurant resObj;
    String restName = "";
    String foodName = "";
    String foodDesc="";
    String foodCategory="";
    String vegOnly = "Y";
    int qty = 0;
    int price = 0;
    int itemTotal = 0;
    String foodNote = "";
    String foodId = "";
    Date createdDate ;
    Date modifiedDate ;
    String resID;
    ParseObject restObj;

    public void setRestObj(ParseObject restObj) {
        this.restObj = restObj;
    }

    public OrderItem(String orderItemId,ParseObject orderObj,ParseObject resObj,
                     String foodName,String foodDesc,String foodCategory,String vegOnly,
                     int qty,int price,int itemTotal,String foodNote,String resName){
        this.orderItemId = orderItemId;
        this.orderObj = orderObj;
        this.restObj = resObj;
        this.foodName = foodName;
        this.foodDesc = foodDesc;
        this.foodCategory = foodCategory;
        this.vegOnly = vegOnly;
        this.qty = qty;
        this.price = price;
        this.itemTotal = itemTotal;
        this.foodNote = foodNote;
        this.restName = resName;
    }

    public OrderItem(ParseObject orderObj,Restaurant resObj,
                     String foodName,String foodDesc,String foodCategory,String vegOnly,
                     int qty,int price,int itemTotal,String foodNote,String foodId,String resID){
        this.orderObj = orderObj;
        this.resObj = resObj;
        this.foodName = foodName;
        this.foodDesc = foodDesc;
        this.foodCategory = foodCategory;
        this.vegOnly = vegOnly;
        this.qty = qty;
        this.price = price;
        this.itemTotal = itemTotal;
        this.foodNote = foodNote;
        this.foodId = foodId;
        this.resID =resID;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public ParseObject getOrderObj() {
        return orderObj;
    }

    public void setOrderObj(ParseObject orderObj) {
        this.orderObj = orderObj;
    }

    public Restaurant getResObj() {
        return resObj;
    }

    public void setResObj(Restaurant resObj) {
        this.resObj = resObj;
    }

    public String getRestName() {
        return restName;
    }

    public void setRestName(String restName) {
        this.restName = restName;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getFoodDesc() {
        return foodDesc;
    }

    public void setFoodDesc(String foodDesc) {
        this.foodDesc = foodDesc;
    }

    public String getFoodCategory() {
        return foodCategory;
    }

    public void setFoodCategory(String foodCategory) {
        this.foodCategory = foodCategory;
    }

    public String getVegOnly() {
        return vegOnly;
    }

    public void setVegOnly(String vegOnly) {
        this.vegOnly = vegOnly;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(int itemTotal) {
        this.itemTotal = itemTotal;
    }

    public String getFoodNote() {
        return foodNote;
    }

    public void setFoodNote(String foodNote) {
        this.foodNote = foodNote;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getResID() {
        return resID;
    }

    public void setResID(String resID) {
        this.resID = resID;
    }

    public void createOrderItem() {
        ParseObject restObject = null;
        if(restObj !=null) {
            restObject = restObj;
        } else {
            restObject = this.getResObj().getRestaurantObj();
        }


        ParseObject orderItem = new ParseObject("OrderItems");
        orderItem.put("OrderId", this.getOrderObj());
        orderItem.put("ResId", restObject);
        orderItem.put("ResName", restName);
        orderItem.put("FoodName", this.getFoodName());
        orderItem.put("FoodDesc", this.getFoodDesc());
        orderItem.put("FoodCategory", this.getFoodCategory());
        orderItem.put("VegOnly", this.getVegOnly());

        orderItem.put("Qty", this.getQty());
        orderItem.put("Price", this.getPrice());
        orderItem.put("ItemTotal", this.getItemTotal());
        orderItem.put("FoodNote", this.getFoodNote());

        orderItem.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e!=null) {
                    Log.e("ORDER SAVE ERROR", e.getLocalizedMessage());
                } else {

                }
            }
        });

    }


}

