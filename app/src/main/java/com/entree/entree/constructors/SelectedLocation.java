package com.entree.entree.constructors;

/**
 * Created by jesudass on 13-Feb-16.
 */
public class SelectedLocation {
    public String placeId;
    public String placeName;

    public SelectedLocation(String placeId, String placeName) {
        this.placeId = placeId;
        this.placeName = placeName;
    }
}
