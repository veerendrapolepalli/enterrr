package com.entree.entree.constructors;

import com.entree.entree.interfacelistener.Items;

/**
 * Created by jesudass on 1/9/2016.
 */
public class OrderItems implements Items {

    private RestaurantOrders resOrder ;


    public OrderItems(RestaurantOrders resOrder) {
        this.resOrder = resOrder;
    }
    public RestaurantOrders getOrderItem(){
        return this.resOrder;
    }

    @Override
    public boolean isSection() {
        return false;
    }

}