package com.entree.entree.constructors;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by jesudass on 1/7/2016.
 */
public class Food implements Serializable {
    private String FoodId = "";
    private Restaurant rest = null;
    private String FoodName = "";
    private String FoodDesc = "";
    private ParseObject fdCat = null;
    private int Price ;
    private String VegOnly="N";
    private String FoodAddon="";
    private String FoodImage = "";
    private boolean IsActive = true;
    private boolean IsRecommended = false;
    private boolean IsSpecial = false;
    private boolean IsGluten = false;
    private boolean IsNut = false;
    private boolean IsSpicy = false;

    public Food(String FoodId,Restaurant restObj,String FoodName,
                String FoodDesc,ParseObject fdCat,int Price,
                String VegOnly,String FoodAddon,String FoodImage,
                boolean IsActive,boolean IsRecommended,boolean IsSpecial,
                boolean IsGluten,boolean IsNut,boolean IsSpicy) {
        this.FoodId = FoodId;
        this.rest = restObj;
        this.FoodName = FoodName;
        this.FoodDesc = FoodDesc;
        this.fdCat = fdCat;
        this.Price = Price;
        this.VegOnly = VegOnly;
        this.FoodAddon = FoodAddon;
        this.FoodImage = FoodImage;
        this.IsActive = IsActive;
        this.IsRecommended = IsRecommended;
        this.IsSpecial = IsSpecial;
        this.IsGluten = IsGluten;
        this.IsNut = IsNut;
        this.IsSpicy = IsSpicy;
    }

    public String getFoodId(){
        return FoodId;
    }
    public Restaurant getRestaurant(){
        return rest;
    }
    public ParseObject getFoodCategory(){
        return fdCat;
    }
    public float getPrice(){
        return Price;
    }
    public String getFoodName(){
        return FoodName;
    }
    public String getFoodAddon(){
        return FoodAddon;
    }
    public String getFoodDesc(){
        return FoodDesc;
    }
    public String getVegOnly(){
        return VegOnly;
    }
    public String getFoodImage(){
        return FoodImage;
    }
    public boolean IsSpicy(){
        return IsSpicy;
    }
    public boolean IsActive(){
        return IsActive;
    }
    public boolean IsRecommended(){
        return IsRecommended;
    }
    public boolean IsSpecial(){
        return IsSpecial;
    }
    public boolean IsGluten(){
        return IsGluten;
    }
    public boolean IsNut(){
        return IsNut;
    }

}
