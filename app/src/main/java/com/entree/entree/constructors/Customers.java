package com.entree.entree.constructors;

import android.location.Address;
import android.os.Handler;
import android.os.Message;

import com.entree.entree.utils.AppConstants;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jesudass on 1/12/2016.
 */
public class Customers implements Serializable {
    private String customerId ;
    private ParseObject user;
    private String fName ;
    private String lName ;
    private String mobNumber ;
    private String altMobNumber ;
    private String emailId ;

    //set constructor
    public Customers(String customerId,ParseObject user,
                     String fName,String lName,String mobNumber,
                     String altMobNumber,String emailId){
        this.customerId = customerId;
        this.user = user;
        this.fName = fName;
        this.lName = lName;
        this.mobNumber=mobNumber;
        this.altMobNumber = altMobNumber;
        this.emailId = emailId;
    }
    //create constructor
    public Customers(ParseObject user,
                     String fName,String lName,String mobNumber,
                     String altMobNumber,String emailId){
        this.user = user;
        this.fName = fName;
        this.lName = lName;
        this.mobNumber=mobNumber;
        this.altMobNumber = altMobNumber;
        this.emailId = emailId;
    }

    public void setCustomerId(String customerId){
        this.customerId = customerId;
    }
    public void setUser(ParseObject user){
        this.user=user;
    }
    public void setFName(String fName){
        this.fName = fName;
    }
    public void setlName(String lName){
        this.lName=lName;
    }
    public void setMobNumber(String mobNumber){
        this.mobNumber=mobNumber;
    }
    public void setAltMobNumber(String altMobNumber){
        this.altMobNumber=altMobNumber;
    }
    public void setEmailId(String emailId){
        this.emailId=emailId;
    }

    public String getCustomerId(){
        return this.customerId;
    }
    public ParseObject getUser(){
        return this.user;
    }
    public String getfName(){
        return this.fName;
    }
    public String getlName(){
        return this.lName;
    }
    public String getMobNumber(){
        return this.mobNumber;
    }
    public String getAltMobNumber(){
        return this.altMobNumber;
    }
    public String getEmailId(){
        return this.emailId;
    }

    public void createCustomers(final ParseUser currentUser, final Handler handler,final UserLocation userLocation){
        final ParseObject customersObj = new ParseObject("Customers");
        customersObj.put("UserId",currentUser);
        customersObj.put("FirstName",this.fName);
        customersObj.put("LastName",this.lName);
        customersObj.put("MobileNo",this.mobNumber);
        customersObj.put("AlternateContactNo",this.altMobNumber);
        customersObj.put("Email",this.emailId);
        customersObj.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Message message = Message.obtain();

                if(e!=null) {
                    message.setTarget(handler);
                    message.sendToTarget();
                } else {
                    Customers customer = new Customers(
                            customersObj.getObjectId(),
                            currentUser,customersObj.get("FirstName").toString(),customersObj.get("LastName").toString(),
                            customersObj.get("MobileNo").toString(),customersObj.get("AlternateContactNo").toString(),
                            customersObj.get("Email").toString());

                    createCustomerAddress(customersObj,customer,userLocation,handler);
                }

            }
        });
    }

    private void createCustomerAddress(ParseObject customersObj,Customers customer,
                                       final UserLocation userLocation,final Handler handler){

        Address address = userLocation.getUserAddress();
        String loc = "";
        //for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
        loc = address.getAddressLine(0);
        //}

        CustomerAddresses custAddress = new CustomerAddresses(customersObj,customer.getfName(),
                customer.getlName(),loc,loc,"",
                address.getFeatureName(),address.getLocality(),address.getAdminArea(),
                address.getPostalCode(),String.valueOf(AppConstants.USER_LATTITUDE),
                String.valueOf(AppConstants.USER_LONGITUDE));
        custAddress.saveUserLocation(customersObj,handler);
    }

    public void sendCustomerToParse(Handler handler,UserLocation userLocation){
        List<ParseObject> ob= null;
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            currentUser.getObjectId();

        }
        ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                "Customers");
        queryCustomers.whereEqualTo("UserId", currentUser);
        try {
            ob = queryCustomers.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null && ob.size()>0) {
            ParseObject customersObj = (ParseObject) ob.get(0);
            String fName = "";
            String lName="";
            String mobNo = "";
            String altContactNo = "";
            String email ="";
            if(customersObj.get("FirstName") !=null) {
                fName = customersObj.get("FirstName").toString();
            }
            if(customersObj.get("LastName") !=null) {
                lName = customersObj.get("LastName").toString();
            }
            if(customersObj.get("MobileNo") !=null) {
                mobNo = customersObj.get("MobileNo").toString();
            }
            if(customersObj.get("AlternateContactNo") !=null) {
                altContactNo = customersObj.get("AlternateContactNo").toString();
            }
            if(customersObj.get("Email") !=null) {
                email = customersObj.get("Email").toString();
            }
            Customers customer = new Customers(
                    customersObj.getObjectId(),
                    currentUser,fName,lName,
                    mobNo,altContactNo,
                    email);

            createCustomerAddress(customersObj,customer,userLocation,handler);

        } else {
            createCustomers(currentUser,handler,userLocation);
        }

    }
}


