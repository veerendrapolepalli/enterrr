package com.entree.entree.constructors;

/**
 * Created by vamshi on 28-03-2016.
 */
public class OrderLocation {

    String UserLocationLat = " ";
    String UserLocationLong = " ";
    String DriverLocationLat = " ";
    String DriverLocationLong = " ";
    String RestLocationLat = " ";
    String RestLocationLong = " ";

    public OrderLocation() {
    }

    public String getUserLocationLat() {
        return UserLocationLat;
    }

    public void setUserLocationLat(String userLocationLat) {
        UserLocationLat = userLocationLat;
    }

    public String getUserLocationLong() {
        return UserLocationLong;
    }

    public void setUserLocationLong(String userLocationLong) {
        UserLocationLong = userLocationLong;
    }

    public String getDriverLocationLat() {
        return DriverLocationLat;
    }

    public void setDriverLocationLat(String driverLocationLat) {
        DriverLocationLat = driverLocationLat;
    }

    public String getDriverLocationLong() {
        return DriverLocationLong;
    }

    public void setDriverLocationLong(String driverLocationLong) {
        DriverLocationLong = driverLocationLong;
    }

    public String getRestLocationLat() {
        return RestLocationLat;
    }

    public void setRestLocationLat(String restLocationLat) {
        RestLocationLat = restLocationLat;
    }

    public String getRestLocationLong() {
        return RestLocationLong;
    }

    public void setRestLocationLong(String restLocationLong) {
        RestLocationLong = restLocationLong;
    }
}
