package com.entree.entree.constructors;

import android.os.AsyncTask;
import android.util.Log;

import com.entree.entree.activity.PaymentActivity;
import com.entree.entree.activity.StoreActivity;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.interfacelistener.OrderCompleteListener;
import com.entree.entree.utils.AppConstants;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jesudass on 1/9/2016.
 */
public class Order implements Serializable {
    String OrderId;
    String OrderNo;
    ParseObject Customer;
    ParseObject employee;
    String deviceType;
    double SubTotalAmount;
    double TaxAmount;
    double DriverTipAmount;
    double TotalAmount;
    String PaymentMode;
    String OrderStatusLatest = "New Order";
    String RestaurantStatusLatest = "New Order";
    String DriverStatusLatest = "New Order";
    String DeliveryFirstName;
    String DeliveryLastName;
    String DeliveryAddress1;
    String DeliveryAddress2;
    String DeliveryLandmark;
    String DeliveryCity = "Bangalore";
    String DeliveryState = "Karnataka";
    String DeliveryPincode;
    String DeliveryLatitude;
    String DeliveryLongitude;
    Date CreatedDate;
    String ModifiedDate;
    Boolean isOrderClosed = false;
    Date DeliveryDate = new Date();
    String DeliveryTime = "ASAP";
    Boolean reOrder = false;
    OrderCompleteListener orderCompleteListener;

    public Order() {
    }

    public Order(String orderNo, ParseObject customer, ParseObject employee,
                 String deviceType, double subTotalAmount, double taxAmount,
                 double driverTipAmount, double totalAmount, String paymentMode,
                 String orderStatusLatest, String restaurantStatusLatest, String driverStatusLatest,
                 String deliveryFirstName, String deliveryLastName, String deliveryAddress1,
                 String deliveryAddress2, String deliveryLandmark, String deliveryCity,
                 String deliveryState, String deliveryPincode, String deliveryLatitude,
                 String deliveryLongitude,
                 Boolean isOrderClosed, Date deliveryDate, String deliveryTime) {

        OrderNo = orderNo;
        Customer = customer;
        this.employee = employee;
        this.deviceType = deviceType;
        SubTotalAmount = subTotalAmount;
        TaxAmount = taxAmount;
        DriverTipAmount = driverTipAmount;
        TotalAmount = totalAmount;
        PaymentMode = paymentMode;
        OrderStatusLatest = orderStatusLatest;
        RestaurantStatusLatest = restaurantStatusLatest;
        DriverStatusLatest = driverStatusLatest;
        DeliveryFirstName = deliveryFirstName;
        DeliveryLastName = deliveryLastName;
        DeliveryAddress1 = deliveryAddress1;
        DeliveryAddress2 = deliveryAddress2;
        DeliveryLandmark = deliveryLandmark;
        DeliveryCity = deliveryCity;
        DeliveryState = deliveryState;
        DeliveryPincode = deliveryPincode;
        DeliveryLatitude = deliveryLatitude;
        DeliveryLongitude = deliveryLongitude;
        this.isOrderClosed = isOrderClosed;
        DeliveryDate = deliveryDate;
        DeliveryTime = deliveryTime;
    }


    public Order(String orderId, String orderNo, ParseObject customer, ParseObject employee,
                 String deviceType, double subTotalAmount, double taxAmount, double driverTipAmount,
                 double totalAmount, String paymentMode, String orderStatusLatest,
                 String deliveryFirstName, String deliveryLastName,
                 String deliveryAddress1, String deliveryAddress2, String deliveryLandmark,
                 String deliveryCity, String deliveryState,
                 String deliveryPincode, String deliveryLatitude, String deliveryLongitude,
                 Date createdDate, Boolean isOrderClosed,
                 Date deliveryDate, String deliveryTime) {
        OrderId = orderId;
        OrderNo = orderNo;
        Customer = customer;
        this.employee = employee;
        this.deviceType = deviceType;
        SubTotalAmount = subTotalAmount;
        TaxAmount = taxAmount;
        DriverTipAmount = driverTipAmount;
        TotalAmount = totalAmount;
        PaymentMode = paymentMode;
        OrderStatusLatest = orderStatusLatest;
        DeliveryFirstName = deliveryFirstName;
        DeliveryLastName = deliveryLastName;
        DeliveryAddress1 = deliveryAddress1;
        DeliveryAddress2 = deliveryAddress2;
        DeliveryLandmark = deliveryLandmark;
        DeliveryCity = deliveryCity;
        DeliveryState = deliveryState;
        DeliveryPincode = deliveryPincode;
        DeliveryLatitude = deliveryLatitude;
        DeliveryLongitude = deliveryLongitude;
        CreatedDate = createdDate;
        this.isOrderClosed = isOrderClosed;
        DeliveryDate = deliveryDate;
        DeliveryTime = deliveryTime;
    }

    public void setReorder(Boolean reOrder) {
        this.reOrder = reOrder;
    }


    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getOrderNo() {
        return OrderNo;
    }

    public void setOrderNo(String orderNo) {
        OrderNo = orderNo;
    }

    public ParseObject getCustomer() {
        return Customer;
    }

    public void setCustomer(ParseObject customer) {
        Customer = customer;
    }

    public ParseObject getEmployee() {
        return employee;
    }

    public void setEmployee(ParseObject employee) {
        this.employee = employee;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public double getSubTotalAmount() {
        return SubTotalAmount;
    }

    public void setSubTotalAmount(float subTotalAmount) {
        SubTotalAmount = subTotalAmount;
    }

    public double getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(float taxAmount) {
        TaxAmount = taxAmount;
    }

    public double getDriverTipAmount() {
        return DriverTipAmount;
    }

    public void setDriverTipAmount(float driverTipAmount) {
        DriverTipAmount = driverTipAmount;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public String getOrderStatusLatest() {
        return OrderStatusLatest;
    }

    public void setOrderStatusLatest(String orderStatusLatest) {
        OrderStatusLatest = orderStatusLatest;
    }

    public String getRestaurantStatusLatest() {
        return RestaurantStatusLatest;
    }

    public void setRestaurantStatusLatest(String restaurantStatusLatest) {
        RestaurantStatusLatest = restaurantStatusLatest;
    }

    public String getDriverStatusLatest() {
        return DriverStatusLatest;
    }

    public void setDriverStatusLatest(String driverStatusLatest) {
        DriverStatusLatest = driverStatusLatest;
    }

    public String getDeliveryFirstName() {
        return DeliveryFirstName;
    }

    public void setDeliveryFirstName(String deliveryFirstName) {
        DeliveryFirstName = deliveryFirstName;
    }

    public String getDeliveryLastName() {
        return DeliveryLastName;
    }

    public void setDeliveryLastName(String deliveryLastName) {
        DeliveryLastName = deliveryLastName;
    }

    public String getDeliveryAddress1() {
        return DeliveryAddress1;
    }

    public void setDeliveryAddress1(String deliveryAddress1) {
        DeliveryAddress1 = deliveryAddress1;
    }

    public String getDeliveryAddress2() {
        return DeliveryAddress2;
    }

    public void setDeliveryAddress2(String deliveryAddress2) {
        DeliveryAddress2 = deliveryAddress2;
    }

    public String getDeliveryLandmark() {
        return DeliveryLandmark;
    }

    public void setDeliveryLandmark(String deliveryLandmark) {
        DeliveryLandmark = deliveryLandmark;
    }

    public String getDeliveryCity() {
        return DeliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        DeliveryCity = deliveryCity;
    }

    public String getDeliveryState() {
        return DeliveryState;
    }

    public void setDeliveryState(String deliveryState) {
        DeliveryState = deliveryState;
    }

    public String getDeliveryPincode() {
        return DeliveryPincode;
    }

    public void setDeliveryPincode(String deliveryPincode) {
        DeliveryPincode = deliveryPincode;
    }

    public String getDeliveryLatitude() {
        return DeliveryLatitude;
    }

    public void setDeliveryLatitude(String deliveryLatitude) {
        DeliveryLatitude = deliveryLatitude;
    }

    public String getDeliveryLongitude() {
        return DeliveryLongitude;
    }

    public void setDeliveryLongitude(String deliveryLongitude) {
        DeliveryLongitude = deliveryLongitude;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public Boolean getIsOrderClosed() {
        return isOrderClosed;
    }

    public void setIsOrderClosed(Boolean isOrderClosed) {
        this.isOrderClosed = isOrderClosed;
    }

    public Date getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public void setOrderCompleteListener(OrderCompleteListener orderCompleteListener) {
        this.orderCompleteListener = orderCompleteListener;
    }

    public void createOrder() {
        final ParseObject order = new ParseObject("Orders");
        order.put("OrderNo", this.getOrderNo());
        order.put("CustomerId", this.getCustomer());
        order.put("EmployeeId", this.getEmployee());
        order.put("DeviceType", this.getDeviceType());
        order.put("SubTotalAmount", this.getSubTotalAmount());
        order.put("TaxAmount", this.getTaxAmount());
        order.put("DriverTipAmount", this.getDriverTipAmount());

        order.put("TotalAmount", this.getTotalAmount());
        order.put("PaymentMode", this.getPaymentMode());

        order.put("OrderStatusLatest", this.getOrderStatusLatest());
        order.put("RestaurantStatusLatest", this.getRestaurantStatusLatest());
        order.put("DriverStatusLatest", this.getDriverStatusLatest());
        order.put("DeliveryFirstName", this.getDeliveryFirstName());
        order.put("DeliveryLastName", this.getDeliveryLastName());

        order.put("DeliveryAddress1", this.getDeliveryAddress1());
        order.put("DeliveryAddress2", this.getDeliveryAddress2());
        order.put("DeliveryLandmark", this.getDeliveryLandmark());
        order.put("DeliveryCity", this.getDeliveryCity());
        order.put("DeliveryState", this.getDeliveryState());
        order.put("DeliveryPincode", this.getDeliveryPincode());
        order.put("DeliveryLatitude", this.getDeliveryLatitude());

        order.put("DeliveryLongitude", this.getDeliveryLongitude());
        order.put("IsOrderClosed", this.getIsOrderClosed());
        order.put("DeliveryDate", this.getDeliveryDate());
        order.put("DeliveryTime", this.getDeliveryTime());

        order.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    Log.e("ORDER SAVE ERROR", e.getLocalizedMessage());
                } else {
                    if (!reOrder) {

                        setOrderId(order.getObjectId());
                        PaymentActivity.OrderId = order.getObjectId();

                        ArrayList<String> arrRestId = new ArrayList<String>();
                        HashMap<String, Integer> arrPrice = new HashMap<String, Integer>();
                        ArrayList<Restaurant> arrRestObj = new ArrayList<Restaurant>();

                        for (OrderItem orderItem : EntreeApplication.arrUserOtderItems) {
                            orderItem.setOrderObj(order);
                            orderItem.createOrderItem();
                            if (!arrRestId.contains(orderItem.getResID())) {
                                arrRestId.add(orderItem.getResID());
                                arrRestObj.add(orderItem.getResObj());
                                arrPrice.put(orderItem.getResID(), orderItem.getItemTotal());
                            }
                        }

                        for (Restaurant rest : arrRestObj) {
                            int price = 0;
                            if (arrPrice.containsKey(rest.getRestaurantId())) {
                                price = arrPrice.get(rest.getRestaurantId());
                            }
                            OrderMapping oMapping = new OrderMapping();
                            oMapping.setOrder(order);
                            oMapping.setCustomer(getCustomer());
                            oMapping.setEmployee(getEmployee());
                            oMapping.setDriver(getEmployee());
                            oMapping.setRestObj(rest);
                            oMapping.setRestTotal(price);
                            oMapping.syncMapping();
                        }


//                        ParseObject oOrder = new ParseObject("Orders");
//                        oOrder.put("OrderId",getOrderId());
//
//                        ParseObject oRestaurant = new ParseObject("Restaurant");
//                        oRestaurant.put("ResId","EntreeReserved");
//
//                        ParseObject oEmployee1 = new ParseObject("Employees");
//                        oEmployee1.put("EmployeeId","EntreeReserved");
//
//                        ParseObject oEmployee2 = new ParseObject("Employees");
//                        oEmployee2.put("DriverId", "EntreeReserved");

//                        String currentUser = ParseUser.getCurrentUser().getObjectId();

//                        ParseQuery oUserDetails = ParseQuery.getQuery("Customers");
//                        oUserDetails.whereEqualTo("UserId", oCustomers);

                        Timeline oTimeline = new Timeline();
                        oTimeline.setDriver(ParseObject.createWithoutData("Employees", "EntreeReserved"));
                        oTimeline.setEmployee(ParseObject.createWithoutData("Employees", "EntreeReserved"));
                        oTimeline.setOrder(ParseObject.createWithoutData("Orders", getOrderId()));
                        oTimeline.setRestaurant(ParseObject.createWithoutData("Restaurant", "EntreeReserved"));
                        oTimeline.setStatus("Driver Assigned");
                        oTimeline.setTimelineType("customer");
                        oTimeline.setTimelineNote("Order Placed by " + getDeliveryFirstName());
                        oTimeline.syncTimeLine();
                        new SendOrderNumber().execute();

                        if (orderCompleteListener != null) {
                            orderCompleteListener.onOrderSynced();
                        }
                    } else {

                    }

                }
            }
        });
    }

    private class SendOrderNumber extends AsyncTask<Void,Void,Void> {

//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            utils.showLoader();
//        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                HttpPost httppost = new HttpPost(AppConstants.SEND_ORDER_NUMBER+getOrderNo());
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;

//                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//                nameValuePairs.add(new BasicNameValuePair("order_number",getOrderNo()));
//                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                response = client.execute(httppost);
//                HttpEntity entity = response.getEntity();
//                InputStream stream = entity.getContent();

            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }

            return null;
        }
    }


    public void reDoOrder() {
        final ParseObject order = new ParseObject("Orders");
    }
}
