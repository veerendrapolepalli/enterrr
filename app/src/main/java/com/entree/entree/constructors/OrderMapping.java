package com.entree.entree.constructors;

import android.util.Log;

import com.entree.entree.activity.PaymentActivity;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.Serializable;

/**
 * Created by jesudass on 1/23/2016.
 */
public class OrderMapping implements Serializable {
    ParseObject order;
    ParseObject customer;
    ParseObject restaurant;
    ParseObject employee;
    ParseObject driver;

    String orderId;
    String customerId;
    String restId;
    String employeeId;
    String driverId;
    Boolean isLeadDriver = false;
    String meetingLocationLat = "";
    String meetingLocationLon = "";
    String meetingLocation = "";
    String meetingTime = "";
    String restStatus = "New Order";
    String driverStatus = "New Order";
    String orderStatusLatest = "";
    float foodPreparationTime =0 ;
    String driverStatusLatest = "";
    String restStatusLatest = "";
    Restaurant restObj;
    int restTotal = 0;


    public OrderMapping(ParseObject order, ParseObject customer, ParseObject restaurant,
                        ParseObject employee, ParseObject driver, Boolean isLeadDriver,
                        String meetingLocationLat, String meetingLocationLon,
                        String meetingLocation, String meetingTime, String restStatus,
                        String driverStatus, String driverStatusLatest, String restStatusLatest) {
        this.order = order;
        this.customer = customer;
        this.restaurant = restaurant;
        this.employee = employee;
        this.driver = driver;
        this.isLeadDriver = isLeadDriver;
        this.meetingLocationLat = meetingLocationLat;
        this.meetingLocationLon = meetingLocationLon;
        this.meetingLocation = meetingLocation;
        this.meetingTime = meetingTime;
        this.restStatus = restStatus;
        this.driverStatus = driverStatus;
        this.driverStatusLatest = driverStatusLatest;
        this.restStatusLatest = restStatusLatest;
    }


    public OrderMapping(String orderId, String customerId, String restId,
                        String employeeId, String driverId, Boolean isLeadDriver,
                        String meetingLocationLat, String meetingLocationLon,
                        String meetingLocation, String meetingTime, String restStatus,
                        String driverStatus, String orderStatusLatest, float foodPreparationTime,
                        String driverStatusLatest, String restStatusLatest) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.restId = restId;
        this.employeeId = employeeId;
        this.driverId = driverId;
        this.isLeadDriver = isLeadDriver;
        this.meetingLocationLat = meetingLocationLat;
        this.meetingLocationLon = meetingLocationLon;
        this.meetingLocation = meetingLocation;
        this.meetingTime = meetingTime;
        this.restStatus = restStatus;
        this.driverStatus = driverStatus;
        this.orderStatusLatest = orderStatusLatest;
        this.foodPreparationTime = foodPreparationTime;
        this.driverStatusLatest = driverStatusLatest;
        this.restStatusLatest = restStatusLatest;
    }

    public OrderMapping(){}

    public String getOrderStatusLatest() {
        return orderStatusLatest;
    }

    public void setOrderStatusLatest(String orderStatusLatest) {
        this.orderStatusLatest = orderStatusLatest;
    }

    public float getFoodPreparationTime() {
        return foodPreparationTime;
    }

    public void setFoodPreparationTime(float foodPreparationTime) {
        this.foodPreparationTime = foodPreparationTime;
    }

    public ParseObject getCustomer() {
        return customer;
    }

    public void setCustomer(ParseObject customer) {
        this.customer = customer;
    }

    public ParseObject getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(ParseObject restaurant) {
        this.restaurant = restaurant;
    }

    public ParseObject getEmployee() {
        return employee;
    }

    public void setEmployee(ParseObject employee) {
        this.employee = employee;
    }

    public ParseObject getDriver() {
        return driver;
    }

    public void setDriver(ParseObject driver) {
        this.driver = driver;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRestId() {
        return restId;
    }

    public void setRestId(String restId) {
        this.restId = restId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Boolean getIsLeadDriver() {
        return isLeadDriver;
    }

    public void setIsLeadDriver(Boolean isLeadDriver) {
        this.isLeadDriver = isLeadDriver;
    }

    public String getMeetingLocationLat() {
        return meetingLocationLat;
    }

    public void setMeetingLocationLat(String meetingLocationLat) {
        this.meetingLocationLat = meetingLocationLat;
    }

    public String getMeetingLocationLon() {
        return meetingLocationLon;
    }

    public void setMeetingLocationLon(String meetingLocationLon) {
        this.meetingLocationLon = meetingLocationLon;
    }

    public String getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getRestStatus() {
        return restStatus;
    }

    public void setRestStatus(String restStatus) {
        this.restStatus = restStatus;
    }

    public String getDriverStatus() {
        return driverStatus;
    }

    public void setDriverStatus(String driverStatus) {
        this.driverStatus = driverStatus;
    }

    public String getDriverStatusLatest() {
        return driverStatusLatest;
    }

    public void setDriverStatusLatest(String driverStatusLatest) {
        this.driverStatusLatest = driverStatusLatest;
    }

    public String getRestStatusLatest() {
        return restStatusLatest;
    }

    public void setRestStatusLatest(String restStatusLatest) {
        this.restStatusLatest = restStatusLatest;
    }

    public ParseObject getOrder() {
        return order;
    }

    public void setOrder(ParseObject order) {
        this.order = order;
    }

    public void setRestObj(Restaurant rest) {
        this.restObj = rest;
    }

    public Restaurant getRestObj(){
        return restObj;
    }

    public int getRestTotal() {
        return restTotal;
    }

    public void setRestTotal(int restTotal) {
        this.restTotal = restTotal;
    }

    public void syncMapping(){
        restaurant = this.getRestObj().getRestaurantObj();
        createOrderMapping();
    }

    public void createOrderMapping() {
        final ParseObject orderMapping = new ParseObject("OrderMapping");

        orderMapping.put("OrderId", this.getOrder());
        orderMapping.put("CustomerId", this.getCustomer());
        orderMapping.put("ResId", this.getRestaurant());
        orderMapping.put("EmployeeId", this.getEmployee());
        orderMapping.put("DriverId", this.getDriver());
        orderMapping.put("IsLeadDriver", this.getIsLeadDriver());
        orderMapping.put("MeetingLocationLatitude", this.getMeetingLocationLat());

        orderMapping.put("MeetingLocationLongitude", this.getMeetingLocationLon());
        orderMapping.put("MeetingLocation", this.getMeetingLocation());
        orderMapping.put("MeetingTime", this.getMeetingTime());
        orderMapping.put("RestaurantStatus", this.getRestStatus());
        orderMapping.put("ResTotalAmount",this.getRestTotal());

        orderMapping.put("DriverStatus", this.getDriverStatus());
        orderMapping.put("FoodPreparationTime", this.getFoodPreparationTime());
        orderMapping.put("DriverStatusLatest", this.getDriverStatusLatest());
        orderMapping.put("OrderStatusLatest", this.getOrderStatusLatest());
        orderMapping.put("RestaurantStatusLatest", this.getRestStatusLatest());
        orderMapping.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e != null){
                    Log.e("ORDER SAVE ERROR", e.getLocalizedMessage());
                }else{

                    PaymentActivity.ObjectId = orderMapping.getObjectId();
                }
            }
        });
    }
}
