package com.entree.entree.constructors;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.io.Serializable;

/**
 * Created by jesudass on 13-Jan-16.
 */
public class CustomerAddresses implements Serializable {
    String addressId = "";
    ParseObject Customer = null;
    String FirstName = "";
    String LastName = "";
    String AddressTitle = "";
    String Address1 = "";
    String Address2 = "";
    String Landmark = "";
    String City = "";
    String State = "";
    String Pincode = "";
    String Latitude = "";
    String Longitude = "";

    public CustomerAddresses(String addressId,ParseObject Customer,
                              String FirstName,String LastName,String AddressTitle,String Address1,
                              String Address2,String Landmark,String City,
                              String State,String Pincode,String Latitude,String Longitude){
        this.addressId = addressId;
        this.Customer = Customer;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.AddressTitle = AddressTitle;
        this.Address1 = Address1;
        this.Address2 = Address2;
        this.Landmark = Landmark;
        this.City = City;
        this.State = State;
        this.Pincode = Pincode;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    public CustomerAddresses(ParseObject Customer,
                             String FirstName,String LastName,String AddressTitle,String Address1,
                             String Address2,String Landmark,String City,
                             String State,String Pincode,String Latitude,String Longitude){
        this.AddressTitle = AddressTitle;
        this.Customer = Customer;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Address1 = Address1;
        this.Address2 = Address2;
        this.Landmark = Landmark;
        this.City = City;
        this.State = State;
        this.Pincode = Pincode;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    public String getAddressId(){
        if(this.addressId==null){
            return "";
        }
        return this.addressId;
    }
    public ParseObject getCustomer(){
        return this.Customer;
    }
    public String getFirstName(){
        if(this.FirstName==null){
            return "";
        }
        return this.FirstName;
    }

    public String getAddressTitle() {
        return AddressTitle;
    }

    public String getLastName(){
        if(this.LastName==null){
            return "";
        }
        return this.LastName;
    }
    public String getAddress1(){
        if(this.Address1==null){
            return "";
        }
        return this.Address1;
    }
    public String getAddress2(){
        if(this.Address2==null){
            return "";
        }
        return this.Address2;
    }
    public String getLandmark(){
        if(this.Landmark==null){
            return "";
        }
        return this.Landmark;
    }
    public String getCity(){
        if(this.City==null){
            return "";
        }
        return this.City;
    }
    public String getState(){
        if(this.State==null){
            return "";
        }
        return this.State;
    }
    public String getPincode(){
        if(this.Pincode==null){
            return "";
        }
        return this.Pincode;
    }
    public String getLatitude(){
        if(this.Latitude==null) {
            return "";
        }
        return this.Latitude;
    }
    public String getLongitude(){
        if(this.Longitude==null) {
            return "";
        }
        return this.Longitude;
    }

    public void saveUserLocation(final ParseObject customer, final Handler handler){
        final ParseObject customerAddressObj = new ParseObject("CustomerAddresses");
        customerAddressObj.put("CustomerId",customer);
        customerAddressObj.put("FirstName",this.getFirstName());
        customerAddressObj.put("LastName",this.getLastName());
        customerAddressObj.put("Address1",this.getAddress1());
        customerAddressObj.put("Address2",this.getAddress2());
        customerAddressObj.put("Landmark",this.getLandmark());
        customerAddressObj.put("City",this.getCity());
        customerAddressObj.put("State",this.getState());
        customerAddressObj.put("Pincode",this.getPincode());
        customerAddressObj.put("Latitude",this.getLatitude());
        customerAddressObj.put("Longitude",this.getLongitude());

        customerAddressObj.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Message message = Message.obtain();
                message.setTarget(handler);
                if (e != null) {

                } else {

                    String FirstName = "";
                    String LastName = "";
                    String Address1 = "";
                    String Address2 = "";
                    String AddressTitle = "";
                    String Landmark = "";
                    String City = "";
                    String State = "";
                    String Pincode = "";
                    String Latitude = "";
                    String Longitude = "";

                    if(customerAddressObj.get("FirstName")!=null)
                        FirstName = customerAddressObj.get("FirstName").toString();
                    if(customerAddressObj.get("LastName")!=null)
                        LastName = customerAddressObj.get("LastName").toString();
                    if(customerAddressObj.get("Address1")!=null)
                        Address1 = customerAddressObj.get("Address1").toString();
                    if(customerAddressObj.get("Address2")!=null)
                        Address2 = customerAddressObj.get("Address2").toString();
                    if(customerAddressObj.get("AddressTitle")!=null)
                        AddressTitle = customerAddressObj.get("AddressTitle").toString();
                    if(customerAddressObj.get("Landmark")!=null)
                        Landmark = customerAddressObj.get("Landmark").toString();
                    if(customerAddressObj.get("City")!=null)
                        City = customerAddressObj.get("City").toString();
                    if(customerAddressObj.get("State")!=null)
                        State = customerAddressObj.get("State").toString();
                    if(customerAddressObj.get("Pincode")!=null)
                        Pincode = customerAddressObj.get("Pincode").toString();
                    if(customerAddressObj.get("Latitude")!=null)
                        Latitude = customerAddressObj.get("Latitude").toString();
                    if(customerAddressObj.get("Longitude")!=null)
                        Longitude = customerAddressObj.get("Longitude").toString();

                    CustomerParseObject customerParseObject = new CustomerParseObject(customer);
                    CustomerAddresses customerAddresses = new CustomerAddresses(
                            customerAddressObj.getObjectId(),
                            (ParseObject) customerAddressObj.get("CustomerId"),
                            FirstName,LastName,Address1,Address2,AddressTitle,
                            Landmark,City,State,Pincode,Latitude,Longitude);
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Customer", customerAddresses);
                    bundle.putSerializable("CustomerParseObject",customerParseObject);
                    message.setData(bundle);
                }
                message.sendToTarget();
            }
        });
    }
}
