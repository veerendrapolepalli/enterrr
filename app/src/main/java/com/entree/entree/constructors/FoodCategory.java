package com.entree.entree.constructors;

import java.io.Serializable;

/**
 * Created by jesudass on 1/6/2016.
 */
public class FoodCategory implements Serializable {
    private String FoodCategoryId="";
    private String FoodCategoryName="";
    private String CreatedDate="";
    private String UpdatedDate="";

    public FoodCategory(String FoodCategoryId,String FoodCategoryname,
                        String CreatedDate,String UpdatedDate) {
        this.FoodCategoryId= FoodCategoryId;
        this.FoodCategoryName= FoodCategoryname;
        this.CreatedDate= CreatedDate;
        this.UpdatedDate= UpdatedDate;
    }

    public FoodCategory(String FoodCategoryId,String FoodCategoryname) {
        this.FoodCategoryId= FoodCategoryId;
        this.FoodCategoryName= FoodCategoryname;
    }

    public String getFoodCategoryId(){
        return FoodCategoryId;
    }
    public String getFoodCategoryname(){
        return FoodCategoryName;
    }

}
