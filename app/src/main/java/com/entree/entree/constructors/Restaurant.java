package com.entree.entree.constructors;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jesudass on 1/5/2016.
 */
public class Restaurant implements Serializable {
    private String ResId = "";
    private String ResTypeName = "";
    private String Address1 = "";
    private String Address2 = "";
    private String City = "";
    private String State = "";
    private String Pincode = "";
    private String LandlineNo = "";
    private String FaxNo = "";
    private String Website = "";
    private String OpeningTime = "";
    private String ClosingTime = "";
    private String ContactPerson = "";
    private String ContactMobile = "";
    private String ContactEmail = "";
    private String Longitude = "";
    private String ResSlogan = "";
    private String CreatedDate = "";
    private String CreatedBy = "";
    private String ResPhoto = "";
    private String ModifiedDate = "";
    private String ModifiedBy = "";
    private Boolean IsClosed ;
    private Boolean IsAvailableForDelivery = true;
    private String ResName = "";
    private String Latitude = "";
    private String ResBannerPhoto = "";
    List<ParseObject> ob;

    public Restaurant(String ResId,String Longitude,String ResSlogan,String ClosingTime,
                      String ContactEmail,Boolean IsClosed,String Website,
                      String FaxNo,String Pincode,String ContactMobile,String LandlineNo,
                      String Address2,String ResPhoto,Boolean IsAvailableForDelivery,String ResName,
                      String ResTypeName,String State,String Address1,String Latitude,
                      String OpeningTime,String ContactPerson,String City,String ResBannerPhoto) {
        this.ResId = ResId;
        this.Longitude = Longitude;
        this.ResSlogan = ResSlogan;
        this.ClosingTime = ClosingTime;
        this.ContactEmail = ContactEmail;
        this.IsClosed = IsClosed;
        this.Website = Website;
        this.FaxNo = FaxNo;
        this.Pincode = Pincode;
        this.ContactMobile=ContactMobile;
        this.LandlineNo = LandlineNo;
        this.Address2 = Address2;
        this.ResPhoto = ResPhoto;
        this.IsAvailableForDelivery = IsAvailableForDelivery;
        this.ResName = ResName;
        this.ResTypeName = ResTypeName;
        this.State = State;
        this.Address1 = Address1;
        this.Latitude = Latitude;
        this.OpeningTime = OpeningTime;
        this.ContactPerson = ContactPerson;
        this.City = City;
        this.ResBannerPhoto = ResBannerPhoto;

    }
    public String getRestaurantId(){
        return ResId;
    }
    public String getLongitude(){
        return Longitude;
    }
    public String getResSlogan(){
        return ResSlogan;
    }
    public String getClosingTime(){
        return ClosingTime;
    }
    public String getContactEmail(){
        return ContactEmail;
    }
    public boolean getIsClosed(){
        return IsClosed;
    }
    public String getWebsite(){
        return Website;
    }
    public String getFaxNo(){
        return FaxNo;
    }
    public String getContactMobile(){
        return ContactMobile;
    }
    public String getPincode(){
        return Pincode;
    }
    public String getLandlineNo(){
        return LandlineNo;
    }
    public String getAddress2(){
        return Address2;
    }
    public String getResPhoto(){
        return ResPhoto;
    }
    public Boolean getIsAvailableForDelivery(){
        return IsAvailableForDelivery;
    }
    public String getResName(){
        return ResName;
    }
    public String getResTypeName(){
        return ResTypeName;
    }
    public String getState(){
        return State;
    }
    public String getAddress1(){
        return Address1;
    }
    public String getLatitude(){
        return Latitude;
    }
    public String getOpeningTime(){
        return OpeningTime;
    }
    public String getContactPerson(){
        return ContactPerson;
    }
    public String getCity(){
        return City;
    }
    public String getResBannerPhoto(){
        return ResBannerPhoto;
    }

    public ParseObject getRestaurantObj(){

        ParseQuery<ParseObject> queryRest = new ParseQuery<ParseObject>(
                "Restaurant");
        queryRest.whereEqualTo("objectId", this.getRestaurantId());
        try {
            ob = queryRest.find();
        } catch (ParseException e) {
            //logUtils.log(e.getMessage());
            e.printStackTrace();
        }
        if (ob != null) {
            return ob.get(0);
        }
        return null;
    }

}
