package com.entree.entree.constructors;

import java.util.ArrayList;

/**
 * Created by vamshi on 30-03-2016.
 */
public class OrderedItems  {

    String ResName;
    int ResTotal;
    ArrayList<String> foodName;
    ArrayList<Integer> foodQty;
    ArrayList<Integer> foodPrice;




    public OrderedItems(String resName, int resTotal, ArrayList<String> foodName, ArrayList<Integer> foodQty, ArrayList<Integer> foodPrice) {
        this.ResName = resName;
        this.ResTotal = resTotal;
        this.foodName = foodName;
        this.foodQty = foodQty;
        this.foodPrice = foodPrice;
    }

    public String getResName() {
        return ResName;
    }

    public void setResName(String resName) {
        ResName = resName;
    }

    public int getResTotal() {
        return ResTotal;
    }

    public void setResTotal(int resTotal) {
        ResTotal = resTotal;
    }

    public ArrayList<String> getFoodName() {
        return foodName;
    }

    public void setFoodName(ArrayList<String> foodName) {
        this.foodName = foodName;
    }

    public ArrayList<Integer> getFoodQty() {
        return foodQty;
    }

    public void setFoodQty(ArrayList<Integer> foodQty) {
        this.foodQty = foodQty;
    }

    public ArrayList<Integer> getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(ArrayList<Integer> foodPrice) {
        this.foodPrice = foodPrice;
    }

//    @Override
//    public boolean isSection() {
//        return true;
//    }
}
