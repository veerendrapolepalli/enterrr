package com.entree.entree.constructors;

import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jesudass on 1/23/2016.
 */
public class Employees implements Serializable {
    ParseObject userObj;
    ParseObject resObj;
    String role ;
    String firstName;
    String lastName;
    String middleName;
    String address1;
    String address2;
    String city;
    String state;
    String pincode;
    String mobNo;
    String altMobNo;
    String email;
    Boolean isActive;
    String designation;
    String age;
    ParseFile employeeImage;
    String lattitude;
    String longitude;
    Boolean isDriverAvailable;
    Date joiningDate;
    Date leavingDate;
    String dlNumber;
    String pickUpPoint;
    String dropOfpoint;

    public Employees(ParseObject userObj, ParseObject resObj, String role, String firstName,
                     String lastName, String middleName, String address1, String address2,
                     String city, String state, String pincode, String mobNo, String altMobNo,
                     String email, Boolean isActive, String designation, String age,
                     ParseFile employeeImage, String lattitude, String longitude,
                     Boolean isDriverAvailable, Date joiningDate, Date leavingDate,
                     String dlNumber, String pickUpPoint, String dropOfpoint) {
        this.userObj = userObj;
        this.resObj = resObj;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.mobNo = mobNo;
        this.altMobNo = altMobNo;
        this.email = email;
        this.isActive = isActive;
        this.designation = designation;
        this.age = age;
        this.employeeImage = employeeImage;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.isDriverAvailable = isDriverAvailable;
        this.joiningDate = joiningDate;
        this.leavingDate = leavingDate;
        this.dlNumber = dlNumber;
        this.pickUpPoint = pickUpPoint;
        this.dropOfpoint = dropOfpoint;
    }

    public ParseObject getUserObj() {
        return userObj;
    }

    public void setUserObj(ParseObject userObj) {
        this.userObj = userObj;
    }

    public ParseObject getResObj() {
        return resObj;
    }

    public void setResObj(ParseObject resObj) {
        this.resObj = resObj;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getAltMobNo() {
        return altMobNo;
    }

    public void setAltMobNo(String altMobNo) {
        this.altMobNo = altMobNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public ParseFile getEmployeeImage() {
        return employeeImage;
    }

    public void setEmployeeImage(ParseFile employeeImage) {
        this.employeeImage = employeeImage;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getIsDriverAvailable() {
        return isDriverAvailable;
    }

    public void setIsDriverAvailable(Boolean isDriverAvailable) {
        this.isDriverAvailable = isDriverAvailable;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public Date getLeavingDate() {
        return leavingDate;
    }

    public void setLeavingDate(Date leavingDate) {
        this.leavingDate = leavingDate;
    }

    public String getDlNumber() {
        return dlNumber;
    }

    public void setDlNumber(String dlNumber) {
        this.dlNumber = dlNumber;
    }

    public String getPickUpPoint() {
        return pickUpPoint;
    }

    public void setPickUpPoint(String pickUpPoint) {
        this.pickUpPoint = pickUpPoint;
    }

    public String getDropOfpoint() {
        return dropOfpoint;
    }

    public void setDropOfpoint(String dropOfpoint) {
        this.dropOfpoint = dropOfpoint;
    }
}
