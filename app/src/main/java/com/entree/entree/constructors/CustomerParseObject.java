package com.entree.entree.constructors;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by jesudass on 1/23/2016.
 */
public class CustomerParseObject implements Serializable {
    private ParseObject customer;

    public CustomerParseObject(ParseObject customer) {
        this.customer = customer;
    }

    public ParseObject getCustomer() {
        return customer;
    }
}

