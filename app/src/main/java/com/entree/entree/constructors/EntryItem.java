package com.entree.entree.constructors;

import com.entree.entree.interfacelistener.Items;

/**
 * Created by jesudass on 1/9/2016.
 */
public class EntryItem implements Items {

    private OrderItem orderItem;


    public EntryItem(OrderItem item) {
        this.orderItem = item;
    }
    public OrderItem getOrderItem(){
        return this.orderItem;
    }

    @Override
    public boolean isSection() {
        return false;
    }

}