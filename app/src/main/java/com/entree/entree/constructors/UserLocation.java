package com.entree.entree.constructors;

import android.location.Address;

import java.io.Serializable;

/**
 * Created by jesudass on 13-Jan-16.
 */
public class UserLocation implements Serializable {
    double Latitude;
    double Longitude;
    Address address;

    public UserLocation(double latitude,double longitude,Address address){
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.address = address;
    }

    public double getUserLattitude(){
        return this.Latitude;
    }
    public double getUserLongitude(){
        return this.Longitude;
    }
    public  Address getUserAddress(){
        return this.address;
    }

}
