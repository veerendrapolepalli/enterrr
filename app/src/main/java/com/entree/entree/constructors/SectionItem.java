package com.entree.entree.constructors;

import com.entree.entree.interfacelistener.Items;

/**
 * Created by jesudass on 1/9/2016.
 */
public class SectionItem implements Items {

    private final String title;
    private final String price;
    private Restaurant restObj;

    public Restaurant getRestObj() {
        return restObj;
    }

    public void setRestObj(Restaurant restObj) {
        this.restObj = restObj;
    }

    public SectionItem(String title,String price) {
        this.title = title;
        this.price  = price;
    }

    public SectionItem(String title,String price, Restaurant restObj) {
        this.title = title;
        this.price  = price;
        this.restObj = restObj;
    }

    public String getTitle(){
        return title;
    }
    public String getPrice(){
        return price;
    }

    @Override
    public boolean isSection() {
        return true;
    }

}
