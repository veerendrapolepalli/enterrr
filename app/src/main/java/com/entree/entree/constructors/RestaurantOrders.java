package com.entree.entree.constructors;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jesudass on 1/24/2016.
 */
public class RestaurantOrders implements Serializable {
    private String restName = "";
    private double total;
    private ArrayList<OrderItem> orderItems;

    public RestaurantOrders(String restName, ArrayList<OrderItem> orderItems,double total) {
        this.restName = restName;
        this.total = total;
        this.orderItems = orderItems;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getRestName() {
        return restName;
    }

    public void setRestName(String restName) {
        this.restName = restName;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
