package com.entree.entree.constructors;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jesudass on 1/24/2016.
 */
public class UserOrder implements Serializable {
    private Order uOrder;
    private ArrayList<OrderItem> arrOrderItem;

    public UserOrder(Order uOrder, ArrayList<OrderItem> arrOrderItem) {
        this.uOrder = uOrder;
        this.arrOrderItem = arrOrderItem;
    }

    public Order getuOrder() {
        return uOrder;
    }

    public void setuOrder(Order uOrder) {
        this.uOrder = uOrder;
    }

    public ArrayList<OrderItem> getArrOrderItem() {
        return arrOrderItem;
    }

    public void setArrOrderItem(ArrayList<OrderItem> arrOrderItem) {
        this.arrOrderItem = arrOrderItem;
    }
}
