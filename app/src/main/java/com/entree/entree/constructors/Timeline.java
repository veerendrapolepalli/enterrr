package com.entree.entree.constructors;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by vamshi on 25-03-2016.
 */
public class Timeline implements Serializable {

    ParseObject driver;
    ParseObject employee;
    ParseObject order;
    ParseObject restaurant;

    String Status;
    String TimelineNote;
    String TimelineType;

    public Timeline() {
    }

    public Timeline(ParseObject driver, ParseObject employee, ParseObject order, ParseObject restaurant, String status, String timelineNote, String timelineType) {
        this.driver = driver;
        this.employee = employee;
        this.order = order;
        this.restaurant = restaurant;
        this.Status = status;
        this.TimelineNote = timelineNote;
        this.TimelineType = timelineType;
    }

    public ParseObject getDriver() {
        return driver;
    }

    public void setDriver(ParseObject driver) {
        this.driver = driver;
    }

    public ParseObject getEmployee() {
        return employee;
    }

    public void setEmployee(ParseObject employee) {
        this.employee = employee;
    }

    public ParseObject getOrder() {
        return order;
    }

    public void setOrder(ParseObject order) {
        this.order = order;
    }

    public ParseObject getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(ParseObject restaurant) {
        this.restaurant = restaurant;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTimelineNote() {
        return TimelineNote;
    }

    public void setTimelineNote(String timelineNote) {
        TimelineNote = timelineNote;
    }

    public String getTimelineType() {
        return TimelineType;
    }

    public void setTimelineType(String timelineType) {
        TimelineType = timelineType;
    }

    public void syncTimeLine(){

        ParseObject oTimeLine = new ParseObject("Timeline");

        oTimeLine.put("DriverId",getDriver());
        oTimeLine.put("EmployeeId",getEmployee());
        oTimeLine.put("OrderId",getOrder());
        oTimeLine.put("ResId",getRestaurant());
        oTimeLine.put("Status",getStatus());
        oTimeLine.put("TimelineNote",getTimelineNote());
        oTimeLine.put("TimelineType",getTimelineType());
        oTimeLine.saveInBackground();



    }




}
