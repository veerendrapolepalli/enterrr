package com.entree.entree.interfacelistener;

/**
 * Created by jesudass on 13-Feb-16.
 */
public interface SelectedPlaceDetailListener {
    public void onFetchedDetailComplete(String lattitude, String longitude);
}
