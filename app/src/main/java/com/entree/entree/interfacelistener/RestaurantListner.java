package com.entree.entree.interfacelistener;

import com.entree.entree.constructors.Restaurant;

import java.util.ArrayList;

/**
 * Created by jesudass on 05/02/16.
 */
public interface RestaurantListner {
    public void returnRestaurantList(ArrayList<Restaurant> arrRestaurant);
}
