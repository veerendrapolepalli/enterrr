package com.entree.entree.interfacelistener;

/**
 * Created by jesudass on 1/23/2016.
 */
public interface UpdateFoodView {
    public void onViewChanged();
}
