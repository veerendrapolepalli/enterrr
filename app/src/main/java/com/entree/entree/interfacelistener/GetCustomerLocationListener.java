package com.entree.entree.interfacelistener;

import com.entree.entree.constructors.CustomerAddresses;

import java.util.ArrayList;

/**
 * Created by jesudass on 13-Feb-16.
 */
public interface GetCustomerLocationListener {
    public void onCustomerLocationFetched(ArrayList<CustomerAddresses> arrCustomerAddress);
}
