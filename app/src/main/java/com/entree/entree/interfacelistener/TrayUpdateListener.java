package com.entree.entree.interfacelistener;

/**
 * Created by jesudass on 09/02/16.
 */
public interface TrayUpdateListener {
    public void onTrayUpdate();
}
