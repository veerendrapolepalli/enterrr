package com.entree.entree.interfacelistener;

/**
 * Created by vpol0001 on 5/12/2016.
 */
public interface FoodStatusChange {

    void notifyFoodChange();

    void notifyPriceChange();

}
