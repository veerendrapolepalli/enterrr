package com.entree.entree.interfacelistener;

import com.entree.entree.constructors.SelectedLocation;

import java.util.ArrayList;

/**
 * Created by jesudass on 13-Feb-16.
 */
public interface SearchedLocationListener {
    public void onSearchComplete(ArrayList<SelectedLocation> arrselectedLocation, ArrayList<String> names);
}
