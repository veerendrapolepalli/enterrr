package com.entree.entree.interfacelistener;

/**
 * Created by jesudass on 1/9/2016.
 */
public interface FoodChangeListener {
    public void onDataChanged(int price);

}
