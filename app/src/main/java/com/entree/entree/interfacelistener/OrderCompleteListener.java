package com.entree.entree.interfacelistener;

/**
 * Created by jesudass on 15/02/16.
 */
public interface OrderCompleteListener {
    public void onOrderSynced();
}
