package com.entree.entree.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Jesudass on 2/22/2016.
 */
public class AvenirBook extends TextView {

    public AvenirBook(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AvenirBook(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvenirBook(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/AvenirLTStd-Book.otf");
        setTypeface(tf);
    }
}
