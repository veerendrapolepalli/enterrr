package com.entree.entree.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by Jesudass on 2/19/2016.
 */
public class EnTextView extends TextView {

    private Context mContext;

    public static final int TF_AVENIR_BOOK = 0;
    public static final int TF_AVENIR_HEAVY = 1;
    public static final int TF_AVENIR_LIGHT = 2;
    public static final int TF_AVENIR_MEDIUM = 3;
    public static final int TF_ROBOTO_CONDENCED = 4;
    public static final int TF_ROBOTO_COND_LIGHT = 5;
    public static final String avenirBook = "fonts/AvenirLTStd-Book.otf";
    public static final String avenirHeavy = "fonts/AvenirLTStd-Heavy.otf";
    public static final String avenirLight = "fonts/AvenirLTStd-Light.otf";
    public static final String avenirMedium = "fonts/AvenirLTStd-Medium.otf";
    public static final String robotoCondensed = "fonts/Roboto-Condensed.ttf";
    public static final String robotoCondensedLight = "fonts/Roboto-Condensed-Light.ttf";

    private static HashMap<String, Typeface> mapCreatedTypeFace = new HashMap<String, Typeface>();

    public EnTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        // setTextSize(28);
    }


    private void setFont(String fontName) {
        Typeface font = mapCreatedTypeFace.get(fontName);
        if (null == font) {
            font = Typeface.createFromAsset(mContext.getAssets(), fontName);
            mapCreatedTypeFace.put(fontName, font);
        }
        setTypeface(font);
    }


    public void setTypeFace(int type) {
        switch (type) {
            case TF_AVENIR_BOOK:
                setFont(avenirBook);
                break;
            case TF_AVENIR_HEAVY:
                setFont(avenirHeavy);
                break;
            case TF_AVENIR_LIGHT:
                setFont(avenirLight);
                break;
            case TF_AVENIR_MEDIUM:
                setFont(avenirMedium);
                break;
            case TF_ROBOTO_CONDENCED:
                setFont(robotoCondensed);
                break;
            case TF_ROBOTO_COND_LIGHT:
                setFont(robotoCondensedLight);
                break;
        }

    }

    @Override
    public void setTypeface(Typeface tf) {
        if (isInEditMode()) {
            return;
        }

       /* String fontPath = GlobalProperties.getInstane().getCustomFontPath();
        if (fontPath != null && !fontPath.equals("")) {
            tf = mapCreatedTypeFace.get(fontPath);*/
            if (null == tf) {
                //tf = Typeface.createFromFile(fontPath);
                //mapCreatedTypeFace.put(fontPath, tf);
            }
       // }

        super.setTypeface(tf);
    }
}
