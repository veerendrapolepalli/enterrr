package com.entree.entree.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Jesudass on 2/22/2016.
 */
public class RobottowCondenced extends TextView {

    public RobottowCondenced(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RobottowCondenced(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RobottowCondenced(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/Roboto-Condensed.ttf");
        setTypeface(tf);
    }
}
