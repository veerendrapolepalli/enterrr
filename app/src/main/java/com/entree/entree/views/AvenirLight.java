package com.entree.entree.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Jesudass on 2/22/2016.
 */
public class AvenirLight extends TextView {

    public AvenirLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AvenirLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvenirLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/AvenirLTStd-Light.otf");
        setTypeface(tf);
    }
}
