package com.entree.entree.views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.entree.entree.R;
import com.entree.entree.activity.MapLocation;
import com.entree.entree.activity.PaymentActivity;
import com.entree.entree.activity.StoreActivity;
import com.entree.entree.activity.TrayContactActivity;
import com.entree.entree.adapter.CustomerAddressListAdapter;
import com.entree.entree.application.EntreeApplication;
import com.entree.entree.constructors.CustomerAddresses;
import com.entree.entree.constructors.EntryItem;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.constructors.UserLocation;
import com.entree.entree.interfacelistener.Items;
import com.entree.entree.utils.JSONParser;
import com.entree.entree.utils.Utils;
import com.entree.entree.wheel.ArrayWheelAdapter;
import com.entree.entree.wheel.OnWheelChangedListener;
import com.entree.entree.wheel.OnWheelScrollListener;
import com.entree.entree.wheel.WheelView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by nidhin on 26/01/16.
 */
public class ReorderFlow extends RelativeLayout implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMapClickListener {
    private Context mContext;
    Button btnDismiss, btnMap, btnSelectDay, btnSelectTime;
    ParseObject customersObj;
    ProgressDialog pdialog;
    ArrayList<CustomerAddresses> arrCustAddr = new ArrayList<CustomerAddresses>();
    ListView lvUserAddress;
    Utils utils;
    AutoCompleteTextView etSearch_location;
    protected Location mLastLocation;
    String[] search_text;
    String url;
    ArrayList<String> names;
    ArrayList<SelectedLocation> arrselectedLocation = new ArrayList<SelectedLocation>();
    ArrayAdapter<String> adp;
    GoogleMap googleMap;
    JSONObject json;
    JSONArray contacts = null;
    private static final String TAG_RESULT = "predictions";
    String browserKey = "AIzaSyCLzXyzHCtBEhWaSPvwgu1ixiZEnUBxlcs";
    int width;
    GoogleApiClient mGoogleApiClient;
    AppCompatActivity activity;
    LinearLayout llMapView, llSelectAddress, llDatePicker, llTimePicker;
    SupportMapFragment fm;
    View popupView;
    WheelView wheelTime, wheelDate;
    String wheelMenuDays[] = new String[]{"Tomorrow", "Today", "Day After"};
    String wheelMenuTime[] = new String[]{"12:00", "12:15", "12:30", "12:45", "1:00", "1:15", "1:30"};
    private boolean wheelScrolled = false;
    CustomerAddresses custAddress;
    JSONObject orderIdObj;
    ArrayList<Items> items;
    ParseObject order;
    String priceAmt;
    PopupWindow popupWindow;

    public ReorderFlow(Context context, ArrayList<Items> items, String priceAmt) {
        super(context);
        this.mContext = context;
        this.items = items;
        this.priceAmt = priceAmt;
        init();
        getOrderDetails();
    }

    public ReorderFlow(Context context, ArrayList<Items> items, String priceAmt, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mContext = context;
        this.items = items;
        this.priceAmt = priceAmt;
        init();
        getOrderDetails();
    }


    public ReorderFlow(Context context, ArrayList<Items> items, String priceAmt, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        this.mContext = context;
        this.items = items;
        this.priceAmt = priceAmt;
        init();
        getOrderDetails();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ReorderFlow(Context context, ArrayList<Items> items, String priceAmt, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        this.mContext = context;
        this.items = items;
        this.priceAmt = priceAmt;
        init();
        getOrderDetails();
    }

    private void setUtils() {
        utils = new Utils(mContext);
    }

    private void init() {
        activity = (AppCompatActivity) mContext;
        setUtils();
        width = mContext.getResources().getDisplayMetrics().widthPixels;
        int height = mContext.getResources().getDisplayMetrics().heightPixels;

        LayoutInflater layoutInflater
                = (LayoutInflater) activity
                .getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        popupView = layoutInflater.inflate(R.layout.popup_window, null);
        popupWindow = new PopupWindow(
                popupView,
                (int) (width * .8),
                ViewGroup.LayoutParams.WRAP_CONTENT, true);

        initUi(popupView);
        initMapView();
        new GetUserAddress(popupWindow, popupView).execute();
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
                EntreeApplication.arrUserOtderItems.clear();
                TrayContactActivity.userLocation = null;
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                ft.remove(fm);
                ft.commit();
                popupView = null;

            }
        });

    }

    private void initUi(View popupView) {
        btnDismiss = (Button) popupView.findViewById(R.id.btnDismiss);
        btnMap = (Button) popupView.findViewById(R.id.btnMap);
        btnSelectDay = (Button) popupView.findViewById(R.id.btnSelectDay);
        btnSelectTime = (Button) popupView.findViewById(R.id.btnSelectTime);


        lvUserAddress = (ListView) popupView.findViewById(R.id.lvUserAddress);
        etSearch_location = (AutoCompleteTextView) popupView.findViewById(R.id.etSearch_location);
        llMapView = (LinearLayout) popupView.findViewById(R.id.llMapView);
        llSelectAddress = (LinearLayout) popupView.findViewById(R.id.llSelectAddress);
        llTimePicker = (LinearLayout) popupView.findViewById(R.id.llTimePicker);
        llDatePicker = (LinearLayout) popupView.findViewById(R.id.llDatePicker);

        wheelDate = (WheelView) popupView.findViewById(R.id.wheelDay);
        wheelTime = (WheelView) popupView.findViewById(R.id.wheelTime);

        etSearch_location.setThreshold(0);
        setOnClickListeners();

        ViewGroup.LayoutParams params = wheelDate.getLayoutParams();
        params.height = (int) (width * .6);
        params.width = LayoutParams.MATCH_PARENT;
//        wheelDate.setMinimumHeight(params.height);
        wheelDate.setLayoutParams(params);

        params = wheelTime.getLayoutParams();
        params.height = (int) (width * .6);
        params.width = LayoutParams.MATCH_PARENT;
//        llTimePicker.setMinimumHeight(params.height);
        wheelTime.setLayoutParams(params);

        initWheelDay();
        initWheelTime();
    }

    private void initMapView() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        fm = (SupportMapFragment) activity.getSupportFragmentManager()
                .findFragmentById(R.id.map);
        googleMap = fm.getMap();
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnMapClickListener(this);

        ViewGroup.LayoutParams params = fm.getView().getLayoutParams();
        params.height = (int) (width * .8);
        fm.getView().setLayoutParams(params);


    }

    private void setOnClickListeners() {
        lvUserAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                custAddress = arrCustAddr.get(position);
                mLastLocation = new Location("");
                mLastLocation.setLatitude(Double.parseDouble(custAddress.getLatitude()));
                mLastLocation.setLongitude(Double.parseDouble(custAddress.getLongitude()));
                llSelectAddress.setVisibility(View.GONE);
                llMapView.setVisibility(View.VISIBLE);
                addMarker();
            }
        });

        btnMap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llMapView.setVisibility(View.GONE);
                llDatePicker.setVisibility(View.VISIBLE);

            }
        });
        btnSelectDay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                llDatePicker.setVisibility(View.GONE);
                llTimePicker.setVisibility(View.VISIBLE);
            }
        });

        btnSelectTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Address addr = new Address(Locale.getDefault());
                String name = custAddress.getAddress1();
                addr.setAddressLine(0, name != null ? name : "");
                addr.setFeatureName(name);
                addr.setLocality(name);
                addr.setAdminArea(name);
                addr.setPostalCode(name);
                popupWindow.dismiss();
                TrayContactActivity.userLocation = new UserLocation(Double.parseDouble(
                        custAddress.getLatitude()),
                        Double.parseDouble(custAddress.getLongitude()), addr);

                Intent i = new Intent(mContext, PaymentActivity.class);
                i.putExtra("priceAmt", priceAmt);
                i.putExtra("CustomerId", customersObj.getObjectId());
                mContext.startActivity(i);
            }
        });

        etSearch_location.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {
                if (arrselectedLocation != null && arrselectedLocation.size() > 0) {

                    pdialog = new ProgressDialog(mContext);
                    SelectedLocation select = arrselectedLocation.get(pos);
                    String detailUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=%s";
                    detailUrl = String.format(detailUrl, select.placeId, browserKey);

                    pdialog.setTitle("Loading your Location");
                    pdialog.show();
                    new GetPlaceDetails(detailUrl).execute();
                }
            }
        });

        etSearch_location.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                search_text = etSearch_location.getText().toString().split(",");
                //if (mLastLocation != null) {
                //  String latnlong = mLastLocation.getLatitude() + "," + mLastLocation.getLongitude();
                url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" +
                        URLEncoder.encode(search_text[0]) + "6&radius=500&sensor=true&key=" + browserKey;
                if (search_text.length <= 1) {
                    names = new ArrayList<String>();
                    Log.d("URL", url);
                    parseData parse = new parseData();
                    parse.execute();
                }
                //}

            }
        });

        etSearch_location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hidekeyboard(v);
                } else {

                }
            }
        });

    }

    private void hidekeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initWheelDay() {
        wheelDate.setViewAdapter(new ArrayWheelAdapter(mContext, wheelMenuDays));
        wheelDate.setVisibleItems(2);
        wheelDate.setCurrentItem(0);
        wheelDate.addChangingListener(changedListener);
        wheelDate.addScrollingListener(scrolledListener);
    }

    private void initWheelTime() {
        wheelTime.setViewAdapter(new ArrayWheelAdapter(mContext, wheelMenuTime));
        wheelTime.setVisibleItems(2);
        wheelTime.setCurrentItem(0);
        wheelTime.addChangingListener(changedListener);
        wheelTime.addScrollingListener(scrolledListener);
    }

    // Wheel scrolled listener
    OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollStarts(WheelView wheel) {
            wheelScrolled = true;
        }

        public void onScrollEnds(WheelView wheel) {
            wheelScrolled = false;
        }

        @Override
        public void onScrollingStarted(WheelView wheel) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            // TODO Auto-generated method stub

        }
    };

    private final OnWheelChangedListener changedListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (!wheelScrolled) {
                // updateStatus();
                //wheelMenuDays[getWheel(R.id.wheelDay).getCurrentItem()
            }
        }
    };

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    private class GetUserAddress extends AsyncTask<Void, Void, Void> {
        List<ParseObject> ob = null;
        List<ParseObject> listCustomerAddress = null;
        PopupWindow popupWindow;
        View popupView;

        public GetUserAddress(PopupWindow popupWindow, View popupView) {
            this.popupWindow = popupWindow;
            this.popupView = popupView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils.showLoader();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                currentUser.getObjectId();
            }
            ParseQuery<ParseObject> queryCustomers = new ParseQuery<ParseObject>(
                    "Customers");
            queryCustomers.whereEqualTo("UserId", currentUser);
            try {
                ob = queryCustomers.find();
            } catch (ParseException e) {
                //logUtils.log(e.getMessage());
                e.printStackTrace();
            }
            if (ob != null && ob.size() > 0) {
                customersObj = (ParseObject) ob.get(0);
                ParseQuery<ParseObject> queryCustomerAddr = new ParseQuery<ParseObject>(
                        "CustomerAddresses");
                queryCustomerAddr.whereEqualTo("CustomerId", customersObj);
                queryCustomerAddr.setLimit(3);

                try {
                    listCustomerAddress = queryCustomerAddr.find();
                } catch (ParseException e) {
                    //logUtils.log(e.getMessage());
                    e.printStackTrace();
                }
                if (listCustomerAddress != null && listCustomerAddress.size() > 0) {
                    for (ParseObject custAddrObj : listCustomerAddress) {
                        String addressId = custAddrObj.getObjectId();
                        ParseObject customer = (ParseObject) custAddrObj.get("CustomerId");
                        String firstName = custAddrObj.getString("FirstName");
                        String lastName = custAddrObj.getString("LastName");
                        String addressTitle = custAddrObj.getString("AddressTitle");
                        String address1 = custAddrObj.getString("Address1");
                        String address2 = custAddrObj.getString("Address2");
                        String landMark = custAddrObj.getString("Landmark");
                        String city = custAddrObj.getString("City");
                        String state = custAddrObj.getString("State");
                        String pincode = custAddrObj.getString("Pincode");
                        String latitude = custAddrObj.getString("Latitude");
                        String longitude = custAddrObj.getString("Longitude");

                        CustomerAddresses custAddress = new CustomerAddresses(addressId, customersObj,
                                firstName, lastName, addressTitle, address1, address2, landMark,
                                city, state, pincode, latitude, longitude);
                        arrCustAddr.add(custAddress);
                    }

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCustAddr != null && arrCustAddr.size() > 0) {
                CustomerAddressListAdapter customerAddressListAdapter =
                        new CustomerAddressListAdapter(mContext, arrCustAddr);

                lvUserAddress.setAdapter(customerAddressListAdapter);
                lvUserAddress.setVisibility(View.VISIBLE);
            } else {
                lvUserAddress.setVisibility(View.GONE);
            }
            utils.hideLoader();
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            //popupWindow.update(0, 0, (int) (width * .8), ViewGroup.LayoutParams.WRAP_CONTENT);

        }
    }

    public class parseData extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            json = jParser.getJSONFromUrl(url.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts
                    contacts = json.getJSONArray(TAG_RESULT);
                    arrselectedLocation = new ArrayList<SelectedLocation>();
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);
                        String description = c.getString("description");
                        String placeId = c.getString("place_id");
                        Log.d("description", description);
                        arrselectedLocation.add(new SelectedLocation(placeId, description));
                        names.add(description);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            adp = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_list_item_1, names) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setTextColor(Color.BLACK);
                    return view;
                }
            };
            etSearch_location.setAdapter(adp);


        }
    }

    public class GetPlaceDetails extends AsyncTask<Void, Integer, Void> {
        String detailUrl = "";
        JSONObject placeDetail;

        public GetPlaceDetails(String detailUrl) {
            this.detailUrl = detailUrl;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub


            JSONParser jParser = new JSONParser();

            // getting JSON string from URL
            json = jParser.getJSONFromUrl(detailUrl.toString());
            if (json != null) {
                try {
                    // Getting Array of Contacts
                    placeDetail = json.getJSONObject("result");
                    if (placeDetail != null) {
                        placeDetail = placeDetail.getJSONObject("geometry");
                        if (placeDetail != null) {
                            placeDetail = placeDetail.getJSONObject("location");
                            if (placeDetail != null) {
                                String lat = placeDetail.getString("lat");
                                String lng = placeDetail.getString("lng");

                                mLastLocation.setLatitude(Double.parseDouble(lat));
                                mLastLocation.setLongitude(Double.parseDouble(lng));

                                Intent i = new Intent(mContext, MapLocation.class);
                                i.putExtra("LATITUDE", mLastLocation.getLatitude());
                                i.putExtra("LONGITUDE", mLastLocation.getLongitude());
                                i.putExtra("LOCATION", mLastLocation);
                                mContext.startActivity(i);
                            }
                        }
                    }
                    Log.d("GEOMETRY", placeDetail.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            adp = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_list_item_1, names) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView text = (TextView) view.findViewById(android.R.id.text1);
                    text.setTextColor(Color.BLACK);
                    return view;
                }
            };
            etSearch_location.setAdapter(adp);
            pdialog.dismiss();

        }
    }

    private void addMarker() {
        MarkerOptions options = new MarkerOptions();

        // following four lines requires 'Google Maps Android API Utility Library'
        // https://developers.google.com/maps/documentation/android/utility/
        // I have used this to display the time as title for location markers
        // you can safely comment the following four lines but for this info
        IconGenerator iconFactory = new IconGenerator(activity);
        iconFactory.setStyle(IconGenerator.STYLE_PURPLE);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pointer));
        options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

        LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        options.position(currentLatLng);
        Marker mapMarker = googleMap.addMarker(options);
        mapMarker.setDraggable(true);

//        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date(atTime));
//        mapMarker.setTitle(mLastUpdateTime);
//        Log.d(TAG, "Marker added.............................");
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }

    private void getOrderDetails() {
        EntryItem ei = null;
        for (Items item : items) {
            if (!item.isSection()) {
                ei = (EntryItem) item;
                if (ei != null) {
                    OrderItem orderItem = ei.getOrderItem();
                    if (orderItem.getResObj() == null) {
                        if (orderItem.getResID() != null || orderItem.getResID().equalsIgnoreCase("")) {
                            ParseQuery<ParseObject> queryOrders = new ParseQuery<ParseObject>(
                                    "Restaurant");
                            queryOrders.whereEqualTo("objectId", orderItem.getResID());
                            List<ParseObject> ob = null;
                            try {
                                ob = queryOrders.find();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (ob != null && ob.size() > 0) {
                                ParseObject restObj = ob.get(0);
                                orderItem.setRestObj(restObj);
                            }
                        }
                    }
                    EntreeApplication.arrUserOtderItems.add(orderItem);
                }

            }
        }
    }


    public class SelectedLocation {
        public String placeId;
        public String placeName;

        public SelectedLocation(String placeId, String placeName) {
            this.placeId = placeId;
            this.placeName = placeName;
        }

    }


}
