package com.entree.entree.application;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.entree.entree.constructors.OrderItem;
import com.entree.entree.handlers.ExampleNotificationOpenedHandler;
import com.entree.entree.interfacelistener.FoodStatusChange;
import com.onesignal.OneSignal;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

/**
 * Created by vpol0001 on 5/7/2016.
 */

public class EntreeApplication extends MultiDexApplication {
    public static ArrayList<OrderItem> arrUserOtderItems = new ArrayList<OrderItem>();
    public static ArrayList<FoodStatusChange> foodStatusChangeListners = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .setAutoPromptLocation(false)
                .init();
        Fabric.with(this, new Crashlytics());
    }


    public static void registerFoodChangeListner(FoodStatusChange lisnter){
        if(!foodStatusChangeListners.contains(lisnter))
            foodStatusChangeListners.add(lisnter);
    }

    public static void unRegisterFoodChangeListner(FoodStatusChange lisnter){
        if(!foodStatusChangeListners.contains(lisnter))
            foodStatusChangeListners.remove(lisnter);
    }


    public static void notifyFoodChange() {
        for (FoodStatusChange listner : foodStatusChangeListners){
            if(listner != null){
                listner.notifyFoodChange();
                listner.notifyPriceChange();
            }
        }
    }

}
