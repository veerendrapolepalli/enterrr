-keep class com.entree.entree.application.EntreeApplication {
    <init>();
    void attachBaseContext(android.content.Context);
}
-keep class com.entree.entree.activity.EntreePreview { <init>(); }
-keep class com.entree.entree.activity.SignIn { <init>(); }
-keep class com.entree.entree.activity.SignUp { <init>(); }
-keep class com.entree.entree.activity.UserLocationActivity { <init>(); }
-keep class com.facebook.FacebookActivity { <init>(); }
-keep class com.entree.entree.activity.Search { <init>(); }
-keep class com.entree.entree.activity.MapLocation { <init>(); }
-keep class com.entree.entree.activity.StoreActivity { <init>(); }
-keep class com.entree.entree.activity.RestaurantList { <init>(); }
-keep class com.entree.entree.activity.TrayOrderSummary { <init>(); }
-keep class com.entree.entree.activity.Profile { <init>(); }
-keep class com.entree.entree.activity.SearchRestaurantActivity { <init>(); }
-keep class com.entree.entree.activity.TrayContactActivity { <init>(); }
-keep class com.entree.entree.activity.PaymentActivity { <init>(); }
-keep class com.entree.entree.activity.OrderHistoryActivity { <init>(); }
-keep class com.entree.entree.activity.OrderHistorySummary { <init>(); }
-keep class com.entree.entree.activity.MyAccountActivity { <init>(); }
-keep class com.entree.entree.activity.ForgotPasswordActivity { <init>(); }
-keep class com.entree.entree.activity.TrackOrderActivity { <init>(); }
-keep class com.entree.entree.activity.EditProfileActivity { <init>(); }
-keep class com.entree.entree.activity.EmptyTrayActivity { <init>(); }
-keep class com.entree.entree.chat.MainActivity { <init>(); }
-keep class com.entree.entree.chat.SendBirdMessagingActivity { <init>(); }
-keep class com.google.android.gms.ads.AdActivity { <init>(); }
-keep class com.google.android.gms.ads.purchase.InAppPurchaseActivity { <init>(); }
-keep class com.google.android.gms.appinvite.PreviewActivity { <init>(); }
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(); }
-keep class com.google.android.gms.auth.api.signin.RevocationBoundService { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementContentProvider { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementReceiver { <init>(); }
-keep class com.google.android.gms.measurement.AppMeasurementService { <init>(); }
-keep class com.citrus.sdk.CitrusActivity { <init>(); }
-keep class com.onesignal.GcmBroadcastReceiver { <init>(); }
-keep class com.onesignal.NotificationOpenedReceiver { <init>(); }
-keep class com.onesignal.GcmIntentService { <init>(); }
-keep class com.onesignal.SyncService { <init>(); }
-keep class com.onesignal.PermissionsActivity { <init>(); }
-keep public class * extends android.app.backup.BackupAgent {
    <init>();
}
-keep public class * extends java.lang.annotation.Annotation {
    *;
}
-keep class com.android.tools.fd.** {
    *;
}
-dontnote com.android.tools.fd.**,android.support.multidex.MultiDexExtractor
