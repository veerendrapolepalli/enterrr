/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.onesignal;

public final class R {
	public static final class attr {
		public static final int ambientEnabled = 0x7f010082;
		public static final int buttonSize = 0x7f0100a8;
		public static final int cameraBearing = 0x7f010073;
		public static final int cameraTargetLat = 0x7f010074;
		public static final int cameraTargetLng = 0x7f010075;
		public static final int cameraTilt = 0x7f010076;
		public static final int cameraZoom = 0x7f010077;
		public static final int circleCrop = 0x7f010071;
		public static final int colorScheme = 0x7f0100a9;
		public static final int imageAspectRatio = 0x7f010070;
		public static final int imageAspectRatioAdjust = 0x7f01006f;
		public static final int liteMode = 0x7f010078;
		public static final int mapType = 0x7f010072;
		public static final int scopeUris = 0x7f0100aa;
		public static final int uiCompass = 0x7f010079;
		public static final int uiMapToolbar = 0x7f010081;
		public static final int uiRotateGestures = 0x7f01007a;
		public static final int uiScrollGestures = 0x7f01007b;
		public static final int uiTiltGestures = 0x7f01007c;
		public static final int uiZoomControls = 0x7f01007d;
		public static final int uiZoomGestures = 0x7f01007e;
		public static final int useViewLifecycle = 0x7f01007f;
		public static final int zOrderOnTop = 0x7f010080;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f0c002a;
		public static final int common_google_signin_btn_text_dark = 0x7f0c0099;
		public static final int common_google_signin_btn_text_dark_default = 0x7f0c002b;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f0c002c;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f0c002d;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f0c002e;
		public static final int common_google_signin_btn_text_light = 0x7f0c009a;
		public static final int common_google_signin_btn_text_light_default = 0x7f0c002f;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f0c0030;
		public static final int common_google_signin_btn_text_light_focused = 0x7f0c0031;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f0c0032;
		public static final int common_plus_signin_btn_text_dark = 0x7f0c009b;
		public static final int common_plus_signin_btn_text_dark_default = 0x7f0c0033;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f0c0034;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f0c0035;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f0c0036;
		public static final int common_plus_signin_btn_text_light = 0x7f0c009c;
		public static final int common_plus_signin_btn_text_light_default = 0x7f0c0037;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f0c0038;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f0c0039;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f0c003a;
		public static final int place_autocomplete_prediction_primary_text = 0x7f0c0066;
		public static final int place_autocomplete_prediction_primary_text_highlight = 0x7f0c0067;
		public static final int place_autocomplete_prediction_secondary_text = 0x7f0c0068;
		public static final int place_autocomplete_search_hint = 0x7f0c0069;
		public static final int place_autocomplete_search_text = 0x7f0c006a;
		public static final int place_autocomplete_separator = 0x7f0c006b;
	}
	public static final class dimen {
		public static final int place_autocomplete_button_padding = 0x7f0900a2;
		public static final int place_autocomplete_powered_by_google_height = 0x7f0900a3;
		public static final int place_autocomplete_powered_by_google_start = 0x7f0900a4;
		public static final int place_autocomplete_prediction_height = 0x7f0900a5;
		public static final int place_autocomplete_prediction_horizontal_margin = 0x7f0900a6;
		public static final int place_autocomplete_prediction_primary_text = 0x7f0900a7;
		public static final int place_autocomplete_prediction_secondary_text = 0x7f0900a8;
		public static final int place_autocomplete_progress_horizontal_margin = 0x7f0900a9;
		public static final int place_autocomplete_progress_size = 0x7f0900aa;
		public static final int place_autocomplete_separator_start = 0x7f0900ab;
	}
	public static final class drawable {
		public static final int common_full_open_on_phone = 0x7f02007c;
		public static final int common_google_signin_btn_icon_dark = 0x7f02007d;
		public static final int common_google_signin_btn_icon_dark_disabled = 0x7f02007e;
		public static final int common_google_signin_btn_icon_dark_focused = 0x7f02007f;
		public static final int common_google_signin_btn_icon_dark_normal = 0x7f020080;
		public static final int common_google_signin_btn_icon_dark_pressed = 0x7f020081;
		public static final int common_google_signin_btn_icon_light = 0x7f020082;
		public static final int common_google_signin_btn_icon_light_disabled = 0x7f020083;
		public static final int common_google_signin_btn_icon_light_focused = 0x7f020084;
		public static final int common_google_signin_btn_icon_light_normal = 0x7f020085;
		public static final int common_google_signin_btn_icon_light_pressed = 0x7f020086;
		public static final int common_google_signin_btn_text_dark = 0x7f020087;
		public static final int common_google_signin_btn_text_dark_disabled = 0x7f020088;
		public static final int common_google_signin_btn_text_dark_focused = 0x7f020089;
		public static final int common_google_signin_btn_text_dark_normal = 0x7f02008a;
		public static final int common_google_signin_btn_text_dark_pressed = 0x7f02008b;
		public static final int common_google_signin_btn_text_light = 0x7f02008c;
		public static final int common_google_signin_btn_text_light_disabled = 0x7f02008d;
		public static final int common_google_signin_btn_text_light_focused = 0x7f02008e;
		public static final int common_google_signin_btn_text_light_normal = 0x7f02008f;
		public static final int common_google_signin_btn_text_light_pressed = 0x7f020090;
		public static final int common_ic_googleplayservices = 0x7f020091;
		public static final int common_plus_signin_btn_icon_dark = 0x7f020092;
		public static final int common_plus_signin_btn_icon_dark_disabled = 0x7f020093;
		public static final int common_plus_signin_btn_icon_dark_focused = 0x7f020094;
		public static final int common_plus_signin_btn_icon_dark_normal = 0x7f020095;
		public static final int common_plus_signin_btn_icon_dark_pressed = 0x7f020096;
		public static final int common_plus_signin_btn_icon_light = 0x7f020097;
		public static final int common_plus_signin_btn_icon_light_disabled = 0x7f020098;
		public static final int common_plus_signin_btn_icon_light_focused = 0x7f020099;
		public static final int common_plus_signin_btn_icon_light_normal = 0x7f02009a;
		public static final int common_plus_signin_btn_icon_light_pressed = 0x7f02009b;
		public static final int common_plus_signin_btn_text_dark = 0x7f02009c;
		public static final int common_plus_signin_btn_text_dark_disabled = 0x7f02009d;
		public static final int common_plus_signin_btn_text_dark_focused = 0x7f02009e;
		public static final int common_plus_signin_btn_text_dark_normal = 0x7f02009f;
		public static final int common_plus_signin_btn_text_dark_pressed = 0x7f0200a0;
		public static final int common_plus_signin_btn_text_light = 0x7f0200a1;
		public static final int common_plus_signin_btn_text_light_disabled = 0x7f0200a2;
		public static final int common_plus_signin_btn_text_light_focused = 0x7f0200a3;
		public static final int common_plus_signin_btn_text_light_normal = 0x7f0200a4;
		public static final int common_plus_signin_btn_text_light_pressed = 0x7f0200a5;
		public static final int places_ic_clear = 0x7f020132;
		public static final int places_ic_search = 0x7f020133;
		public static final int powered_by_google_dark = 0x7f020134;
		public static final int powered_by_google_light = 0x7f020135;
	}
	public static final class id {
		public static final int adjust_height = 0x7f0d0035;
		public static final int adjust_width = 0x7f0d0036;
		public static final int auto = 0x7f0d0042;
		public static final int dark = 0x7f0d0043;
		public static final int hybrid = 0x7f0d0037;
		public static final int icon_only = 0x7f0d003f;
		public static final int light = 0x7f0d0044;
		public static final int none = 0x7f0d0012;
		public static final int normal = 0x7f0d000e;
		public static final int place_autocomplete_clear_button = 0x7f0d01d1;
		public static final int place_autocomplete_powered_by_google = 0x7f0d01d3;
		public static final int place_autocomplete_prediction_primary_text = 0x7f0d01d5;
		public static final int place_autocomplete_prediction_secondary_text = 0x7f0d01d6;
		public static final int place_autocomplete_progress = 0x7f0d01d4;
		public static final int place_autocomplete_search_button = 0x7f0d01cf;
		public static final int place_autocomplete_search_input = 0x7f0d01d0;
		public static final int place_autocomplete_separator = 0x7f0d01d2;
		public static final int satellite = 0x7f0d0038;
		public static final int standard = 0x7f0d0040;
		public static final int terrain = 0x7f0d0039;
		public static final int wide = 0x7f0d0041;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0b0005;
	}
	public static final class layout {
		public static final int place_autocomplete_fragment = 0x7f040095;
		public static final int place_autocomplete_item_powered_by_google = 0x7f040096;
		public static final int place_autocomplete_item_prediction = 0x7f040097;
		public static final int place_autocomplete_progress = 0x7f040098;
	}
	public static final class string {
		public static final int auth_google_play_services_client_facebook_display_name = 0x7f070058;
		public static final int auth_google_play_services_client_google_display_name = 0x7f070059;
		public static final int common_google_play_services_api_unavailable_text = 0x7f070023;
		public static final int common_google_play_services_enable_button = 0x7f070024;
		public static final int common_google_play_services_enable_text = 0x7f070025;
		public static final int common_google_play_services_enable_title = 0x7f070026;
		public static final int common_google_play_services_install_button = 0x7f070027;
		public static final int common_google_play_services_install_text_phone = 0x7f070028;
		public static final int common_google_play_services_install_text_tablet = 0x7f070029;
		public static final int common_google_play_services_install_title = 0x7f07002a;
		public static final int common_google_play_services_invalid_account_text = 0x7f07002b;
		public static final int common_google_play_services_invalid_account_title = 0x7f07002c;
		public static final int common_google_play_services_network_error_text = 0x7f07002d;
		public static final int common_google_play_services_network_error_title = 0x7f07002e;
		public static final int common_google_play_services_notification_ticker = 0x7f07002f;
		public static final int common_google_play_services_restricted_profile_text = 0x7f070030;
		public static final int common_google_play_services_restricted_profile_title = 0x7f070031;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f070032;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f070033;
		public static final int common_google_play_services_unknown_issue = 0x7f070034;
		public static final int common_google_play_services_unsupported_text = 0x7f070035;
		public static final int common_google_play_services_unsupported_title = 0x7f070036;
		public static final int common_google_play_services_update_button = 0x7f070037;
		public static final int common_google_play_services_update_text = 0x7f070038;
		public static final int common_google_play_services_update_title = 0x7f070039;
		public static final int common_google_play_services_updating_text = 0x7f07003a;
		public static final int common_google_play_services_updating_title = 0x7f07003b;
		public static final int common_google_play_services_wear_update_text = 0x7f07003c;
		public static final int common_open_on_phone = 0x7f07003d;
		public static final int common_signin_button_text = 0x7f07003e;
		public static final int common_signin_button_text_long = 0x7f07003f;
		public static final int place_autocomplete_clear_button = 0x7f07004c;
		public static final int place_autocomplete_search_hint = 0x7f07004d;
	}
	public static final class styleable {
		public static final int[] LoadingImageView = { 0x7f01006f, 0x7f010070, 0x7f010071 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
		public static final int[] MapAttrs = { 0x7f010072, 0x7f010073, 0x7f010074, 0x7f010075, 0x7f010076, 0x7f010077, 0x7f010078, 0x7f010079, 0x7f01007a, 0x7f01007b, 0x7f01007c, 0x7f01007d, 0x7f01007e, 0x7f01007f, 0x7f010080, 0x7f010081, 0x7f010082 };
		public static final int MapAttrs_ambientEnabled = 16;
		public static final int MapAttrs_cameraBearing = 1;
		public static final int MapAttrs_cameraTargetLat = 2;
		public static final int MapAttrs_cameraTargetLng = 3;
		public static final int MapAttrs_cameraTilt = 4;
		public static final int MapAttrs_cameraZoom = 5;
		public static final int MapAttrs_liteMode = 6;
		public static final int MapAttrs_mapType = 0;
		public static final int MapAttrs_uiCompass = 7;
		public static final int MapAttrs_uiMapToolbar = 15;
		public static final int MapAttrs_uiRotateGestures = 8;
		public static final int MapAttrs_uiScrollGestures = 9;
		public static final int MapAttrs_uiTiltGestures = 10;
		public static final int MapAttrs_uiZoomControls = 11;
		public static final int MapAttrs_uiZoomGestures = 12;
		public static final int MapAttrs_useViewLifecycle = 13;
		public static final int MapAttrs_zOrderOnTop = 14;
		public static final int[] SignInButton = { 0x7f0100a8, 0x7f0100a9, 0x7f0100aa };
		public static final int SignInButton_buttonSize = 0;
		public static final int SignInButton_colorScheme = 1;
		public static final int SignInButton_scopeUris = 2;
	}
}
