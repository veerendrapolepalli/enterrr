/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.google.maps.android;

public final class R {
	public static final class attr {
		public static final int cameraBearing = 0x7f010073;
		public static final int cameraTargetLat = 0x7f010074;
		public static final int cameraTargetLng = 0x7f010075;
		public static final int cameraTilt = 0x7f010076;
		public static final int cameraZoom = 0x7f010077;
		public static final int circleCrop = 0x7f010071;
		public static final int imageAspectRatio = 0x7f010070;
		public static final int imageAspectRatioAdjust = 0x7f01006f;
		public static final int liteMode = 0x7f010078;
		public static final int mapType = 0x7f010072;
		public static final int uiCompass = 0x7f010079;
		public static final int uiMapToolbar = 0x7f010081;
		public static final int uiRotateGestures = 0x7f01007a;
		public static final int uiScrollGestures = 0x7f01007b;
		public static final int uiTiltGestures = 0x7f01007c;
		public static final int uiZoomControls = 0x7f01007d;
		public static final int uiZoomGestures = 0x7f01007e;
		public static final int useViewLifecycle = 0x7f01007f;
		public static final int zOrderOnTop = 0x7f010080;
	}
	public static final class color {
		public static final int common_action_bar_splitter = 0x7f0c002a;
	}
	public static final class drawable {
		public static final int bubble_mask = 0x7f02005a;
		public static final int bubble_shadow = 0x7f02005b;
		public static final int common_full_open_on_phone = 0x7f02007d;
		public static final int common_ic_googleplayservices = 0x7f020092;
		public static final int ic_plusone_medium_off_client = 0x7f0200fd;
		public static final int ic_plusone_small_off_client = 0x7f0200fe;
		public static final int ic_plusone_standard_off_client = 0x7f0200ff;
		public static final int ic_plusone_tall_off_client = 0x7f020100;
	}
	public static final class id {
		public static final int adjust_height = 0x7f0d0035;
		public static final int adjust_width = 0x7f0d0036;
		public static final int hybrid = 0x7f0d0037;
		public static final int none = 0x7f0d0012;
		public static final int normal = 0x7f0d000e;
		public static final int satellite = 0x7f0d0038;
		public static final int terrain = 0x7f0d0039;
		public static final int text = 0x7f0d000a;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0b0005;
	}
	public static final class layout {
		public static final int text_bubble = 0x7f0400aa;
	}
	public static final class raw {
		public static final int gtm_analytics = 0x7f060000;
	}
	public static final class string {
		public static final int common_google_play_services_enable_button = 0x7f070024;
		public static final int common_google_play_services_enable_text = 0x7f070025;
		public static final int common_google_play_services_enable_title = 0x7f070026;
		public static final int common_google_play_services_install_button = 0x7f070027;
		public static final int common_google_play_services_install_text_phone = 0x7f070028;
		public static final int common_google_play_services_install_text_tablet = 0x7f070029;
		public static final int common_google_play_services_install_title = 0x7f07002a;
		public static final int common_google_play_services_invalid_account_text = 0x7f07002b;
		public static final int common_google_play_services_invalid_account_title = 0x7f07002c;
		public static final int common_google_play_services_network_error_text = 0x7f07002d;
		public static final int common_google_play_services_network_error_title = 0x7f07002e;
		public static final int common_google_play_services_notification_ticker = 0x7f07002f;
		public static final int common_google_play_services_unknown_issue = 0x7f070034;
		public static final int common_google_play_services_unsupported_text = 0x7f070035;
		public static final int common_google_play_services_unsupported_title = 0x7f070036;
		public static final int common_google_play_services_update_button = 0x7f070037;
		public static final int common_google_play_services_update_text = 0x7f070038;
		public static final int common_google_play_services_update_title = 0x7f070039;
		public static final int common_open_on_phone = 0x7f07003d;
		public static final int common_signin_button_text = 0x7f07003e;
		public static final int common_signin_button_text_long = 0x7f07003f;
	}
	public static final class style {
		public static final int Bubble_TextAppearance_Dark = 0x7f0a00b9;
		public static final int Bubble_TextAppearance_Light = 0x7f0a00ba;
		public static final int ClusterIcon_TextAppearance = 0x7f0a00bd;
	}
	public static final class styleable {
		public static final int[] LoadingImageView = { 0x7f01006f, 0x7f010070, 0x7f010071 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
		public static final int[] MapAttrs = { 0x7f010072, 0x7f010073, 0x7f010074, 0x7f010075, 0x7f010076, 0x7f010077, 0x7f010078, 0x7f010079, 0x7f01007a, 0x7f01007b, 0x7f01007c, 0x7f01007d, 0x7f01007e, 0x7f01007f, 0x7f010080, 0x7f010081, 0x7f010082 };
		public static final int MapAttrs_cameraBearing = 1;
		public static final int MapAttrs_cameraTargetLat = 2;
		public static final int MapAttrs_cameraTargetLng = 3;
		public static final int MapAttrs_cameraTilt = 4;
		public static final int MapAttrs_cameraZoom = 5;
		public static final int MapAttrs_liteMode = 6;
		public static final int MapAttrs_mapType = 0;
		public static final int MapAttrs_uiCompass = 7;
		public static final int MapAttrs_uiMapToolbar = 15;
		public static final int MapAttrs_uiRotateGestures = 8;
		public static final int MapAttrs_uiScrollGestures = 9;
		public static final int MapAttrs_uiTiltGestures = 10;
		public static final int MapAttrs_uiZoomControls = 11;
		public static final int MapAttrs_uiZoomGestures = 12;
		public static final int MapAttrs_useViewLifecycle = 13;
		public static final int MapAttrs_zOrderOnTop = 14;
	}
}
